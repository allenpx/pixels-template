var path = require('path');
var fs = require('fs');
var colors = require('colors');

var cwd = process.cwd();
var target = path.normalize(cwd+'/lib');
var dest = path.normalize(cwd+'/node_modules/lib');


// does the destination already exist?
if (fs.existsSync(dest)) {
    process.exit();
}else{

    fs.symlink(target, dest, 'dir', function(e){
        
        if(e){
            console.log("Postinstall Error".red.bold);
            console.log(e);
        }else{
            console.log('Postinstall Success'.green.bold);
        }

        process.exit()
        
    });
    
}



