var _ = require('underscore');
var pkg = require('./package');
var lessOptions = {
        paths: [
        "less",
        "node_modules",
    ],
    plugins: [
      (require('less-plugin-glob'))
    ],
    compress:true,
    sourceMap:true,
    sourceMapRootpath:''
}

module.exports = function(grunt) {

    var liveReloadPort = 6425;

    grunt.initConfig({

        pkg: pkg,

        uglify: {
            js:{
                options: {
                    compress:true,
                    mangle:false,
                    sourceMap:false
                },
                files: {
                    'js/template.js': ['js/template.dev.js'],
                }
            },
        },

        less: {
            template_base: {
                options: _.extend(lessOptions, {sourceMapURL:'css/template.base.css.map'}),
                files: { "css/template.base.css":  "less/build/template.base.less" }
            },
            template_responsive: {
                options: _.extend(lessOptions, {sourceMapURL:'css/template.responsive.css.map'}),
                files: { "css/template.responsive.css":  "less/build/template.responsive.less" }
            },
        },

        'template-module': {
            templates:{
                files: "lib/tpl/**/*.hbs",
                options: {
                    provider:'handlebars',
                    module: true,
                    single:true,
                }
            }
        },

        browserify: {

            options:{
                debug:false,
                external: [
                  'jquery',
                  'backbone',
                  'underscore',
                  'inherit',
                  'inherit/events'
                ],
            },

            template: {
                src: ['lib/index.js'],
                dest: 'js/template.dev.js',
                options: {
                    debug:true,
                    external:null,
                    alias: [
                        'jquery',
                        'backbone',
                        'underscore',
                        'inherit',
                        'inherit/events',
                    ],
                },
            },
            
        },

        copy: {
            icons: {
                src: './node_modules/pristine-icons/icons.svg',
                dest: './images/icons.svg',
            },
        },

        watch:{
            html: {
                files: ['*.html'],
                options:{
                    livereload:{
                        port:liveReloadPort
                    }
                }
            },
            less: { 
                files: [
                    'less/**/*.less',
                    'node_modules/ui-2/less/**/*.less',
                ],
                tasks: ['less'],
                options:{
                    livereload:{
                        port:liveReloadPort
                    }
                }
            },
            templates: { 
                files: ['lib/tpl/**/**', '!lib/tpl/**/*.js'],
                tasks: ['template-module','browserify'],
                options:{
                    livereload:{
                        port:liveReloadPort
                    }
                }
            },
            js: {
                files: [
                    'lib/**/*.js',
                    'lib/**/*.json',
                ],
                tasks: [
                    'browserify', 
                    'uglify'
                ],
                options:{
                    livereload:{
                        port:liveReloadPort
                    }
                }
            },
        }

    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-tasks');
    grunt.loadNpmTasks('grunt-template-module');

    grunt.registerTask('default', [
        'copy',
        'template-module',
        'browserify',
        'uglify',
        'less',
        'watch'
    ]);


};
