var cargo = require('cargo');
var _ = require('underscore');
var $ = require('jquery');

exports.domains = {
    'allen':'129.63.83.158',
    'dev':'dev.uml.edu',
}
exports.stylesheets = [
    'template.base.',
    'template.responsive.',
];

exports.testStyles = function(domain){

    domain = domain || 'localhost';
    
    cargo.local('debug_styles', domain);

    _.each(exports.stylesheets, function(sheet){
        $('link[href*="'+sheet+'"]').attr('href', 'http://'+domain+'/system/template/v5/css/'+sheet+'css?t='+Date.now());
    });

}

exports.revertStyles = function(){
    cargo.local('debug_styles', null);
}


exports.testPrintStyles = function(bool){
    if(typeof bool == 'undefined'){
        if(cargo.local('debug_print') != undefined){
            bool = false;
        }else{
            bool = true;
        }
    }

    if(bool){
        cargo.local('debug_print', true);

        $('head link[rel="stylesheet"]').each(function(index, sheet){
            var $sheet = $(sheet);
            if( $sheet.attr('media') == undefined || $sheet.attr('media').indexOf('print') == -1 ){
                $sheet.remove();
            }
        });

    }else{
        cargo.local.remove('debug_print');
    }



}

exports.init = function(){

    require('./design-grid')();


}


exports.removeEMTags = function(){
    $('.components>span').each(function(index, span){
        $(span).find('.component').unwrap();
    });

    $('.component.blocks .inside>span').each(function(index, span){
        $(span).find('.block').unwrap();
    });
}

exports.evaluate = function(){

}


$(document).ready(function(){
    exports.init();
})