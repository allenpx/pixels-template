var $ = jQuery = global.jQuery = require('jquery');
var jQuery = $;
var _ = require('underscore');
var cargo = require('cargo');
global.cargo = cargo;
var Combokeys = require("combokeys");

var template = require('lib/tpl/design-grid');
 
var $grid = null;

module.exports = function(){
 
	$('body').prepend(template());
 
	$grid = $('#grid-overlay');

	if(typeof cargo.local('gridVisible') == "undefined"){
		cargo.local('gridVisible', 'hidden');
	}

	exports.toggle(cargo.local('gridVisible'));
    
    var combokeys = new Combokeys(document.documentElement);

	combokeys.bind('ctrl+l', function(e) {
	    exports.toggle();
	    return false;
	}); 

	$('nav.primary').bind('tripleclick', function(e) {
	    exports.toggle();
	    return false;
	});
}


exports.toggle = function(value){

	if(typeof value === 'undefined'){
		if(cargo.local('gridVisible') == 'hidden'){
			value = "visible";
			cargo.local('gridVisible', 'visible');
		}else{
			value = "hidden";
			cargo.local('gridVisible', 'hidden');
		}
	}else{
		cargo.local('gridVisible', value);
	}

	if(value === "hidden"){
		$grid.css('display', 'none');
	}else{
		$grid.css('display', 'block');
	}

}






