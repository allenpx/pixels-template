require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function (global){
var $ = jQuery = global.jQuery = require('jquery');
var jQuery = $;
var _ = require('underscore');
var cargo = require('cargo');
global.cargo = cargo;
var Combokeys = require("combokeys");

var template = require('lib/tpl/design-grid');
 
var $grid = null;

module.exports = function(){
 
	$('body').prepend(template());
 
	$grid = $('#grid-overlay');

	if(typeof cargo.local('gridVisible') == "undefined"){
		cargo.local('gridVisible', 'hidden');
	}

	exports.toggle(cargo.local('gridVisible'));
    
    var combokeys = new Combokeys(document.documentElement);

	combokeys.bind('ctrl+l', function(e) {
	    exports.toggle();
	    return false;
	}); 

	$('nav.primary').bind('tripleclick', function(e) {
	    exports.toggle();
	    return false;
	});
}


exports.toggle = function(value){

	if(typeof value === 'undefined'){
		if(cargo.local('gridVisible') == 'hidden'){
			value = "visible";
			cargo.local('gridVisible', 'visible');
		}else{
			value = "hidden";
			cargo.local('gridVisible', 'hidden');
		}
	}else{
		cargo.local('gridVisible', value);
	}

	if(value === "hidden"){
		$grid.css('display', 'none');
	}else{
		$grid.css('display', 'block');
	}

}







}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"cargo":5,"combokeys":6,"jquery":"jquery","lib/tpl/design-grid":3,"underscore":"underscore"}],2:[function(require,module,exports){
(function (global){
global.jQuery = require('jquip');
global.$ = global.jQuery;
global._ = require('underscore');
global.Backbone = require('backbone');
global.Backbone.$ = $;
global.ui = require('ui-2');
require('backbone-super');


var test = new ui.Input({
    label:"Test"
});
$('#test-view').append(test.render().$el);


require('lib/debug/design-grid')();
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"backbone":"backbone","backbone-super":4,"jquip":"jquery","lib/debug/design-grid":1,"ui-2":61,"underscore":"underscore"}],3:[function(require,module,exports){
var _ = require('handlebars/runtime');
module.exports = _.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div id=\"grid-overlay\" class=\"grid-overlay-wrapper\" style=\"display:none;\">\n<div class=\"grid-overlay\">"
    + escapeExpression(((helper = (helper = helpers.test || (depth0 != null ? depth0.test : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"test","hash":{},"data":data}) : helper)))
    + "\n	<div class=\"show-for-small-only\">\n		<div>\n			<div class=\"column\"></div> \n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n\n			<div class=\"column\"></div> \n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n\n			<div class=\"column\"></div> \n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n\n			<div class=\"column\"></div> \n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n		</div>\n	</div>\n	<div class=\"show-for-medium-only\">\n		<div>\n			<div class=\"column\"></div> \n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n\n			<div class=\"column\"></div> \n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n		</div>\n	</div>\n	<div class=\"show-for-large-up\">\n		<div>\n			<div class=\"column\"></div> \n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n			<div class=\"column\"></div>\n			<div class=\"column\"></div> \n			<div class=\"column\"></div>\n		</div>\n	</div>\n</div>";
},"useData":true});
},{"handlebars/runtime":47}],4:[function(require,module,exports){
// This is a plugin, constructed from parts of Backbone.js and John Resig's inheritance script.
// (See http://backbonejs.org, http://ejohn.org/blog/simple-javascript-inheritance/)
// No credit goes to me as I did absolutely nothing except patch these two together.
(function(root, factory) {

  // Set up Backbone appropriately for the environment. Start with AMD.
  if (typeof define === 'function' && define.amd) {
    define(['underscore', 'backbone'], function(_, Backbone) {
      // Export global even in AMD case in case this script is loaded with
      // others that may still expect a global Backbone.
      factory( _, Backbone);
    });

  // Next for Node.js or CommonJS.
  } else if (typeof exports !== 'undefined' && typeof require === 'function') {
    var _ = require('underscore'),
		Backbone = require('backbone');
    factory(_, Backbone);

  // Finally, as a browser global.
  } else {
    factory(root._, root.Backbone);
  }

}(this, function factory(_, Backbone) {
	Backbone.Model.extend = Backbone.Collection.extend = Backbone.Router.extend = Backbone.View.extend = function(protoProps, classProps) {
		var child = inherits(this, protoProps, classProps);
		child.extend = this.extend;
		return child;
	};
	var unImplementedSuper = function(method){throw "Super does not implement this method: " + method;};

  var fnTest = /\b_super\b/;

  var makeWrapper = function(parentProto, name, fn) {
    var wrapper = function() {
      var tmp = this._super;

      // Add a new ._super() method that is the same method
      // but on the super-class
      this._super = parentProto[name] || unImplementedSuper(name);

      // The method only need to be bound temporarily, so we
      // remove it when we're done executing
      var ret;
      try {
        ret = fn.apply(this, arguments);
      } finally {
        this._super = tmp;
      }
      return ret;
    };

    //we must move properties from old function to new
    for (var prop in fn) {
      wrapper[prop] = fn[prop];
      delete fn[prop];
    }

    return wrapper;
  };

	var ctor = function(){}, inherits = function(parent, protoProps, staticProps) {
        var child, parentProto = parent.prototype;

		// The constructor function for the new subclass is either defined by you
		// (the "constructor" property in your `extend` definition), or defaulted
		// by us to simply call the parent's constructor.
		if (protoProps && protoProps.hasOwnProperty('constructor')) {
			child = protoProps.constructor;
		} else {
			child = function(){ return parent.apply(this, arguments); };
		}

		// Inherit class (static) properties from parent.
		_.extend(child, parent, staticProps);

		// Set the prototype chain to inherit from `parent`, without calling
		// `parent`'s constructor function.
		ctor.prototype = parentProto;
		child.prototype = new ctor();

		// Add prototype properties (instance properties) to the subclass,
		// if supplied.
		if (protoProps) {
			_.extend(child.prototype, protoProps);

			// Copy the properties over onto the new prototype
			for (var name in protoProps) {
				// Check if we're overwriting an existing function
				if (typeof protoProps[name] == "function" && fnTest.test(protoProps[name])) {
					child.prototype[name] = makeWrapper(parentProto, name, protoProps[name]);
				}
			}
		}

		// Add static properties to the constructor function, if supplied.
		if (staticProps) _.extend(child, staticProps);

		// Correctly set child's `prototype.constructor`.
		child.prototype.constructor = child;

		// Set a convenience property in case the parent's prototype is needed later.
		child.__super__ = parentProto;

		return child;
	};

	return inherits;
}));


},{"backbone":"backbone","underscore":"underscore"}],5:[function(require,module,exports){
/*!
 * cargo 0.8.0+201405131636
 * https://github.com/ryanve/cargo
 * MIT License (c) 2014 Ryan Van Etten
 */
!function(root, name, make) {
  if (typeof module != 'undefined' && module.exports) module.exports = make()
  else root[name] = make()
}(this, 'cargo', function() {

  var cargo = {}
    , win = typeof window != 'undefined' && window
    , son = typeof JSON != 'undefined' && JSON || false
    , has = {}.hasOwnProperty
    
  function clone(o) {
    var k, r = {}
    for (k in o) has.call(o, k) && (r[k] = o[k])
    return r
  }
  
  function test(api, key) {
    if (api) try {
      key = key || 'cargo'+-new Date
      api.setItem(key, key)
      api.removeItem(key)
      return true
    } catch (e) {}
    return false
  }
  
  /**
   * @param {Storage=} api
   * @return {Function} abstraction
   */
  function abstracts(api) {
    var und, stores = test(api), cache = {}, all = stores ? api : cache
    function f(k, v) {
      var n = arguments.length
      if (1 < n) return und === v ? f['remove'](k) : f['set'](k, v), v
      return n ? f['get'](k) : clone(all)
    }
    f['stores'] = stores
    f['decode'] = son.parse
    f['encode'] = son.stringify
    f['get'] = stores ? function(k) {
      return und == (k = api.getItem(k)) ? und : k
    } : function(k) {
      return !has.call(cache, k) ? und : cache[k]
    }
    f['set'] = stores ? function(k, v) {
      api.setItem(k, v)
    } : function(k, v) {
      cache[k] = v
    }
    f['remove'] = stores ? function(k) {
      api.removeItem(k)
    } : function(k) {
      delete cache[k]
    }
    return f
  }

  cargo['session'] = abstracts(win.sessionStorage)
  cargo['local'] = abstracts(win.localStorage)
  cargo['temp'] = abstracts()
  return cargo
});
},{}],6:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

module.exports = function (element) {
  var self = this
  var Combokeys = self.constructor

  /**
   * a list of all the callbacks setup via Combokeys.bind()
   *
   * @type {Object}
   */
  self.callbacks = {}

  /**
   * direct map of string combinations to callbacks used for trigger()
   *
   * @type {Object}
   */
  self.directMap = {}

  /**
   * keeps track of what level each sequence is at since multiple
   * sequences can start out with the same sequence
   *
   * @type {Object}
   */
  self.sequenceLevels = {}

  /**
   * variable to store the setTimeout call
   *
   * @type {null|number}
   */
  self.resetTimer

  /**
   * temporary state where we will ignore the next keyup
   *
   * @type {boolean|string}
   */
  self.ignoreNextKeyup = false

  /**
   * temporary state where we will ignore the next keypress
   *
   * @type {boolean}
   */
  self.ignoreNextKeypress = false

  /**
   * are we currently inside of a sequence?
   * type of action ("keyup" or "keydown" or "keypress") or false
   *
   * @type {boolean|string}
   */
  self.nextExpectedAction = false

  self.element = element

  self.addEvents()

  Combokeys.instances.push(self)
  return self
}

module.exports.prototype.bind = require('./prototype/bind')
module.exports.prototype.bindMultiple = require('./prototype/bindMultiple')
module.exports.prototype.unbind = require('./prototype/unbind')
module.exports.prototype.trigger = require('./prototype/trigger')
module.exports.prototype.reset = require('./prototype/reset.js')
module.exports.prototype.stopCallback = require('./prototype/stopCallback')
module.exports.prototype.handleKey = require('./prototype/handleKey')
module.exports.prototype.addEvents = require('./prototype/addEvents')
module.exports.prototype.bindSingle = require('./prototype/bindSingle')
module.exports.prototype.getKeyInfo = require('./prototype/getKeyInfo')
module.exports.prototype.pickBestAction = require('./prototype/pickBestAction')
module.exports.prototype.getReverseMap = require('./prototype/getReverseMap')
module.exports.prototype.getMatches = require('./prototype/getMatches')
module.exports.prototype.resetSequences = require('./prototype/resetSequences')
module.exports.prototype.fireCallback = require('./prototype/fireCallback')
module.exports.prototype.bindSequence = require('./prototype/bindSequence')
module.exports.prototype.resetSequenceTimer = require('./prototype/resetSequenceTimer')
module.exports.prototype.detach = require('./prototype/detach')

module.exports.instances = []
module.exports.reset = require('./reset')

/**
 * variable to store the flipped version of MAP from above
 * needed to check if we should use keypress or not when no action
 * is specified
 *
 * @type {Object|undefined}
 */
module.exports.REVERSE_MAP = null

},{"./prototype/addEvents":7,"./prototype/bind":8,"./prototype/bindMultiple":9,"./prototype/bindSequence":10,"./prototype/bindSingle":11,"./prototype/detach":12,"./prototype/fireCallback":13,"./prototype/getKeyInfo":14,"./prototype/getMatches":15,"./prototype/getReverseMap":16,"./prototype/handleKey":17,"./prototype/pickBestAction":20,"./prototype/reset.js":21,"./prototype/resetSequenceTimer":22,"./prototype/resetSequences":23,"./prototype/stopCallback":24,"./prototype/trigger":25,"./prototype/unbind":26,"./reset":27}],7:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'
module.exports = function () {
  var self = this
  var on = require('dom-event')
  var element = self.element

  self.eventHandler = require('./handleKeyEvent').bind(self)

  on(element, 'keypress', self.eventHandler)
  on(element, 'keydown', self.eventHandler)
  on(element, 'keyup', self.eventHandler)
}

},{"./handleKeyEvent":18,"dom-event":38}],8:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'
/**
 * binds an event to Combokeys
 *
 * can be a single key, a combination of keys separated with +,
 * an array of keys, or a sequence of keys separated by spaces
 *
 * be sure to list the modifier keys first to make sure that the
 * correct key ends up getting bound (the last key in the pattern)
 *
 * @param {string|Array} keys
 * @param {Function} callback
 * @param {string=} action - "keypress", "keydown", or "keyup"
 * @returns void
 */
module.exports = function (keys, callback, action) {
  var self = this

  keys = keys instanceof Array ? keys : [keys]
  self.bindMultiple(keys, callback, action)
  return self
}

},{}],9:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
 * binds multiple combinations to the same callback
 *
 * @param {Array} combinations
 * @param {Function} callback
 * @param {string|undefined} action
 * @returns void
 */
module.exports = function (combinations, callback, action) {
  var self = this

  for (var j = 0; j < combinations.length; ++j) {
    self.bindSingle(combinations[j], callback, action)
  }
}

},{}],10:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
 * binds a key sequence to an event
 *
 * @param {string} combo - combo specified in bind call
 * @param {Array} keys
 * @param {Function} callback
 * @param {string=} action
 * @returns void
 */
module.exports = function (combo, keys, callback, action) {
  var self = this

  // start off by adding a sequence level record for this combination
  // and setting the level to 0
  self.sequenceLevels[combo] = 0

  /**
   * callback to increase the sequence level for this sequence and reset
   * all other sequences that were active
   *
   * @param {string} nextAction
   * @returns {Function}
   */
  function increaseSequence (nextAction) {
    return function () {
      self.nextExpectedAction = nextAction
      ++self.sequenceLevels[combo]
      self.resetSequenceTimer()
    }
  }

  /**
   * wraps the specified callback inside of another function in order
   * to reset all sequence counters as soon as this sequence is done
   *
   * @param {Event} e
   * @returns void
   */
  function callbackAndReset (e) {
    var characterFromEvent
    self.fireCallback(callback, e, combo)

    // we should ignore the next key up if the action is key down
    // or keypress.  this is so if you finish a sequence and
    // release the key the final key will not trigger a keyup
    if (action !== 'keyup') {
      characterFromEvent = require('../../helpers/characterFromEvent')
      self.ignoreNextKeyup = characterFromEvent(e)
    }

    // weird race condition if a sequence ends with the key
    // another sequence begins with
    setTimeout(
      function () {
        self.resetSequences()
      },
      10
    )
  }

  // loop through keys one at a time and bind the appropriate callback
  // function.  for any key leading up to the final one it should
  // increase the sequence. after the final, it should reset all sequences
  //
  // if an action is specified in the original bind call then that will
  // be used throughout.  otherwise we will pass the action that the
  // next key in the sequence should match.  this allows a sequence
  // to mix and match keypress and keydown events depending on which
  // ones are better suited to the key provided
  for (var j = 0; j < keys.length; ++j) {
    var isFinal = j + 1 === keys.length
    var wrappedCallback = isFinal ? callbackAndReset : increaseSequence(action || self.getKeyInfo(keys[j + 1]).action)
    self.bindSingle(keys[j], wrappedCallback, action, combo, j)
  }
}

},{"../../helpers/characterFromEvent":28}],11:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
 * binds a single keyboard combination
 *
 * @param {string} combination
 * @param {Function} callback
 * @param {string=} action
 * @param {string=} sequenceName - name of sequence if part of sequence
 * @param {number=} level - what part of the sequence the command is
 * @returns void
 */
module.exports = function (combination, callback, action, sequenceName, level) {
  var self = this

  // store a direct mapped reference for use with Combokeys.trigger
  self.directMap[combination + ':' + action] = callback

  // make sure multiple spaces in a row become a single space
  combination = combination.replace(/\s+/g, ' ')

  var sequence = combination.split(' ')
  var info

  // if this pattern is a sequence of keys then run through this method
  // to reprocess each pattern one key at a time
  if (sequence.length > 1) {
    self.bindSequence(combination, sequence, callback, action)
    return
  }

  info = self.getKeyInfo(combination, action)

  // make sure to initialize array if this is the first time
  // a callback is added for this key
  self.callbacks[info.key] = self.callbacks[info.key] || []

  // remove an existing match if there is one
  self.getMatches(info.key, info.modifiers, {type: info.action}, sequenceName, combination, level)

  // add this call back to the array
  // if it is a sequence put it at the beginning
  // if not put it at the end
  //
  // this is important because the way these are processed expects
  // the sequence ones to come first
  self.callbacks[info.key][sequenceName ? 'unshift' : 'push']({
    callback: callback,
    modifiers: info.modifiers,
    action: info.action,
    seq: sequenceName,
    level: level,
    combo: combination
  })
}

},{}],12:[function(require,module,exports){
var off = require('dom-event').off
module.exports = function () {
  var self = this
  var element = self.element

  off(element, 'keypress', self.eventHandler)
  off(element, 'keydown', self.eventHandler)
  off(element, 'keyup', self.eventHandler)
}

},{"dom-event":38}],13:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
 * actually calls the callback function
 *
 * if your callback function returns false this will use the jquery
 * convention - prevent default and stop propogation on the event
 *
 * @param {Function} callback
 * @param {Event} e
 * @returns void
 */
module.exports = function (callback, e, combo, sequence) {
  var self = this
  var preventDefault
  var stopPropagation

  // if this event should not happen stop here
  if (self.stopCallback(e, e.target || e.srcElement, combo, sequence)) {
    return
  }

  if (callback(e, combo) === false) {
    preventDefault = require('../../helpers/preventDefault')
    preventDefault(e)
    stopPropagation = require('../../helpers/stopPropagation')
    stopPropagation(e)
  }
}

},{"../../helpers/preventDefault":32,"../../helpers/stopPropagation":37}],14:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
 * Gets info for a specific key combination
 *
 * @param  {string} combination key combination ("command+s" or "a" or "*")
 * @param  {string=} action
 * @returns {Object}
 */
module.exports = function (combination, action) {
  var self = this
  var keysFromString
  var keys
  var key
  var j
  var modifiers = []
  var SPECIAL_ALIASES
  var SHIFT_MAP
  var isModifier

  keysFromString = require('../../helpers/keysFromString')
  // take the keys from this pattern and figure out what the actual
  // pattern is all about
  keys = keysFromString(combination)

  SPECIAL_ALIASES = require('../../helpers/special-aliases')
  SHIFT_MAP = require('../../helpers/shift-map')
  isModifier = require('../../helpers/isModifier')
  for (j = 0; j < keys.length; ++j) {
    key = keys[j]

    // normalize key names
    if (SPECIAL_ALIASES[key]) {
      key = SPECIAL_ALIASES[key]
    }

    // if this is not a keypress event then we should
    // be smart about using shift keys
    // this will only work for US keyboards however
    if (action && action !== 'keypress' && SHIFT_MAP[key]) {
      key = SHIFT_MAP[key]
      modifiers.push('shift')
    }

    // if this key is a modifier then add it to the list of modifiers
    if (isModifier(key)) {
      modifiers.push(key)
    }
  }

  // depending on what the key combination is
  // we will try to pick the best event for it
  action = self.pickBestAction(key, modifiers, action)

  return {
    key: key,
    modifiers: modifiers,
    action: action
  }
}

},{"../../helpers/isModifier":30,"../../helpers/keysFromString":31,"../../helpers/shift-map":33,"../../helpers/special-aliases":34}],15:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
 * finds all callbacks that match based on the keycode, modifiers,
 * and action
 *
 * @param {string} character
 * @param {Array} modifiers
 * @param {Event|Object} e
 * @param {string=} sequenceName - name of the sequence we are looking for
 * @param {string=} combination
 * @param {number=} level
 * @returns {Array}
 */
module.exports = function (character, modifiers, e, sequenceName, combination, level) {
  var self = this
  var j
  var callback
  var matches = []
  var action = e.type
  var isModifier
  var modifiersMatch

  if (action === 'keypress') {
    // 'any-character' callbacks are only on `keypress`
    var anyCharCallbacks = self.callbacks['any-character'] || []
    anyCharCallbacks.forEach(function (callback) {
      matches.push(callback)
    })
  }

  if (!self.callbacks[character]) {return matches}

  isModifier = require('../../helpers/isModifier')
  // if a modifier key is coming up on its own we should allow it
  if (action === 'keyup' && isModifier(character)) {
    modifiers = [character]
  }

  // loop through all callbacks for the key that was pressed
  // and see if any of them match
  for (j = 0; j < self.callbacks[character].length; ++j) {
    callback = self.callbacks[character][j]

    // if a sequence name is not specified, but this is a sequence at
    // the wrong level then move onto the next match
    if (!sequenceName && callback.seq && self.sequenceLevels[callback.seq] !== callback.level) {
      continue
    }

    // if the action we are looking for doesn't match the action we got
    // then we should keep going
    if (action !== callback.action) {
      continue
    }

    // if this is a keypress event and the meta key and control key
    // are not pressed that means that we need to only look at the
    // character, otherwise check the modifiers as well
    //
    // chrome will not fire a keypress if meta or control is down
    // safari will fire a keypress if meta or meta+shift is down
    // firefox will fire a keypress if meta or control is down
    modifiersMatch = require('./modifiersMatch')
    if ((action === 'keypress' && !e.metaKey && !e.ctrlKey) || modifiersMatch(modifiers, callback.modifiers)) {
      // when you bind a combination or sequence a second time it
      // should overwrite the first one.  if a sequenceName or
      // combination is specified in this call it does just that
      //
      // @todo make deleting its own method?
      var deleteCombo = !sequenceName && callback.combo === combination
      var deleteSequence = sequenceName && callback.seq === sequenceName && callback.level === level
      if (deleteCombo || deleteSequence) {
        self.callbacks[character].splice(j, 1)
      }

      matches.push(callback)
    }
  }

  return matches
}

},{"../../helpers/isModifier":30,"./modifiersMatch":19}],16:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
 * reverses the map lookup so that we can look for specific keys
 * to see what can and can't use keypress
 *
 * @return {Object}
 */
module.exports = function () {
  var self = this
  var constructor = self.constructor
  var SPECIAL_KEYS_MAP

  if (!constructor.REVERSE_MAP) {
    constructor.REVERSE_MAP = {}
    SPECIAL_KEYS_MAP = require('../../helpers/special-keys-map')
    for (var key in SPECIAL_KEYS_MAP) {
      // pull out the numeric keypad from here cause keypress should
      // be able to detect the keys from the character
      if (key > 95 && key < 112) {
        continue
      }

      if (SPECIAL_KEYS_MAP.hasOwnProperty(key)) {
        constructor.REVERSE_MAP[SPECIAL_KEYS_MAP[key]] = key
      }
    }
  }
  return constructor.REVERSE_MAP
}

},{"../../helpers/special-keys-map":36}],17:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
 * handles a character key event
 *
 * @param {string} character
 * @param {Array} modifiers
 * @param {Event} e
 * @returns void
 */
module.exports = function (character, modifiers, e) {
  var self = this
  var callbacks
  var j
  var doNotReset = {}
  var maxLevel = 0
  var processedSequenceCallback = false
  var isModifier
  var ignoreThisKeypress

  callbacks = self.getMatches(character, modifiers, e)
  // Calculate the maxLevel for sequences so we can only execute the longest callback sequence
  for (j = 0; j < callbacks.length; ++j) {
    if (callbacks[j].seq) {
      maxLevel = Math.max(maxLevel, callbacks[j].level)
    }
  }

  // loop through matching callbacks for this key event
  for (j = 0; j < callbacks.length; ++j) {
    // fire for all sequence callbacks
    // this is because if for example you have multiple sequences
    // bound such as "g i" and "g t" they both need to fire the
    // callback for matching g cause otherwise you can only ever
    // match the first one
    if (callbacks[j].seq) {
      // only fire callbacks for the maxLevel to prevent
      // subsequences from also firing
      //
      // for example 'a option b' should not cause 'option b' to fire
      // even though 'option b' is part of the other sequence
      //
      // any sequences that do not match here will be discarded
      // below by the resetSequences call
      if (callbacks[j].level !== maxLevel) {
        continue
      }

      processedSequenceCallback = true

      // keep a list of which sequences were matches for later
      doNotReset[callbacks[j].seq] = 1
      self.fireCallback(callbacks[j].callback, e, callbacks[j].combo, callbacks[j].seq)
      continue
    }

    // if there were no sequence matches but we are still here
    // that means this is a regular match so we should fire that
    if (!processedSequenceCallback) {
      self.fireCallback(callbacks[j].callback, e, callbacks[j].combo)
    }
  }

  // if the key you pressed matches the type of sequence without
  // being a modifier (ie "keyup" or "keypress") then we should
  // reset all sequences that were not matched by this event
  //
  // this is so, for example, if you have the sequence "h a t" and you
  // type "h e a r t" it does not match.  in this case the "e" will
  // cause the sequence to reset
  //
  // modifier keys are ignored because you can have a sequence
  // that contains modifiers such as "enter ctrl+space" and in most
  // cases the modifier key will be pressed before the next key
  //
  // also if you have a sequence such as "ctrl+b a" then pressing the
  // "b" key will trigger a "keypress" and a "keydown"
  //
  // the "keydown" is expected when there is a modifier, but the
  // "keypress" ends up matching the nextExpectedAction since it occurs
  // after and that causes the sequence to reset
  //
  // we ignore keypresses in a sequence that directly follow a keydown
  // for the same character
  ignoreThisKeypress = e.type === 'keypress' && self.ignoreNextKeypress
  isModifier = require('../../helpers/isModifier')
  if (e.type === self.nextExpectedAction && !isModifier(character) && !ignoreThisKeypress) {
    self.resetSequences(doNotReset)
  }

  self.ignoreNextKeypress = processedSequenceCallback && e.type === 'keydown'
}

},{"../../helpers/isModifier":30}],18:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
 * handles a keydown event
 *
 * @param {Event} e
 * @returns void
 */
module.exports = function (e) {
  var self = this
  var characterFromEvent
  var eventModifiers

  // normalize e.which for key events
  // @see http://stackoverflow.com/questions/4285627/javascript-keycode-vs-charcode-utter-confusion
  if (typeof e.which !== 'number') {
    e.which = e.keyCode
  }
  characterFromEvent = require('../../helpers/characterFromEvent')
  var character = characterFromEvent(e)

  // no character found then stop
  if (!character) {
    return
  }

  // need to use === for the character check because the character can be 0
  if (e.type === 'keyup' && self.ignoreNextKeyup === character) {
    self.ignoreNextKeyup = false
    return
  }

  eventModifiers = require('../../helpers/eventModifiers')
  self.handleKey(character, eventModifiers(e), e)
}

},{"../../helpers/characterFromEvent":28,"../../helpers/eventModifiers":29}],19:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
 * checks if two arrays are equal
 *
 * @param {Array} modifiers1
 * @param {Array} modifiers2
 * @returns {boolean}
 */
module.exports = function (modifiers1, modifiers2) {
  return modifiers1.sort().join(',') === modifiers2.sort().join(',')
}

},{}],20:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
 * picks the best action based on the key combination
 *
 * @param {string} key - character for key
 * @param {Array} modifiers
 * @param {string=} action passed in
 */
module.exports = function (key, modifiers, action) {
  var self = this

  // if no action was picked in we should try to pick the one
  // that we think would work best for this key
  if (!action) {
    action = self.getReverseMap()[key] ? 'keydown' : 'keypress'
  }

  // modifier keys don't work as expected with keypress,
  // switch to keydown
  if (action === 'keypress' && modifiers.length) {
    action = 'keydown'
  }

  return action
}

},{}],21:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
 * resets the library back to its initial state. This is useful
 * if you want to clear out the current keyboard shortcuts and bind
 * new ones - for example if you switch to another page
 *
 * @returns void
 */
module.exports = function () {
  var self = this
  self.callbacks = {}
  self.directMap = {}
  return this
}

},{}],22:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'
/**
 * called to set a 1 second timeout on the specified sequence
 *
 * this is so after each key press in the sequence you have 1 second
 * to press the next key before you have to start over
 *
 * @returns void
 */
module.exports = function () {
  var self = this

  clearTimeout(self.resetTimer)
  self.resetTimer = setTimeout(
    function () {
      self.resetSequences()
    },
    1000
  )
}

},{}],23:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
 * resets all sequence counters except for the ones passed in
 *
 * @param {Object} doNotReset
 * @returns void
 */
module.exports = function (doNotReset) {
  var self = this

  doNotReset = doNotReset || {}

  var activeSequences = false
  var key

  for (key in self.sequenceLevels) {
    if (doNotReset[key]) {
      activeSequences = true
      continue
    }
    self.sequenceLevels[key] = 0
  }

  if (!activeSequences) {
    self.nextExpectedAction = false
  }
}

},{}],24:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
* should we stop this event before firing off callbacks
*
* @param {Event} e
* @param {Element} element
* @return {boolean}
*/
module.exports = function (e, element) {
  // if the element has the class "combokeys" then no need to stop
  if ((' ' + element.className + ' ').indexOf(' combokeys ') > -1) {
    return false
  }

  var tagName = element.tagName.toLowerCase()

  // stop for input, select, and textarea
  return tagName === 'input' || tagName === 'select' || tagName === 'textarea' || element.isContentEditable
}

},{}],25:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'
/**
 * triggers an event that has already been bound
 *
 * @param {string} keys
 * @param {string=} action
 * @returns void
 */
module.exports = function (keys, action) {
  var self = this
  if (self.directMap[keys + ':' + action]) {
    self.directMap[keys + ':' + action]({}, keys)
  }
  return this
}

},{}],26:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'
/**
 * unbinds an event to Combokeys
 *
 * the unbinding sets the callback function of the specified key combo
 * to an empty function and deletes the corresponding key in the
 * directMap dict.
 *
 * TODO: actually remove this from the callbacks dictionary instead
 * of binding an empty function
 *
 * the keycombo+action has to be exactly the same as
 * it was defined in the bind method
 *
 * @param {string|Array} keys
 * @param {string} action
 * @returns void
 */
module.exports = function (keys, action) {
  var self = this

  return self.bind(keys, function () {}, action)
}

},{}],27:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

module.exports = function () {
  var self = this

  self.instances.forEach(function (combokeys) {
    combokeys.reset()
  })
}

},{}],28:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
 * takes the event and returns the key character
 *
 * @param {Event} e
 * @return {string}
 */
module.exports = function (e) {
  var SPECIAL_KEYS_MAP,
  SPECIAL_CHARACTERS_MAP
  SPECIAL_KEYS_MAP = require('./special-keys-map')
  SPECIAL_CHARACTERS_MAP = require('./special-characters-map')

  // for keypress events we should return the character as is
  if (e.type === 'keypress') {
    var character = String.fromCharCode(e.which)

    // if the shift key is not pressed then it is safe to assume
    // that we want the character to be lowercase.  this means if
    // you accidentally have caps lock on then your key bindings
    // will continue to work
    //
    // the only side effect that might not be desired is if you
    // bind something like 'A' cause you want to trigger an
    // event when capital A is pressed caps lock will no longer
    // trigger the event.  shift+a will though.
    if (!e.shiftKey) {
      character = character.toLowerCase()
    }

    return character
  }

  // for non keypress events the special maps are needed
  if (SPECIAL_KEYS_MAP[e.which]) {
    return SPECIAL_KEYS_MAP[e.which]
  }

  if (SPECIAL_CHARACTERS_MAP[e.which]) {
    return SPECIAL_CHARACTERS_MAP[e.which]
  }

  // if it is not in the special map

  // with keydown and keyup events the character seems to always
  // come in as an uppercase character whether you are pressing shift
  // or not.  we should make sure it is always lowercase for comparisons
  return String.fromCharCode(e.which).toLowerCase()
}

},{"./special-characters-map":35,"./special-keys-map":36}],29:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
 * takes a key event and figures out what the modifiers are
 *
 * @param {Event} e
 * @returns {Array}
 */
module.exports = function (e) {
  var modifiers = []

  if (e.shiftKey) {
    modifiers.push('shift')
  }

  if (e.altKey) {
    modifiers.push('alt')
  }

  if (e.ctrlKey) {
    modifiers.push('ctrl')
  }

  if (e.metaKey) {
    modifiers.push('meta')
  }

  return modifiers
}

},{}],30:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
 * determines if the keycode specified is a modifier key or not
 *
 * @param {string} key
 * @returns {boolean}
 */
module.exports = function (key) {
  return key === 'shift' || key === 'ctrl' || key === 'alt' || key === 'meta'
}

},{}],31:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
 * Converts from a string key combination to an array
 *
 * @param  {string} combination like "command+shift+l"
 * @return {Array}
 */
module.exports = function (combination) {
  if (combination === '+') {
    return ['+']
  }

  return combination.split('+')
}

},{}],32:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
 * prevents default for this event
 *
 * @param {Event} e
 * @returns void
 */
module.exports = function (e) {
  if (e.preventDefault) {
    e.preventDefault()
    return
  }

  e.returnValue = false
}

},{}],33:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'
/**
 * this is a mapping of keys that require shift on a US keypad
 * back to the non shift equivelents
 *
 * this is so you can use keyup events with these keys
 *
 * note that this will only work reliably on US keyboards
 *
 * @type {Object}
 */
module.exports = {
  '~': '`',
  '!': '1',
  '@': '2',
  '#': '3',
  '$': '4',
  '%': '5',
  '^': '6',
  '&': '7',
  '*': '8',
  '(': '9',
  ')': '0',
  '_': '-',
  '+': '=',
  ':': ';',
  '"': "'",
  '<': ',',
  '>': '.',
  '?': '/',
  '|': '\\'
}

},{}],34:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'
/**
 * this is a list of special strings you can use to map
 * to modifier keys when you specify your keyboard shortcuts
 *
 * @type {Object}
 */
module.exports = {
  'option': 'alt',
  'command': 'meta',
  'return': 'enter',
  'escape': 'esc',
  'mod': /Mac|iPod|iPhone|iPad/.test(navigator.platform) ? 'meta' : 'ctrl'
}

},{}],35:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'
/**
 * mapping for special characters so they can support
 *
 * this dictionary is only used incase you want to bind a
 * keyup or keydown event to one of these keys
 *
 * @type {Object}
 */
module.exports = {
  106: '*',
  107: '+',
  109: '-',
  110: '.',
  111: '/',
  186: ';',
  187: '=',
  188: ',',
  189: '-',
  190: '.',
  191: '/',
  192: '`',
  219: '[',
  220: '\\',
  221: ']',
  222: "'"
}

},{}],36:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'
/**
 * mapping of special keycodes to their corresponding keys
 *
 * everything in this dictionary cannot use keypress events
 * so it has to be here to map to the correct keycodes for
 * keyup/keydown events
 *
 * @type {Object}
 */
module.exports = {
  8: 'backspace',
  9: 'tab',
  13: 'enter',
  16: 'shift',
  17: 'ctrl',
  18: 'alt',
  20: 'capslock',
  27: 'esc',
  32: 'space',
  33: 'pageup',
  34: 'pagedown',
  35: 'end',
  36: 'home',
  37: 'left',
  38: 'up',
  39: 'right',
  40: 'down',
  45: 'ins',
  46: 'del',
  91: 'meta',
  93: 'meta',
  187: 'plus',
  189: 'minus',
  224: 'meta'
}

/**
 * loop through the f keys, f1 to f19 and add them to the map
 * programatically
 */
for (var i = 1; i < 20; ++i) {
  module.exports[111 + i] = 'f' + i
}

/**
 * loop through to map numbers on the numeric keypad
 */
for (i = 0; i <= 9; ++i) {
  module.exports[i + 96] = i
}

},{}],37:[function(require,module,exports){
/* eslint-env node, browser */
'use strict'

/**
 * stops propogation for this event
 *
 * @param {Event} e
 * @returns void
 */
module.exports = function (e) {
  if (e.stopPropagation) {
    e.stopPropagation()
    return
  }

  e.cancelBubble = true
}

},{}],38:[function(require,module,exports){
module.exports = on;
module.exports.on = on;
module.exports.off = off;

function on (element, event, callback, capture) {
  !element.addEventListener && (event = 'on' + event);
  (element.addEventListener || element.attachEvent).call(element, event, callback, capture);
  return callback;
}

function off (element, event, callback, capture) {
  !element.removeEventListener && (event = 'on' + event);
  (element.removeEventListener || element.detachEvent).call(element, event, callback, capture);
  return callback;
}

},{}],39:[function(require,module,exports){
// shim for using process in browser

var process = module.exports = {};
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = setTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            currentQueue[queueIndex].run();
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    clearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        setTimeout(drainQueue, 0);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

// TODO(shtylman)
process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}],40:[function(require,module,exports){
'use strict';

var _interopRequireWildcard = function (obj) { return obj && obj.__esModule ? obj : { 'default': obj }; };

exports.__esModule = true;

var _import = require('./handlebars/base');

var base = _interopRequireWildcard(_import);

// Each of these augment the Handlebars object. No need to setup here.
// (This is done to easily share code between commonjs and browse envs)

var _SafeString = require('./handlebars/safe-string');

var _SafeString2 = _interopRequireWildcard(_SafeString);

var _Exception = require('./handlebars/exception');

var _Exception2 = _interopRequireWildcard(_Exception);

var _import2 = require('./handlebars/utils');

var Utils = _interopRequireWildcard(_import2);

var _import3 = require('./handlebars/runtime');

var runtime = _interopRequireWildcard(_import3);

var _noConflict = require('./handlebars/no-conflict');

var _noConflict2 = _interopRequireWildcard(_noConflict);

// For compatibility and usage outside of module systems, make the Handlebars object a namespace
function create() {
  var hb = new base.HandlebarsEnvironment();

  Utils.extend(hb, base);
  hb.SafeString = _SafeString2['default'];
  hb.Exception = _Exception2['default'];
  hb.Utils = Utils;
  hb.escapeExpression = Utils.escapeExpression;

  hb.VM = runtime;
  hb.template = function (spec) {
    return runtime.template(spec, hb);
  };

  return hb;
}

var inst = create();
inst.create = create;

_noConflict2['default'](inst);

inst['default'] = inst;

exports['default'] = inst;
module.exports = exports['default'];
},{"./handlebars/base":41,"./handlebars/exception":42,"./handlebars/no-conflict":43,"./handlebars/runtime":44,"./handlebars/safe-string":45,"./handlebars/utils":46}],41:[function(require,module,exports){
'use strict';

var _interopRequireWildcard = function (obj) { return obj && obj.__esModule ? obj : { 'default': obj }; };

exports.__esModule = true;
exports.HandlebarsEnvironment = HandlebarsEnvironment;
exports.createFrame = createFrame;

var _import = require('./utils');

var Utils = _interopRequireWildcard(_import);

var _Exception = require('./exception');

var _Exception2 = _interopRequireWildcard(_Exception);

var VERSION = '3.0.1';
exports.VERSION = VERSION;
var COMPILER_REVISION = 6;

exports.COMPILER_REVISION = COMPILER_REVISION;
var REVISION_CHANGES = {
  1: '<= 1.0.rc.2', // 1.0.rc.2 is actually rev2 but doesn't report it
  2: '== 1.0.0-rc.3',
  3: '== 1.0.0-rc.4',
  4: '== 1.x.x',
  5: '== 2.0.0-alpha.x',
  6: '>= 2.0.0-beta.1'
};

exports.REVISION_CHANGES = REVISION_CHANGES;
var isArray = Utils.isArray,
    isFunction = Utils.isFunction,
    toString = Utils.toString,
    objectType = '[object Object]';

function HandlebarsEnvironment(helpers, partials) {
  this.helpers = helpers || {};
  this.partials = partials || {};

  registerDefaultHelpers(this);
}

HandlebarsEnvironment.prototype = {
  constructor: HandlebarsEnvironment,

  logger: logger,
  log: log,

  registerHelper: function registerHelper(name, fn) {
    if (toString.call(name) === objectType) {
      if (fn) {
        throw new _Exception2['default']('Arg not supported with multiple helpers');
      }
      Utils.extend(this.helpers, name);
    } else {
      this.helpers[name] = fn;
    }
  },
  unregisterHelper: function unregisterHelper(name) {
    delete this.helpers[name];
  },

  registerPartial: function registerPartial(name, partial) {
    if (toString.call(name) === objectType) {
      Utils.extend(this.partials, name);
    } else {
      if (typeof partial === 'undefined') {
        throw new _Exception2['default']('Attempting to register a partial as undefined');
      }
      this.partials[name] = partial;
    }
  },
  unregisterPartial: function unregisterPartial(name) {
    delete this.partials[name];
  }
};

function registerDefaultHelpers(instance) {
  instance.registerHelper('helperMissing', function () {
    if (arguments.length === 1) {
      // A missing field in a {{foo}} constuct.
      return undefined;
    } else {
      // Someone is actually trying to call something, blow up.
      throw new _Exception2['default']('Missing helper: "' + arguments[arguments.length - 1].name + '"');
    }
  });

  instance.registerHelper('blockHelperMissing', function (context, options) {
    var inverse = options.inverse,
        fn = options.fn;

    if (context === true) {
      return fn(this);
    } else if (context === false || context == null) {
      return inverse(this);
    } else if (isArray(context)) {
      if (context.length > 0) {
        if (options.ids) {
          options.ids = [options.name];
        }

        return instance.helpers.each(context, options);
      } else {
        return inverse(this);
      }
    } else {
      if (options.data && options.ids) {
        var data = createFrame(options.data);
        data.contextPath = Utils.appendContextPath(options.data.contextPath, options.name);
        options = { data: data };
      }

      return fn(context, options);
    }
  });

  instance.registerHelper('each', function (context, options) {
    if (!options) {
      throw new _Exception2['default']('Must pass iterator to #each');
    }

    var fn = options.fn,
        inverse = options.inverse,
        i = 0,
        ret = '',
        data = undefined,
        contextPath = undefined;

    if (options.data && options.ids) {
      contextPath = Utils.appendContextPath(options.data.contextPath, options.ids[0]) + '.';
    }

    if (isFunction(context)) {
      context = context.call(this);
    }

    if (options.data) {
      data = createFrame(options.data);
    }

    function execIteration(field, index, last) {
      if (data) {
        data.key = field;
        data.index = index;
        data.first = index === 0;
        data.last = !!last;

        if (contextPath) {
          data.contextPath = contextPath + field;
        }
      }

      ret = ret + fn(context[field], {
        data: data,
        blockParams: Utils.blockParams([context[field], field], [contextPath + field, null])
      });
    }

    if (context && typeof context === 'object') {
      if (isArray(context)) {
        for (var j = context.length; i < j; i++) {
          execIteration(i, i, i === context.length - 1);
        }
      } else {
        var priorKey = undefined;

        for (var key in context) {
          if (context.hasOwnProperty(key)) {
            // We're running the iterations one step out of sync so we can detect
            // the last iteration without have to scan the object twice and create
            // an itermediate keys array.
            if (priorKey) {
              execIteration(priorKey, i - 1);
            }
            priorKey = key;
            i++;
          }
        }
        if (priorKey) {
          execIteration(priorKey, i - 1, true);
        }
      }
    }

    if (i === 0) {
      ret = inverse(this);
    }

    return ret;
  });

  instance.registerHelper('if', function (conditional, options) {
    if (isFunction(conditional)) {
      conditional = conditional.call(this);
    }

    // Default behavior is to render the positive path if the value is truthy and not empty.
    // The `includeZero` option may be set to treat the condtional as purely not empty based on the
    // behavior of isEmpty. Effectively this determines if 0 is handled by the positive path or negative.
    if (!options.hash.includeZero && !conditional || Utils.isEmpty(conditional)) {
      return options.inverse(this);
    } else {
      return options.fn(this);
    }
  });

  instance.registerHelper('unless', function (conditional, options) {
    return instance.helpers['if'].call(this, conditional, { fn: options.inverse, inverse: options.fn, hash: options.hash });
  });

  instance.registerHelper('with', function (context, options) {
    if (isFunction(context)) {
      context = context.call(this);
    }

    var fn = options.fn;

    if (!Utils.isEmpty(context)) {
      if (options.data && options.ids) {
        var data = createFrame(options.data);
        data.contextPath = Utils.appendContextPath(options.data.contextPath, options.ids[0]);
        options = { data: data };
      }

      return fn(context, options);
    } else {
      return options.inverse(this);
    }
  });

  instance.registerHelper('log', function (message, options) {
    var level = options.data && options.data.level != null ? parseInt(options.data.level, 10) : 1;
    instance.log(level, message);
  });

  instance.registerHelper('lookup', function (obj, field) {
    return obj && obj[field];
  });
}

var logger = {
  methodMap: { 0: 'debug', 1: 'info', 2: 'warn', 3: 'error' },

  // State enum
  DEBUG: 0,
  INFO: 1,
  WARN: 2,
  ERROR: 3,
  level: 1,

  // Can be overridden in the host environment
  log: function log(level, message) {
    if (typeof console !== 'undefined' && logger.level <= level) {
      var method = logger.methodMap[level];
      (console[method] || console.log).call(console, message); // eslint-disable-line no-console
    }
  }
};

exports.logger = logger;
var log = logger.log;

exports.log = log;

function createFrame(object) {
  var frame = Utils.extend({}, object);
  frame._parent = object;
  return frame;
}

/* [args, ]options */
},{"./exception":42,"./utils":46}],42:[function(require,module,exports){
'use strict';

exports.__esModule = true;

var errorProps = ['description', 'fileName', 'lineNumber', 'message', 'name', 'number', 'stack'];

function Exception(message, node) {
  var loc = node && node.loc,
      line = undefined,
      column = undefined;
  if (loc) {
    line = loc.start.line;
    column = loc.start.column;

    message += ' - ' + line + ':' + column;
  }

  var tmp = Error.prototype.constructor.call(this, message);

  // Unfortunately errors are not enumerable in Chrome (at least), so `for prop in tmp` doesn't work.
  for (var idx = 0; idx < errorProps.length; idx++) {
    this[errorProps[idx]] = tmp[errorProps[idx]];
  }

  if (Error.captureStackTrace) {
    Error.captureStackTrace(this, Exception);
  }

  if (loc) {
    this.lineNumber = line;
    this.column = column;
  }
}

Exception.prototype = new Error();

exports['default'] = Exception;
module.exports = exports['default'];
},{}],43:[function(require,module,exports){
(function (global){
'use strict';

exports.__esModule = true;
/*global window */

exports['default'] = function (Handlebars) {
  /* istanbul ignore next */
  var root = typeof global !== 'undefined' ? global : window,
      $Handlebars = root.Handlebars;
  /* istanbul ignore next */
  Handlebars.noConflict = function () {
    if (root.Handlebars === Handlebars) {
      root.Handlebars = $Handlebars;
    }
  };
};

module.exports = exports['default'];
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],44:[function(require,module,exports){
'use strict';

var _interopRequireWildcard = function (obj) { return obj && obj.__esModule ? obj : { 'default': obj }; };

exports.__esModule = true;
exports.checkRevision = checkRevision;

// TODO: Remove this line and break up compilePartial

exports.template = template;
exports.wrapProgram = wrapProgram;
exports.resolvePartial = resolvePartial;
exports.invokePartial = invokePartial;
exports.noop = noop;

var _import = require('./utils');

var Utils = _interopRequireWildcard(_import);

var _Exception = require('./exception');

var _Exception2 = _interopRequireWildcard(_Exception);

var _COMPILER_REVISION$REVISION_CHANGES$createFrame = require('./base');

function checkRevision(compilerInfo) {
  var compilerRevision = compilerInfo && compilerInfo[0] || 1,
      currentRevision = _COMPILER_REVISION$REVISION_CHANGES$createFrame.COMPILER_REVISION;

  if (compilerRevision !== currentRevision) {
    if (compilerRevision < currentRevision) {
      var runtimeVersions = _COMPILER_REVISION$REVISION_CHANGES$createFrame.REVISION_CHANGES[currentRevision],
          compilerVersions = _COMPILER_REVISION$REVISION_CHANGES$createFrame.REVISION_CHANGES[compilerRevision];
      throw new _Exception2['default']('Template was precompiled with an older version of Handlebars than the current runtime. ' + 'Please update your precompiler to a newer version (' + runtimeVersions + ') or downgrade your runtime to an older version (' + compilerVersions + ').');
    } else {
      // Use the embedded version info since the runtime doesn't know about this revision yet
      throw new _Exception2['default']('Template was precompiled with a newer version of Handlebars than the current runtime. ' + 'Please update your runtime to a newer version (' + compilerInfo[1] + ').');
    }
  }
}

function template(templateSpec, env) {
  /* istanbul ignore next */
  if (!env) {
    throw new _Exception2['default']('No environment passed to template');
  }
  if (!templateSpec || !templateSpec.main) {
    throw new _Exception2['default']('Unknown template object: ' + typeof templateSpec);
  }

  // Note: Using env.VM references rather than local var references throughout this section to allow
  // for external users to override these as psuedo-supported APIs.
  env.VM.checkRevision(templateSpec.compiler);

  function invokePartialWrapper(partial, context, options) {
    if (options.hash) {
      context = Utils.extend({}, context, options.hash);
    }

    partial = env.VM.resolvePartial.call(this, partial, context, options);
    var result = env.VM.invokePartial.call(this, partial, context, options);

    if (result == null && env.compile) {
      options.partials[options.name] = env.compile(partial, templateSpec.compilerOptions, env);
      result = options.partials[options.name](context, options);
    }
    if (result != null) {
      if (options.indent) {
        var lines = result.split('\n');
        for (var i = 0, l = lines.length; i < l; i++) {
          if (!lines[i] && i + 1 === l) {
            break;
          }

          lines[i] = options.indent + lines[i];
        }
        result = lines.join('\n');
      }
      return result;
    } else {
      throw new _Exception2['default']('The partial ' + options.name + ' could not be compiled when running in runtime-only mode');
    }
  }

  // Just add water
  var container = {
    strict: function strict(obj, name) {
      if (!(name in obj)) {
        throw new _Exception2['default']('"' + name + '" not defined in ' + obj);
      }
      return obj[name];
    },
    lookup: function lookup(depths, name) {
      var len = depths.length;
      for (var i = 0; i < len; i++) {
        if (depths[i] && depths[i][name] != null) {
          return depths[i][name];
        }
      }
    },
    lambda: function lambda(current, context) {
      return typeof current === 'function' ? current.call(context) : current;
    },

    escapeExpression: Utils.escapeExpression,
    invokePartial: invokePartialWrapper,

    fn: function fn(i) {
      return templateSpec[i];
    },

    programs: [],
    program: function program(i, data, declaredBlockParams, blockParams, depths) {
      var programWrapper = this.programs[i],
          fn = this.fn(i);
      if (data || depths || blockParams || declaredBlockParams) {
        programWrapper = wrapProgram(this, i, fn, data, declaredBlockParams, blockParams, depths);
      } else if (!programWrapper) {
        programWrapper = this.programs[i] = wrapProgram(this, i, fn);
      }
      return programWrapper;
    },

    data: function data(value, depth) {
      while (value && depth--) {
        value = value._parent;
      }
      return value;
    },
    merge: function merge(param, common) {
      var obj = param || common;

      if (param && common && param !== common) {
        obj = Utils.extend({}, common, param);
      }

      return obj;
    },

    noop: env.VM.noop,
    compilerInfo: templateSpec.compiler
  };

  function ret(context) {
    var options = arguments[1] === undefined ? {} : arguments[1];

    var data = options.data;

    ret._setup(options);
    if (!options.partial && templateSpec.useData) {
      data = initData(context, data);
    }
    var depths = undefined,
        blockParams = templateSpec.useBlockParams ? [] : undefined;
    if (templateSpec.useDepths) {
      depths = options.depths ? [context].concat(options.depths) : [context];
    }

    return templateSpec.main.call(container, context, container.helpers, container.partials, data, blockParams, depths);
  }
  ret.isTop = true;

  ret._setup = function (options) {
    if (!options.partial) {
      container.helpers = container.merge(options.helpers, env.helpers);

      if (templateSpec.usePartial) {
        container.partials = container.merge(options.partials, env.partials);
      }
    } else {
      container.helpers = options.helpers;
      container.partials = options.partials;
    }
  };

  ret._child = function (i, data, blockParams, depths) {
    if (templateSpec.useBlockParams && !blockParams) {
      throw new _Exception2['default']('must pass block params');
    }
    if (templateSpec.useDepths && !depths) {
      throw new _Exception2['default']('must pass parent depths');
    }

    return wrapProgram(container, i, templateSpec[i], data, 0, blockParams, depths);
  };
  return ret;
}

function wrapProgram(container, i, fn, data, declaredBlockParams, blockParams, depths) {
  function prog(context) {
    var options = arguments[1] === undefined ? {} : arguments[1];

    return fn.call(container, context, container.helpers, container.partials, options.data || data, blockParams && [options.blockParams].concat(blockParams), depths && [context].concat(depths));
  }
  prog.program = i;
  prog.depth = depths ? depths.length : 0;
  prog.blockParams = declaredBlockParams || 0;
  return prog;
}

function resolvePartial(partial, context, options) {
  if (!partial) {
    partial = options.partials[options.name];
  } else if (!partial.call && !options.name) {
    // This is a dynamic partial that returned a string
    options.name = partial;
    partial = options.partials[partial];
  }
  return partial;
}

function invokePartial(partial, context, options) {
  options.partial = true;

  if (partial === undefined) {
    throw new _Exception2['default']('The partial ' + options.name + ' could not be found');
  } else if (partial instanceof Function) {
    return partial(context, options);
  }
}

function noop() {
  return '';
}

function initData(context, data) {
  if (!data || !('root' in data)) {
    data = data ? _COMPILER_REVISION$REVISION_CHANGES$createFrame.createFrame(data) : {};
    data.root = context;
  }
  return data;
}
},{"./base":41,"./exception":42,"./utils":46}],45:[function(require,module,exports){
'use strict';

exports.__esModule = true;
// Build out our basic SafeString type
function SafeString(string) {
  this.string = string;
}

SafeString.prototype.toString = SafeString.prototype.toHTML = function () {
  return '' + this.string;
};

exports['default'] = SafeString;
module.exports = exports['default'];
},{}],46:[function(require,module,exports){
'use strict';

exports.__esModule = true;
exports.extend = extend;

// Older IE versions do not directly support indexOf so we must implement our own, sadly.
exports.indexOf = indexOf;
exports.escapeExpression = escapeExpression;
exports.isEmpty = isEmpty;
exports.blockParams = blockParams;
exports.appendContextPath = appendContextPath;
var escape = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  '\'': '&#x27;',
  '`': '&#x60;'
};

var badChars = /[&<>"'`]/g,
    possible = /[&<>"'`]/;

function escapeChar(chr) {
  return escape[chr];
}

function extend(obj /* , ...source */) {
  for (var i = 1; i < arguments.length; i++) {
    for (var key in arguments[i]) {
      if (Object.prototype.hasOwnProperty.call(arguments[i], key)) {
        obj[key] = arguments[i][key];
      }
    }
  }

  return obj;
}

var toString = Object.prototype.toString;

exports.toString = toString;
// Sourced from lodash
// https://github.com/bestiejs/lodash/blob/master/LICENSE.txt
/*eslint-disable func-style, no-var */
var isFunction = function isFunction(value) {
  return typeof value === 'function';
};
// fallback for older versions of Chrome and Safari
/* istanbul ignore next */
if (isFunction(/x/)) {
  exports.isFunction = isFunction = function (value) {
    return typeof value === 'function' && toString.call(value) === '[object Function]';
  };
}
var isFunction;
exports.isFunction = isFunction;
/*eslint-enable func-style, no-var */

/* istanbul ignore next */
var isArray = Array.isArray || function (value) {
  return value && typeof value === 'object' ? toString.call(value) === '[object Array]' : false;
};exports.isArray = isArray;

function indexOf(array, value) {
  for (var i = 0, len = array.length; i < len; i++) {
    if (array[i] === value) {
      return i;
    }
  }
  return -1;
}

function escapeExpression(string) {
  if (typeof string !== 'string') {
    // don't escape SafeStrings, since they're already safe
    if (string && string.toHTML) {
      return string.toHTML();
    } else if (string == null) {
      return '';
    } else if (!string) {
      return string + '';
    }

    // Force a string conversion as this will be done by the append regardless and
    // the regex test will do this transparently behind the scenes, causing issues if
    // an object's to string has escaped characters in it.
    string = '' + string;
  }

  if (!possible.test(string)) {
    return string;
  }
  return string.replace(badChars, escapeChar);
}

function isEmpty(value) {
  if (!value && value !== 0) {
    return true;
  } else if (isArray(value) && value.length === 0) {
    return true;
  } else {
    return false;
  }
}

function blockParams(params, ids) {
  params.path = ids;
  return params;
}

function appendContextPath(contextPath, id) {
  return (contextPath ? contextPath + '.' : '') + id;
}
},{}],47:[function(require,module,exports){
// Create a simple path alias to allow browserify to resolve
// the runtime on a supported path.
module.exports = require('./dist/cjs/handlebars.runtime')['default'];

},{"./dist/cjs/handlebars.runtime":40}],48:[function(require,module,exports){
var Backbone = require('backbone');
var $ = require('cheerio');
var _ = require('underscore');
var ui = require('./ui');
var Model = require('./model');
require('backbone-super');


var Base = Backbone.View.extend({

	initialize:function(options){

		if(options.el){
			_.extend(options, this.fromDom(options.el));
		}

		this.options = _.extend({}, {
			'classes':''
		}, options);

		this.id = this.options.id || _.uniqueId('ui');
		$(this.el).attr('id', this.id);
		if(options.static != true){
			this.$el.attr('data-ui-init', '');
		}
		this.$el.addClass(this.options.classes);

		if(this.options.static != true){
			ui.registerComponent(this);
		}


		this.model = new Model(this.options.modelOptions || {});
		this.model.set('value', this.options.value || this.options.defaultValue || '');
		this.model.on('change:value', this.applyModelValueToDisplay, this);
		this.model.on('validate:value', this.validated, this);

		this.on('rendered', this.rendered, this);

		this.setBindedModel();

	},

	rendered:function(){
		//this.setDisplayValue();
	},

	setBindedModel:function(){
		if(this.options.model && this.options.attribute){
			this.model.listenTo(this.options.model, 'change:'+this.options.attribute, _.bind(this.applyBindedModelValueToModel, this));
			this.model.listenTo(this.options.model, 'validate:'+this.options.attribute, _.bind(this.validated, this));
			this.applyBindedModelValueToModel();
		}else{
			this.model.set('value', this.options.value || this.options.defaultValue || "");
		}
	},


	applyModelValueToBindedModel:function(event){
		this.options.model.set(this.options.attribute, this.model.get('value'));
	},
	applyBindedModelValueToModel:function(event){
		this.model.set('value', this.options.model.get(this.options.attribute));
	},
	applyDisplayValueToModel:function(event){
		this.model.set('value', this.getDisplayValue());
	},
	applyModelValueToDisplay:function(){
		val = this.model.get('value');
		this.setDisplayValue(val);

		if(this.options.model && this.options.attribute){
			this.applyModelValueToBindedModel();
		}

	},

	error:function(arg){

		if(typeof arg === "boolean"){
			if(arg){
				this._setError(true);
			}else{
				this._setError(false);
			}
		}

		if(typeof arg === "string"){
			this._setError(true, arg);
		}

	},

	/*
	
  ,ad8888ba,                                                88          88
 d8"'    `"8b                                               ""          88
d8'        `8b                                                          88
88          88 8b       d8  ,adPPYba, 8b,dPPYba, 8b,dPPYba, 88  ,adPPYb,88  ,adPPYba,
88          88 `8b     d8' a8P_____88 88P'   "Y8 88P'   "Y8 88 a8"    `Y88 a8P_____88
Y8,        ,8P  `8b   d8'  8PP""""""" 88         88         88 8b       88 8PP"""""""
 Y8a.    .a8P    `8b,d8'   "8b,   ,aa 88         88         88 "8a,   ,d88 "8b,   ,aa
  `"Y8888Y"'       "8"      `"Ybbd8"' 88         88         88  `"8bbdP"Y8  `"Ybbd8"'


	 */
	getDisplayValue:function(){
		return true;
	},
	setDisplayValue:function(value){

	},
	listenToDisplay:function(){

	},
	validated:function(validation){

	},

	_setError:function(isError, message){

	},

	applyDataAttributes:function(options, element){
		options = options || {};
		element = element || this.$el;

		_.each(options, function(value, key){
			if(key.substr(0,5) == "data-"){
				element.attr(key, value);
			}
		});

	},

	dataAttributeMapping:{},

	mapDataAttributes:function(options, mapping){
		if(!mapping){
			mapping = Base.dataAttributeMapping;
		}
		options = options || {};
		var valueMap;
		var keyMap;
		var type;
		_.each(options, function(value, key){

			if(key.substr(0,5) == "data-" && typeof mapping[key.substr(5)] != 'undefined'){
				keyMap = key.substr(5);
				valueMap = mapping[key.substr(5)];
				type = "string";

				if(_.isObject(valueMap)){
					if(valueMap.type == "boolean"){
						value = _.toBoolean(value);
					}
				}
				options[keyMap] = value;
			}
		});

		return options;

	},

	getOptionsFromDom:function(options){
		Base.mapDataAttributes(options);

		return options;
	},
	
	fromDom:function(element){
			
		var options = {};

		if(element.find('.error-message')){
			options.errorMessage = element.find('.error-message').html();
		}

		return options;

	},

	destroy:function(){
		ui.trigger('destroy', this);
	},

});

module.exports = Base;
},{"./model":55,"./ui":61,"backbone":"backbone","backbone-super":4,"cheerio":62,"underscore":"underscore"}],49:[function(require,module,exports){
var Backbone = require('backbone');
var $ = require('cheerio');
var _ = require('underscore');
var ui = require('./ui');
var Model = require('./model');
require('backbone-super');


var Base = Backbone.View.extend({

    UIElement:true,
    UIType:"base",

    initialize:function(options){

        this.options = _.extend({
            className:"base",
            'classes':'',
            'static': ui.static,
            modelOptions:{}
        }, options);

        //set the id
        if(this.options.el && this.options.el[0] && this.options.el[0].id){
            this.id = this.options.el[0].id;
        }else{
            this.id = this.options.id || _.uniqueId('ui');
        }
        $(this.el).attr('id', this.id);
        $(this.el).attr('data-ui', this.options.className);
        this.name = this.options.name || '_'+this.id; // create name from id

        if(this.options.staticModel){
            $(this.el).attr('data-ui-model', this.options.staticModel);
        }
        if(this.options.staticAttribute){
            $(this.el).attr('data-ui-attribute', this.options.staticAttribute);
        }

        //set up models
        if(this.options.static != true){
            if(this.options.initInternalModel != false){
                this.initInternalModel();
                this.bindExternalModel();
            }
            ui.registerComponent(this);
        }

    },


    beforeInternalModelInit:function(){}, // overide

    initInternalModel:function(){

        this.beforeInternalModelInit();

        if(typeof this.options.form == "string" && this.options.form != "" && typeof ui.formElements[this.options.form] != undefined){
            ui.formElements[this.options.form][this.id] = this;
        }

        this.model = new Model({
            value:this.options.value || this.options.defaultValue || ''
        }, this.options.modelOptions || {});
        this.listenTo(this.model, 'change:value', this.applyModelValueToDisplay);
        this.listenTo(this.model, 'validate:value', this.validated);

    },

    bindExternalModel:function(){
        if(this.options.model && this.options.attribute){
            this.stopListening(this.model, 'validate:value');
            this.listenTo(this.options.model, 'change:'+this.options.attribute, this.applyExternalModelValueToModel);
            this.listenTo(this.options.model, 'validate:'+this.options.attribute, this.validated);
            this.applyExternalModelValueToModel();
        }else{
            this.model.set('value', this.options.value || this.options.defaultValue || "");
        }
    },

    validateModel:function(){
        if(this.options.model && this.options.attribute){
            this.options.model.validate();
        }else{
            this.model.validate();
        }
    },

    applyModelValueToExternalModel:function(event){
        this.options.model.set(this.options.attribute, this.model.get('value'));
    },
    applyExternalModelValueToModel:function(event){
        this.model.set('value', this.options.model.get(this.options.attribute));
    },
    applyDisplayValueToModel:function(event){
        this.model.set('value', this.getDisplayValue());
    },
    applyModelValueToDisplay:function(){
        val = this.model.get('value');
        this.setDisplayValue(val);

        if(this.options.model && this.options.attribute){
            this.applyModelValueToExternalModel();
        }

    },

    isError:false,
    error:function(arg){
        if(_.isBoolean(arg)){
            if(arg){
                this.displayError(true);
            }else{
                this.displayError(false);
            }
        }else if(_.isString(arg)){
            this.displayError(true, arg);
        }else{
            this.displayError(false);
        }

    },

    isClean:true,
    dirty:function(){
        this.isClean = false;
    },

    flashError:function(){
        if(this.isError){
            this.$el.addClass('ui-error-show');
            // var self = this;
            // _.delay(function(){
            //     self.$el.removeClass('ui-error-show');
            // },500);
        }
    },




    /*
    
  ,ad8888ba,                                                88          88
 d8"'    `"8b                                               ""          88
d8'        `8b                                                          88
88          88 8b       d8  ,adPPYba, 8b,dPPYba, 8b,dPPYba, 88  ,adPPYb,88  ,adPPYba,
88          88 `8b     d8' a8P_____88 88P'   "Y8 88P'   "Y8 88 a8"    `Y88 a8P_____88
Y8,        ,8P  `8b   d8'  8PP""""""" 88         88         88 8b       88 8PP"""""""
 Y8a.    .a8P    `8b,d8'   "8b,   ,aa 88         88         88 "8a,   ,d88 "8b,   ,aa
  `"Y8888Y"'       "8"      `"Ybbd8"' 88         88         88  `"8bbdP"Y8  `"Ybbd8"'


     */
    getDisplayValue:function(){
        return true;
    },
    setDisplayValue:function(value){

    },
    listenToDisplay:function(){

    },
    validated:function(event){ 
        if(typeof event != 'undefined' && event.valid == false){
            this.error(event.message)
        }else{
            this.error(false);
        }
    },

    displayError:function(isError, message){

    },
    
    focusin:function(){
        this.$el.addClass('ui-focus');
    },

    destroy:function(){
        ui.trigger('destroy', this);
    },
    getConstructor:function(){
        return Base;
    }

}, {

    //get basic options from html
    optionsFromHTML:function($element){
        var options = {};

        //set el from element
        options.el = $element; 

        //get id
        if($element.attr('id') != ''){
            options.id = $element.attr('id');
        }
        if($element.attr('class') != ''){
            options.classes = $element.attr('class');
        }

        if($element.attr('data-form')){
            options.form = $element.attr('data-form');
        }

        if($element.attr('data-ui-model') != ''){
            options.model = ui.models[$element.attr('data-ui-model')];
        }
        if($element.attr('data-ui-attribute') != ''){
            options.attribute = $element.attr('data-ui-attribute');
        }


        if($element.children('.ui-description')){
            options.description = $element.children('.ui-description').html();
        }

        return options;

    }
});

module.exports = Base;
},{"./model":55,"./ui":61,"backbone":"backbone","backbone-super":4,"cheerio":62,"underscore":"underscore"}],50:[function(require,module,exports){
var Base = require('./base2');
var $ = require('cheerio');
var _ = require('underscore');

var ui = require('./ui');

var UIButton = Base.extend({

    UIType:"button",

    tagName:'button',

    initialize:function(options){

        this._super(_.extend({
            defaultValue:"",
            label:"Button",
            type:"button",
            internalModel:false
        }, options));

        if(this.options.menu){
            this.options.menu.closed = true;
            this.menu = new ui.Menu(this.options.menu);
            this.listenTo(this.menu, 'select', function(){
                this.menu.close();
            });
        }

        this.$label = $('<span/>');
    
    },



    render:function(){

        this.$el.empty();
        this.$el.addClass('ui-button');
        this.$el.addClass(this.options.classes);
        this.$el.attr("data-ui", '');
        this.$el.attr("type", this.options.type);

        var classes = [];


        if(this.options.iconleft){
            classes.push('has-icon-left');
            var iconLeft = $('<i class="'+this.options.iconleft+'"></i>');
            this.$el.append(iconLeft);
        }

        if(this.options.icon){
            classes.push('has-icon');
            var iconCenter = $('<i class="'+this.options.icon+'"></i>');
            this.$el.append(iconCenter);
        }

        //append label
        if(this.options.label && this.options.label != '' && this.options.label != null){
            this.$label.text(this.options.label);
            this.$el.append(this.$label);
        }

        if(this.options.iconright){
            classes.push('has-icon-right');
            var iconRight = $('<i class="'+this.options.iconright+'"></i>');
            this.$el.append(iconRight);
        }

        this.$el.addClass(classes.join(' '));

        if(this.menu){
            this.$el.after(this.menu.render().$el);
        }

        this.trigger('render');

        if(!this.options.static){
            this.listenToDisplay();
        }

        return this;

    },


    listenToDisplay:function(){
        this.$el.on('click', _.bind(this.action, this));
        this.$el.on('mousedown', _.bind(this.onMouseDown, this));
    },

    onMouseDown:function(event){

        this.trigger('mousedown', event);

    },

    action:function(){

        if(this.menu){
            this.menu.toggleOpen();
        }

        if(this.options.action){
            this.options.action();
        }

        this.trigger('action');

    },

    _active:false,
    active:function(bool){
        if(bool == true){
            this._active = true;
            this.$el.addClass('ui-active');
        }else if(bool == false){
            this._active = false;
            this.$el.removeClass('ui-active');
        }
        return this._active;
    }   


}, {

    templateHelper:function(data){
        data.hash.static = true;
        return ui.htmlSafe(new UIButton(data.hash).render().$el);
    },

    // newFromHTML:function($element){
    //     var options = Base.optionsFromHTML($element);

    //     if($element.find('label').length){
    //         options.label = $element.find('label').text();
    //     }

    //     var elementFromHTML = new Input(options).render();
    // }

});
 
//ui.registerSelector('.ui-button[data-ui]:not(.ui-init)', _.bind(Input.newFromHTML, Input));
ui.registerHelper('ui-button', _.bind(UIButton.templateHelper, UIButton)); 

module.exports = UIButton;







},{"./base2":49,"./ui":61,"cheerio":62,"underscore":"underscore"}],51:[function(require,module,exports){
var Base = require('./base');
var ui = require('./ui');
var $ = require('cheerio');
var _ = require('underscore');


var Dialog = Base.extend({

	model:null,
	contentIsView:false,
	$content:null,
	$container:null,
 	initialize:function(options){
 		this._super(options);

 		this._super(_.extend({
 			classes:"ui ui-dialog"
 		}, options));

 		this.app = this.options.app || null;

 		this.$containerOuter = $('<div class="container-outer"></div>');
 		this.$overlay = $('<div class="overlay"></div>');
 		this.$containerInner = $('<div class="container-inner"></div>');
 		this.$contentOuter = $('<div class="content-outer"></div>');
 		this.$contentInner = $('<div class="content-inner"></div>');
 		this.$close = $('<div class="close"><i class="icon icon-stacked"><i class="icon icon-close primary"></i><i class="icon icon-close white"></i></i></div>');
 		this.$content = $('<div class="content"></div>');

 		this.$headerTitle = $('<div class="title"></div>');
 		this.$header = $('<div class="header"></div>'); 
 		this.$header.append(this.$headerTitle);
 		this.$footer = $('<div class="footer"></div>');

 		$(window).on('resize.modal', $.proxy(this.resize,this));

 		this.$overlay.click($.proxy(function(){
 			this.hide();
 		},this));
 		this.$close.click($.proxy(function(){
 			this.hide();
 		},this));

 		this.resize();

 	},
 	render:function(){
 		this.$el.empty();
 		this.$el.addClass('ui ui-dialog');
 		this.$el.addClass(this.options.classes);

 		this.$containerOuter.append(this.$overlay);
 		this.$containerOuter.append(this.$containerInner);
 		this.$containerInner.append(this.$contentOuter);
 		this.$contentOuter.append(this.$contentInner);
 		this.$containerInner.prepend(this.$header);
 		this.$containerInner.append(this.$close);
 		this.$contentInner.append(this.$content);
 		this.$containerInner.append(this.$footer);

 		if(this.options.title == undefined && this.options.content.title == undefined){
 			this.options.title = '&nbsp;';
 		}else if(this.options.content.title != undefined){
 			this.$el.addClass('has-title')
 			this.options.title = this.options.content.title;
 		}
 		this.$headerTitle.html(this.options.title);
 		this.$footer.empty();

 		this.options.options = this.options.content.modalOptions || this.options.options;

 		this.setOptions(this.options.options);

 		if(this.options.contentPadding == false){
 			this.$content.addClass('collapse');
 		}
 		if(this.options.fill == true){
 			this.$content.addClass('fill');
 		}

 		this.$el.append(this.$containerOuter);

 		this.setContent(this.options.content);



 		return this;

 	},
 	setContent:function(content){
 		this.$content.empty();
 		//console.log(content)
 		if(typeof content == 'string'){
 			this.$content.append(content);
 		}else if(content.$el != undefined){
 			this.contentIsView = true;
 			this.$content.append(content.$el);
 			content.modal = this;
 		}else{ 
 			this.$content.append(content);
 		} 
 	},
 	show:function(){
 		_.defer($.proxy(function(){
 			this.$el.addClass('active');
 		},this));
 	},
 	hide:function(){
 		this.$el.addClass('hidden');
 		this.trigger('hide');
 		var self = this;
 		_.delay(function(){
 			self.destroy();
 		},250); 
 	},
 	cancel:function(){
 		this.trigger('cancel');
 		if(this.contentIsView && typeof this.options.content.cancel != 'undefined'){
 			this.options.content.cancel();
 		}
 		this.hide();
 	},
 	setOptions:function(options){
 		this.$footer.empty();
 		if(options){
 			this.$el.addClass('has-options');
 			if(this.options.optionsAlign == "center"){
 				this.$footer.addClass('text-center');
 			}else if(this.options.optionsAlign == "right"){
 				this.$footer.addClass('text-right');
 			}
 			_.each(options, function(optionObject){
 				var $button = $('<div class="button">'+optionObject.label+'</div>');
 				if(optionObject.action == "cancel"){
 					$button.addClass('cancel');
 					$button.on('click', $.proxy(this.cancel,this));
 				}else if(optionObject.action != undefined && typeof optionObject.action == "string"){
 					$button.on('click', $.proxy(function(){
 						this.options.content.trigger(optionObject.action);
 					},this));
 					
 				}else if(optionObject.action != undefined && typeof optionObject.action == "function"){
 					$button.on('click', $.proxy(function(){
 						optionObject.action();
 					},this));
 				}
 				this.$footer.append($button);
 			},this);
 		};
 	},
 	destroy:function(){
 		this.$el.remove();
 		$(window).off('resize.dialog');
 		this.trigger('destroy', {dialog:this});
 	},
 	resize:function(){
 		if($(window).width() >= 768 && this.options.width){
 			this.$containerInner.css('max-width', (typeof this.options.width == 'number')?this.options.width+"px":this.options.width );
 		}else{
 			this.$containerInner.css('max-width', "");
 		}
 	},
 	enable:function(){
 		this.$el.removeClass('disabled');
 	},
 	disable:function(){
 		this.$el.addClass('disabled');
 	}

},{

	built:false,

	$container:null,

	currentModal:null,

    dialogs:[],

	buildContainer:function(){
		Dialog.$container = $('<div class="ui ui-dialogs"></div>');
		Dialog.$container.append('<div class="overlay"></div>');
		$('body').append(Dialog.$container);
	},

	push:function(dialogObject){

        ui.trigger('dialog.push');

		if(typeof dialogObject == 'undefined'){
			Dialog.hide();
			return false;
		}

		if(Dialog.built == false){
			Dialog.buildContainer();
			Dialog.built = true;
		}

		if(Dialog.dialogs.length){
			Dialog.dialogs[0].hide();
		}

		Dialog.show();
		
		var newDialog = null;
		if(typeof dialogObject.$el == 'undefined'){
			newDialog = new Dialog(dialogObject).render();
		}else{
			newDialog = dialogObject;
		}

		Dialog.currentDialog = newDialog;
        Dialog.dialogs.push(newDialog);
		Dialog.$container.append(Dialog.currentDialog.$el);
		Dialog.currentDialog.show();
		Dialog.currentDialog.on('hide', Dialog.onhide, Dialog);
		Dialog.currentDialog.on('destroy', Dialog.destroy, Dialog);


	},
	show:function(){

        ui.trigger('dialog.show');

		Dialog.$container.addClass('active');
		$('html,body').addClass('dialog-open');
	},
	hide:function(){
        


		Dialog.currentModal.hide();
		$('html,body').removeClass('dialog-open');
	},
	onhide:function(){

        ui.trigger('dialog.hide');

		$('html,body').removeClass('dialog-open');
	},
	destroy:function(event){



        event.dialog.off('hide');
		event.dialog.off('destroy');
        Dialog.dialogs.shift();
		if(Dialog.dialogs.length == 0){
            Dialog.$container.removeClass('active');
        }
	}

});

module.exports = Dialog;
},{"./base":48,"./ui":61,"cheerio":62,"underscore":"underscore"}],52:[function(require,module,exports){
var _ = require('handlebars/runtime');
module.exports = _.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<script>window._ui_models = window._ui_models || [];\r\nwindow._ui_models.push({\r\n    \"name\":\""
    + escapeExpression(((helper = (helper = helpers.formModelName || (depth0 != null ? depth0.formModelName : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"formModelName","hash":{},"data":data}) : helper)))
    + "\",\r\n    \"attributes\":";
  stack1 = ((helper = (helper = helpers.formData || (depth0 != null ? depth0.formData : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"formData","hash":{},"data":data}) : helper));
  if (stack1 != null) { buffer += stack1; }
  buffer += ",\r\n    \"validation\":";
  stack1 = ((helper = (helper = helpers.formValidation || (depth0 != null ? depth0.formValidation : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"formValidation","hash":{},"data":data}) : helper));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\r\n});\r\n</script>\r\n";
},"useData":true});
},{"handlebars/runtime":47}],53:[function(require,module,exports){
var Backbone = require('backbone');
var $ = require('cheerio');
var _ = require('underscore');
var ui = require('./ui');
require('backbone-super');

var UILayout = Backbone.View.extend({

	namedElements:null,

 	initialize:function(options){

        this.options = _.extend({},{
            classes:'',
            model:null,
            formData:{},
            formValidation:{},
            children:[],
            static:false
        }, options || {});

        this.options.self = this;

 		this.namedElements = {};

 	},

	build:function(){
		this.$el.empty();

		this.$el.append(this.buildFragment(this.options.children, this.$el));
	},

	buildFragment:function(childrenArray, $parentElement){
		UILayout.buildFragment(childrenArray, $parentElement, this.options);
	},

 	get:function(id){
 		return this.namedElements[id];
 	},

 	render:function(){

 		this.$el.addClass(this.options.classes);
 		this.build();

 		return this;
 	}

},{

    renderStatic:function(structure, options){

        options = options || {};
        structure = _.extend({},{
            classes:'',
            formData:{},
            formValidation:{},
            children:[],
            formModelName:'form',
            static:true
        }, structure || {});

        structure.self = this;

        var $root = $('<div/>');
        var $el = $('<div/>');

        if(options.type == "form"){
            $el.addClass('ui-form');
        }
        if(structure.formTagId){
            $el.attr('ui-form-tag-id', structure.formTagId);
        }
        if(structure.formModelName){
            $el.attr('ui-form-model', structure.formModelName);
        }

        $el.append(UILayout.buildFragment(structure.children, $el, structure));
        $root.append($el);
        return $root.html();

    },

    buildFragment:function(childrenArray, $parentElement, options){

        if(Object.prototype.toString.call(childrenArray) === '[object Object]'){
            childrenArray = [childrenArray];
        }

        _.each(childrenArray, function(child){

            if(options.model && typeof child.model == 'undefined'){
                child.model = options.model;
            }

            if(options.formData && child.options){

                if(typeof child.options.name != 'undefined' && typeof options.formData[child.options.name] != 'undefined'){

                    child.options.value = options.formData[child.options.name];
                    child.options.staticModel = options.formModelName;
                    child.options.staticAttribute = child.options.name;

                    if(typeof options.formValidation != 'undefined' && typeof options.formValidation[child.options.name] != 'undefined'){
                        if(options.formValidation[child.options.name].required == true){
                            child.options.required = true;
                        }
                    }

                    if(typeof options.formErrors != 'undefined' && options.formErrors._isValid == false && typeof options.formErrors[child.options.name] != 'undefined'){
                        child.options.errorMessage = options.formErrors[child.options.name].message;
                    }

                    if(typeof options.formTagId != 'undefined'){
                        child.options.form = options.formTagId;
                    }

                }

            }

            child.tagName = child.tagName || "div";
            child.type = child.type || '';
            child.options = child.options || {};
            child.options.static = child.options.static || options.static; 
            if(child.type.indexOf('ui.') != -1){
                var $item = new ui[child.type.split('.')[1]](child).render();
                $item.$el.attr('id', child.name);
            }else if(child.ui){ 
                child.options = child.options || {};
                var $item = new ui[child.ui](child.options).render();
                $item.$el.attr('id', child.name);
            }else if(child.type == "html"){
                var $item = $(child.html);
                $item.attr('id', child.name);
                $item.addClass(child.type);
            }else{
                var $item = $('<'+child.tagName+'/>');
                $item.attr('id', child.name);
                $item.addClass(child.type);
            }

            if(!options.static && child.name){
                this.namedElements[child.name] = $item;
            }else{
                child.name = _.uniqueId("e");
            }
            

            if(!options.static && child.events){
                _.each(child.events, function(eventObject){
                    $('body').on(eventObject.event, '#'+child.name, eventObject.callback);
                });
            }

            if(child.classes){
                $item.addClass(child.classes);
            }
            if(child.text){
                $item.text(child.text);
            }

            if(child.iconLeft){
                $item.prepend('<i class="'+child.iconLeft+'"></i>&nbsp;');
            }

            if(child.children != undefined){ 
                if( _.keys(child.children).length ){
                    $item.append(UILayout.buildFragment(child.children, $item, options));
                }
            }

            if($item.$el != undefined){
                $parentElement.append($item.$el);
            }else{
                $parentElement.append($item);
            }

        }, options.self);

    },



});

module.exports = UILayout;

},{"./ui":61,"backbone":"backbone","backbone-super":4,"cheerio":62,"underscore":"underscore"}],54:[function(require,module,exports){
var Base = require('./base2');
var $ = require('cheerio');
var _ = require('underscore');

var ui = require('./ui');

var UIMenu = Base.extend({

    UIType:"select",

    initialize:function(options){

        this._super(_.extend({
            className:"Menu",
            defaultValue:"",
            items:[]
        }, options));
          
        this.$items = $('<div/>');
        this.$items.attr("class", "ui-menu-items");

        this.itemsHash = {};

        this.on('render', function(){
            this.applyModelValueToDisplay();
        });

        if(this.options.closed){
            this.isOpen = false;
        }else{
            this.isOpen = true;
        }

    },

    beforeInternalModelInit:function(){
        if(this.options.validation){
            if(typeof this.options.modelOptions.validation == 'undefined'){
                this.options.modelOptions.validation = {};
            }
            this.options.modelOptions.validation['value'] = this.options.validation;
        }
    }, 

    toggleOpen:function(){
        if(this.isOpen){
            this.close();
        }else{
            this.open();
        }
    },
    open:function(animated){
        animated = animated || true;
        this.$el.toggleClass('ui-animated', animated);
        this.$el.removeClass('ui-closed');
        this.$el.addClass('ui-open');
        this.isOpen = true;
        this.$el.height(this.$items.outerHeight());
        _.delay(_.bind(this.focus,this), 100);



    },

    close:function(animated){
        this.isOpen = false;
        animated = animated || true;
        this.$el.toggleClass('ui-animated', animated);
        this.$el.addClass('ui-closed');
        this.$el.removeClass('ui-open');
        this.$el.height('0px');
    },

    render:function(){

        this.$el.empty();
        this.$el.addClass('ui-menu');
        this.$el.attr('tabindex', -1);
        this.$el.addClass(this.options.classes);
        this.$el.attr("data-ui", '');

        var classes = [];

        //append label
        if(this.options.label){
            this.$el.append(this.$label);
            this.$label.text(this.options.label);
        }

        //render items
        this.renderItems();

        if(this.isOpen == false){
            this.close(false);
        }

        this.trigger('render');

        if(!this.options.static){
            this.listenToDisplay();
        }

        return this;
    },

    renderItems:function(){
        this.$items.empty();
        this.itemsHash = {};

        _.each(this.options.items, function(itemObject, i){

            var button = new ui.Button({
                classes:"ui-menu-item",
                label:itemObject.label,
            }).render();

            if(!this.options.static){
                this.listenTo(button, 'action', function(){
                    this.model.set('value', itemObject.value);
                    this.trigger('select', {value:itemObject.value, button:button});
                }); 
            }

            button.on('mousedown', function(event){
                event.preventDefault();
                event.stopPropagation();
                return false;
            });

            this.$items.append(button.$el);
            this.itemsHash[itemObject.value.toString()] = button;

        },this);

        this.$el.append(this.$items);

    },


    setDisplayValue:function(value){
        value = value.toString();
        _.each(this.itemsHash, function(item, key){
            if(key == value){
                item.active(true);
            }else{
                item.active(false);
            }
        });
    },
    // getDisplayValue:function(){
    //     return this.$input.val();
    // },

    listenToDisplay:function(){
        this.$el.addClass("ui-init");
        ui.$body.on('focusin focus', '#'+this.id, _.bind(this.focusin, this));
        ui.$body.on('focusout', '#'+this.id, _.bind(this.focusout, this));
    },



    focus:function(event){
        $('#'+this.id).focus();
    },
    focusin:function(){
        this.$el.addClass('ui-focus');
    },
    focusout:function(event){
        this.$el.removeClass('ui-focus');
        if(this.options.closed){
            this.close();
        }
    },

}, {

    templateHelper:function(data){
        data.hash.static = true;
        var element = new UIMenu(data.hash).render();
        return new Handlebars.SafeString(ui.html(element.$el));
    },

    newFromHTML:function($element){
        var options = Base.optionsFromHTML($element);

        if($element.find('label').length){
            options.label = $element.find('label').text();
        }

        var elementFromHTML = new UIMenu(options).render();
    }

});
  
ui.registerHelper('ui-select', _.bind(UIMenu.templateHelper, UIMenu)); 

module.exports = UIMenu;







},{"./base2":49,"./ui":61,"cheerio":62,"underscore":"underscore"}],55:[function(require,module,exports){
var Backbone = require('backbone');
var $ = require('cheerio');
var _ = require('underscore');
var ui = require('./ui');
require('backbone-super');

var UIModel = Backbone.Model.extend({

	initialize:function(attributes, options){

		this.options = options || {};

		this.validation = this.options.validation;
		this.on('change', this.change, this);
        this.validate();
 
	},

	change:function(){
		this.validate();
	},
	validate:function(){
		this.isValid();
	},
	isValid:function(){  

        this.errors = _.bind(UIModel.validate, this)(this.attributes, this.validation, {
            triggerEvents:true
        });
		this.trigger("validate", this.errors);

        if(this.errors && this.errors._isValid == false){
            return false;
        }else{
            return true;
        }

	},

	validateAttribute:function(key, options){
		return UIModel.validateAttribute(key, options);
	},

},{

    validate:function(attributes, _rules, _options){

        var rules = _rules || {};
        var options = _options || {};
        options.triggerEvents =  options.triggerEvents || false;
        var modelValidation = {};

        _.each(rules, function(keyOptions, key){
            var validated = UIModel.validateAttribute(key, attributes[key], rules[key]);
            if(validated.valid == false){
                modelValidation._isValid = false;
                modelValidation[key] = validated;
            }
            if(options.triggerEvents){
                this.trigger('validate:'+key, validated);
            }
        },this);

        return modelValidation;
    },

    validateAttribute:function(key, value, _options){
        
        var options = _options || {};

        var validation = {valid:true, message:''};

        var isEmpty = !UIModel.isEmpty(value).valid;

        if(validation.valid && options.pattern && isEmpty == false){
            validation = UIModel.isEmail(value);
        }
        if(validation.valid && options.allowedValue && isEmpty == false){
            validation = UIModel.isMatch(value, options.allowedValue);
        }
        if(validation.valid && options.required && isEmpty == true){
            validation = UIModel.isEmpty(value);
        }

        if(!validation.valid && typeof options.message != 'undefined'){
            validation.message = options.message;
        }

        return validation;

    },


	isEmpty:function(value){
        var valid = true;
        if(typeof value == "undefined" || value.trim().length == 0){
            valid = false;
        }
		return UIModel.returnValidation(valid, UIModel.messages.isEmpty);
	},

    _isMatch:function(value1, value2){
        if(value1 == value2){
            return true;
        }else{
            return false;
        }
    }, 

    isMatch:function(value, allowedValue){
        var valid = false;
        if(_.isArray(allowedValue)){
            var found = false;
            _.each(allowedValue, function(value2){
                if(!found){
                    found = UIModel._isMatch(value, value2);
                }
            });
            if(found){
                valid = true;
            }
        }else{
            valid = UIModel._isMatch(value, allowedValue);
        }

        return UIModel.returnValidation(valid, UIModel.messages.isMatch);

    },


	isEmail:function(value){
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		var valid = re.test(value);

		return UIModel.returnValidation(valid, UIModel.messages.isEmail);
	},

	returnValidation:function(isValid, message){
		if(isValid){
			return {
				valid:true
			}
		}else{
			return {
				valid:false,
				message:message
			}
		}
	},

	messages:{
		isEmpty:"This is required.",
        isEmail:"Must be a valid email.",
		isMatch:"The value does not match allowed values.",
	}


});

module.exports = UIModel;
},{"./ui":61,"backbone":"backbone","backbone-super":4,"cheerio":62,"underscore":"underscore"}],56:[function(require,module,exports){
var Base = require('./base2');
var $ = require('cheerio');
var _ = require('underscore');

var ui = require('./ui');

var UIRadioGroup = Base.extend({

    UIType:"radio-group",

    initialize:function(options){

        this._super(_.extend({
            className:'RadioGroup',
            defaultValue:"",
            items:[] 
        }, options));

        this.$errorMessage = $('<div class="ui-error-message"></div>');
        
        this.$items = $('<div/>');
        this.$items.attr('class', 'ui-radio-group-items');
        this.itemsHash = {};

        this.$label = $('<label/>');
        this.$label.attr('for', this.name);
        this.$label.attr('data-name', this.name);

        if(this.options.description){
            this.$description = $('<div class="ui-description"></div>');
        }

        if(this.options.closed){
            this.isOpen = false;
        }else{
            this.isOpen = true;
        }

        this.on('render', function(){
            this.applyModelValueToDisplay();
            this.validateModel();
        });

    },

    beforeInternalModelInit:function(){
        if(this.options.validation){
            if(typeof this.options.modelOptions.validation == 'undefined'){
                this.options.modelOptions.validation = {};
            }
            this.options.modelOptions.validation['value'] = this.options.validation;
        }
    }, 

    toggleOpen:function(){
        if(this.isOpen){
            this.close();
        }else{
            this.open();
        }
    },

    open:function(animated){
        animated = animated || true;
        this.$el.toggleClass('ui-animated', animated);
        this.$el.removeClass('ui-closed');
        this.$el.addClass('ui-open');
        this.isOpen = true;
        this.$el.height(this.$items.outerHeight());
        _.delay(_.bind(this.focus,this), 100);
    },

    close:function(animated){
        this.isOpen = false;
        animated = animated || true;
        this.$el.toggleClass('ui-animated', animated);
        this.$el.addClass('ui-closed');
        this.$el.removeClass('ui-open');
        this.$el.height('0px');
    },

    render:function(){

        this.$el.empty();
        this.$el.addClass('ui-radio-group');
        this.$el.addClass(this.options.classes);
        if(typeof this.options.form == "string" && this.options.form != "" ){
            this.$el.attr("data-form", this.options.form);
        }
        if(this.options.required){
            this.$el.attr('data-ui-required', '');
            this.$el.addClass('ui-required');
        }
        var classes = [];

        //append label
        if(this.options.label){
            this.$el.append(this.$label);
            this.$label.text(this.options.label);
        }

        //append description
        if(this.options.description){
            this.$el.append(this.$description);
            this.$description.html(this.options.description);
        }

        //render items
        this.renderItems();

        if(this.isOpen == false){
            this.close(false);
        }

        if(typeof this.options.errorMessage != 'undefined'){
            this.error(this.options.errorMessage);
        }


        if(!this.options.static){
            this.trigger('render');
            this.listenToDisplay();
        }else{
            this.setDisplayValue(this.options.value);
        }

        return this;
    },

    renderItems:function(){
        this.$items.empty();
        this.itemsHash = {};
        _.each(this.options.items, function(itemObj, i){

            var toggle = new ui.Toggle({
                classes:"ui-radio-group-item ui-child",
                label:itemObj.label,
                style:"radio",
                name:this.name,
                value:itemObj.value,
                static:this.options.static || ui.static
            }).render();

            if(!this.options.static){ 
                this.listenTo(toggle, 'activate', function(){
                    this.model.set('value', itemObj.value);
                    this.trigger('select', {value:itemObj.value, toggle:toggle});
                }); 
            }

            this.$items.append(toggle.$el);
            this.itemsHash[itemObj.value.toString()] = toggle;

        },this);

        if(this.options.other){

            this.options.other.placeholder = this.options.other.placeholder || "Other";

            var toggle = new ui.Toggle({
                classes:"ui-radio-group-item ui-child",
                style:"radio",
                name:this.name,
                value:'other',
                static:this.options.static || ui.static,
                inputacc:_.extend({
                    placeholder:this.options.other.placeholder,
                    model:this.model,
                    attribute:"other"
                },this.options.other)
            }).render();

            if(!this.options.static){ 
                this.listenTo(toggle, 'activate', function(){
                    this.model.set('value', 'other');
                    this.trigger('select', {value:'other', toggle:toggle});
                }); 
            }

            this.$items.append(toggle.$el);
            this.itemsHash['other'] = toggle;
        } 

        this.$el.append(this.$items);

    },


    displayError:function(isError, message){
        if(isError){
            this.isError = true;
        }else{
            this.isError = false;
            this.$el.removeClass('ui-error'); 
            this.$el.removeClass('ui-error-show');
        }
        if(this.isClean == true && this.options.static == false){
            return false;
        } 

        if(!this.options.static){
            if(this.options.error && this.model.get('value') == this.options.errorValue){
                if(!this.$el.hasClass("ui-focus")){
                    this.$el.addClass('ui-error');   
                }
                this.$errorMessage.html(message);
                this.$items.before(this.$errorMessage);
                return false;
            }else{
                this.options.error = null;
            }
        }
        if(isError){
            this.$errorMessage.html(message);
            this.$items.before(this.$errorMessage);
            if(!this.$el.hasClass("ui-focus")){
                this.$el.addClass('ui-error');   
            }
        }else{
            this.$el.removeClass('ui-error');
            this.$errorMessage.remove();
        }
    },


    setDisplayValue:function(value){
        value = value || "";
        value = value.toString();
        _.each(this.itemsHash, function(item, key){
            if(key == value){
                item.active(true);
            }else{
                item.active(false);
            }
        });
    },
    // getDisplayValue:function(){
    //     return this.$input.val();
    // },

    listenToDisplay:function(){
        this.$el.addClass("ui-init");
        ui.$body.on('focusin focus', '#'+this.id, _.bind(this.focusin, this));
        ui.$body.on('focusout', '#'+this.id, _.bind(this.focusout, this));
    },



    focus:function(event){
        $('#'+this.id).focus();
    },
    focusin:function(){
        this.$el.addClass('ui-focus');
    },
    focusout:function(event){
        this.$el.removeClass('ui-focus');
        if(this.options.closed){
            this.close();
        }
    },

}, {

    templateHelper:function(data){
        data.hash.static = true;
        return ui.htmlSafe(new UIInput(data.hash).render().$el);
    },

    newFromHTML:function($element){
        var options = Base.optionsFromHTML($element);

        if($element.children('label').length){
            options.label = $element.children('label').text();
        }

        options.items = [];
        $element.find('input[type="radio"]').each(function(index, optionEl){

            options.items.push({
                label:$(optionEl).attr('data-label'),
                value:optionEl.value
            });
            if(optionEl.checked == true){
                options.value = optionEl.value;
            }

        });
        
        if($element.attr('data-ui-attribute') != ''){
            options.name = $element.attr('data-ui-attribute');
        }

        var elementFromHTML = new UIRadioGroup(options).render();
    }




});

ui.registerHelper('ui-radio-group', _.bind(UIRadioGroup.templateHelper, UIRadioGroup)); 

module.exports = UIRadioGroup;







},{"./base2":49,"./ui":61,"cheerio":62,"underscore":"underscore"}],57:[function(require,module,exports){
(function (process){
var Base = require('./base2');
var $ = require('cheerio');
var _ = require('underscore');

var ui = require('./ui');

var UISelect = Base.extend({

    UIType:"select",

    initialize:function(options){

        this._super(_.extend({
            className:"Select",
            defaultValue:"",
            items:[]
        }, options));
          
        this.$errorMessage = $('<div class="ui-error-message"></div>');

        this.selectId = 'display_'+this.id;
        this.$select = $('<select/>');
        this.$select.attr("class", 'ui-display');
        this.$select.attr("id", this.selectId);
        this.$select.attr("name", this.name);

        this.$menuEl = $('<div/>');

        this.$label = $('<label/>');
        this.$label.attr('for', this.selectId);

        this.$icon = $('<i class="ui-select-icon fa fa-chevron-down"></i>');

        if(this.options.error){
            this.options.errorValue = this.options.value;
        }

        this.on('render', function(){
            this.applyModelValueToDisplay();
            this.validateModel();
        });



    },

    beforeInternalModelInit:function(){
        if(this.options.validation){
            if(typeof this.options.modelOptions.validation == 'undefined'){
                this.options.modelOptions.validation = {};
            }
            this.options.modelOptions.validation['value'] = this.options.validation;
        }
    }, 

    initInternalModel:function(){
        this._super();
        this.menu = new ui.Menu({
            el:this.$menuEl,
            model:this.model,
            attribute:"value",
            items:this.options.items,
            closed:true
        });

        this.listenTo(this.menu, 'select', function(){
            this.menu.close();
        });

    },

    render:function(){

        this.$el.empty();
        this.$el.addClass('ui-select');
        this.$el.addClass(this.options.classes);

        var classes = [];

        //append label
        if(this.options.label){
            this.$el.append(this.$label);
            this.$label.text(this.options.label);
        }

        //render select
        this.$el.append(this.renderSelect()); 

        if(this.options.iconprefix){
            classes.push('ui-has-acc-left');
            var iconLeft = $('<div class="ui-acc ui-acc-icon ui-acc-left"><i class="'+this.options.iconprefix+'"></i></div>');
            this.attachAccessory(iconLeft);
            this.$select.before(iconLeft);
        }

        if(this.options.iconsuffix){
            classes.push('ui-has-acc-right');
            var iconRight = $('<div class="ui-acc ui-acc-icon ui-acc-right"><i class="'+this.options.iconsuffix+'"></i></div>');
            this.attachAccessory(iconRight);
            this.$select.after(iconRight);
        }

        if(this.options.prefix && _.isString(this.options.prefix)){
            classes.push('ui-has-acc-left');
            var prefix = $('<div class="ui-acc ui-acc-label ui-acc-left">'+this.options.prefix+'</div>');
            this.attachAccessory(prefix);
            this.$select.before(prefix);
        }
        if(this.options.suffix && _.isString(this.options.suffix)){
            classes.push('ui-has-acc-right');
            var suffix = $('<div class="ui-acc ui-acc-label ui-acc-right">'+this.options.suffix+'</div>');
            this.attachAccessory(suffix);
            this.$select.after(suffix);
        }


        if(this.options.suffix && this.options.suffix.$el){
            classes.push('ui-has-acc-right');

            if(this.options.suffix.UIElement && this.options.suffix.UIType == "button"){
                var suffix = $('<div class="ui-acc ui-acc-button ui-acc-right"></div>');
            }
            suffix.append(this.options.suffix.$el);
            this.$select.after(suffix);
        }

        if( (this.options.errorMessage || this.options.errormessage) && this.options.errorMessage ){
            this.error(this.options.errorMessage);
        }

        this.$el.addClass(classes.join(' '));

        this.$el.append(this.$icon);
        

        if(!this.options.static){
            this.trigger('render');
            this.$select.after(this.menu.render().$el);
            this.listenToDisplay();
        }else{
            this.setDisplayValue(this.options.value);
        }

        return this;
    },

    renderSelect:function(){
        this.$select.empty();

        if(this.options.placeholder){
            var $option = $('<option>'+this.options.placeholder+'</option>');
            $option.attr('value', "");

            this.$select.append($option);
        }

        _.each(this.options.items, function(itemObjs, index){

            var $option = $('<option>'+itemObjs.label+'</option>');
            $option.attr('value', itemObjs.value);
            this.$select.append($option);

        },this);

        return this.$select;

    },
 
    afterRender:function(){
        this._super();
        if(!this.options.static){
            this.listenToDisplay();
        }
        if(this.options.error){
            this.error(this.options.error);
        }
    },

    setDisplayValue:function(value){

        if(this.$select){
            //this.$select.val(value);
            this.$select.attr('value',value);
            //this.$select[0].value = value;
            this.$select.find('option[value="'+value+'"]').attr('selected', '');
        }

        if(value == ''){
            this.$el.addClass('ui-empty-value');
        }else{
            this.$el.removeClass('ui-empty-value');
        }

    },
    getDisplayValue:function(){
        return this.$select.val();
    },

    listenToDisplay:function(){

        this.$el.addClass("ui-init");
        ui.$body.on('change keyup', '#'+this.selectId, _.bind(function(event){
            this.applyDisplayValueToModel();
        }, this));

        var self = this;
        ui.$body.on('mousedown', '#'+this.selectId, function(event){
            event.preventDefault();
            event.stopPropagation();
            return false;
        });
        ui.$body.on('click', '#'+this.selectId, function(event){
            event.preventDefault();
            event.stopPropagation();
            self.menu.toggleOpen();
            return false;
        });
        ui.$body.on('keydown', '#'+this.selectId, function(event){
            var code = event.keyCode || event.which;
            if(code == 13) { //Enter keycode
                event.preventDefault();
                event.stopPropagation();
                self.menu.toggleOpen();
                return false;
            }
        });
        ui.$body.on('focusin', '#'+this.selectId, _.bind(this.focusin, this));
        ui.$body.on('focusout', '#'+this.selectId, _.bind(this.focusout, this));

    },

    displayError:function(isError, message){

        if(this.options.error && this.model.get('value') == this.options.errorValue){
            if(!this.$el.hasClass("ui-focus")){
                this.$el.addClass('error');   
            }
            this.$errorMessage.html(message);
            this.$el.append(this.$errorMessage);
            return false;
        }else{
            this.options.error = null;
        }

        if(isError){
            if(!this.$el.hasClass("ui-focus")){
                this.$el.addClass('error');   
            }
            this.$errorMessage.html(message);
            this.$el.append(this.$errorMessage);
        }else{
            this.$el.removeClass('error');
            this.$errorMessage.remove();
        }
    },

    focus:function(event){
        this.$select.focus();
    },

    focusin:function(){
        this._super();
        this.$el.addClass('ui-focus');
        this.menu.focus();
    },

    focusout:function(event){
        if(!this.menu.isOpen){
            this.$el.removeClass('ui-focus');
            this.menu.close();
            this.validateModel();
        }else{
            console.log('select focusout')
        }
    },

    attachAccessory:function($element){
        if(process.browser){
            var id = _.uniqueId('acc');
            $element.attr('id', id);

            ui.$body.on('click', '#'+id, $.proxy(this.focus, this));
        }
    },

}, {

    templateHelper:function(data){
        data.hash.static = true;
        var element = new UISelect(data.hash).render();
        return new Handlebars.SafeString(ui.html(element.$el));
    },

    newFromHTML:function($element){
        var options = Base.optionsFromHTML($element);

        if($element.find('label').length){
            options.label = $element.find('label').text();
        }

        options.items = [];
        $element.find('select option').each(function(index, optionEl){
            if(optionEl.value != ''){ 
                options.items.push({
                    label:optionEl.text,
                    value:optionEl.value
                });
            }else{
                options.placeholder = optionEl.text;
            }
        });

        options.name = $element.find('select').attr('name');
        options.value = $element.find('select').val();

        var elementFromHTML = new UISelect(options).render();
    }

});
  
ui.registerHelper('ui-select', _.bind(UISelect.templateHelper, UISelect)); 

module.exports = UISelect;







}).call(this,require('_process'))
},{"./base2":49,"./ui":61,"_process":39,"cheerio":62,"underscore":"underscore"}],58:[function(require,module,exports){
var Backbone = require('backbone');
var $ = require('cheerio');
var _ = require('underscore');
var ui = require('./ui');

var formHeadTemplate = require('./hbs/static-form-head');

var UIStaticForm = {

    render:function(json, callback){

        json = _.extend({
            formData:{},
            formValidation:{},
            formErrors:{},
            isValid:false,
            validate:false,
            formTagId:"form"
        }, json);

        if(json.validate == true){
            json.formErrors = ui.Model.validate(json.formData, json.formValidation);
            if(json.formErrors._isValid == false){
                json.isValid = false;
            }else{
                json.isValid = true;
            }
        }else{

        }

        var response = {
            isValid:json.isValid,
            html:''
        };

        if(json.isValid == false){
            response.html = formHeadTemplate({
                formModelName:json.formModelName,
                formData:JSON.stringify(json.formData),
                formValidation:JSON.stringify(json.formValidation)
            });
            response.html += ui.Layout.renderStatic(json, {
                type:"form",
            });
            if(json.actions){
                response.html += ui.Layout.renderStatic({
                    children:json.actions
                });
            }
        }

        if(callback){
            callback(null, response);
        }
        return response;


    }

};

module.exports = UIStaticForm;

},{"./hbs/static-form-head":52,"./ui":61,"backbone":"backbone","cheerio":62,"underscore":"underscore"}],59:[function(require,module,exports){
(function (process){
var Base = require('./base2');
var ui = require('./ui');
var $ = require('cheerio');
var _ = require('underscore');

var UIInput = Base.extend({

    UIType:"input",

	initialize:function(options){

		this._super(_.extend({
            className:'Input',
			defaultValue:"",
            type:"text",
		}, options));

        //console.log(this.options)
		  
        this.$errorMessage = $('<div class="ui-error-message"></div>');
        
        this.inputId = 'input_'+this.id;
        this.$input = $('<input/>');
        this.$input.attr('id', this.inputId);
        this.$input.attr('type', this.options.type);
        this.$input.attr('name', this.name);

        this.$label = $('<label/>');
        this.$label.attr('for', this.inputId);


        if(this.options.readonly){
            this.$input.attr('readonly', true);
        }
        if(this.options.placeholder){
            this.$input.attr('placeholder', this.options.placeholder);
        }
        if(this.options.autocomplete){
            this.$input.attr('autocomplete', true);
        }


        if(this.options.error){
            this.options.errorValue = this.options.value;
        }

        this.on('render', function(){
            this.applyModelValueToDisplay();
            this.validateModel();
        });
        
        // if(this.options.autoselect){
        //     this.$input.attr('onClick', 'this.setSelectionRange(0, this.value.length)');
        // }


	},

    beforeInternalModelInit:function(){
        if(this.options.validation){
            if(typeof this.options.modelOptions.validation == 'undefined'){
                this.options.modelOptions.validation = {};
            }
            this.options.modelOptions.validation['value'] = this.options.validation;
        }

    }, 



	render:function(){

		this.$el.empty();
		this.$el.addClass('ui-input');
		this.$el.addClass(this.options.classes);
        if(typeof this.options.form == "string" && this.options.form != "" ){
            this.$el.attr("data-form", this.options.form);
        }
        if(this.options.required){
            this.$el.attr('data-ui-required', '');
            this.$el.addClass('ui-required');
        }

		var classes = [];

        //append label
        if(this.options.label){
            this.$el.append(this.$label);
            this.$label.text(this.options.label);
		}

        //append input
		this.$el.append(this.$input);

		if(this.options.iconprefix){
			classes.push('ui-has-acc-left');
			var iconLeft = $('<div class="ui-acc ui-acc-icon ui-acc-left"><i class="'+this.options.iconprefix+'"></i></div>');
			this.attachAccessory(iconLeft);
			this.$input.before(iconLeft);
		}

		if(this.options.iconsuffix){
			classes.push('ui-has-acc-right');
			var iconRight = $('<div class="ui-acc ui-acc-icon ui-acc-right"><i class="'+this.options.iconsuffix+'"></i></div>');
			this.attachAccessory(iconRight);
			this.$input.after(iconRight);
		}

		if(this.options.prefix && _.isString(this.options.prefix)){
			classes.push('ui-has-acc-left');
			var prefix = $('<div class="ui-acc ui-acc-label ui-acc-left">'+this.options.prefix+'</div>');
			this.attachAccessory(prefix);
			this.$input.before(prefix);
		}
		if(this.options.suffix && _.isString(this.options.suffix)){
			classes.push('ui-has-acc-right');
			var suffix = $('<div class="ui-acc ui-acc-label ui-acc-right">'+this.options.suffix+'</div>');
			this.attachAccessory(suffix);
			this.$input.after(suffix);
		}


        if(this.options.suffix && this.options.suffix.$el){
            classes.push('ui-has-acc-right');

            if(this.options.suffix.UIElement && this.options.suffix.UIType == "button"){
                var suffix = $('<div class="ui-acc ui-acc-button ui-acc-right"></div>');
            }
            if(this.options.suffix.UIElement && this.options.suffix.UIType == "select"){
                var suffix = $('<div class="ui-acc ui-acc-select ui-acc-right"></div>');
            }
            suffix.append(this.options.suffix.render().$el);
            this.$input.after(suffix);
        }



		if(typeof this.options.errorMessage != 'undefined'){
			this.error(this.options.errorMessage);
		}

		this.$el.addClass(classes.join(' '));


        if(!this.options.static){
            this.trigger('render');
            this.listenToDisplay();
        }else{
            this.setDisplayValue(this.options.value);
        }

        if(this.options.error){
            this.error(this.options.error);
        }

		return this;
	},
 

	setDisplayValue:function(value){
		if(this.$input){
			this.$input.val(value);
			this.$input.attr('value',value);
		}
	},

	getDisplayValue:function(){
		return this.$input.val();
	},

	listenToDisplay:function(){

        this.$el.addClass("ui-init");
		ui.$body.on('change keyup', '#'+this.inputId, _.bind(function(event){
            this.dirty();
			this.applyDisplayValueToModel();
		}, this));

		ui.$body.on('focusin', '#'+this.inputId, _.bind(this.focusin, this));
		ui.$body.on('focusout', '#'+this.inputId, _.bind(this.focusout, this));

	},

	displayError:function(isError, message){
        
        if(isError){
            this.isError = true;
        }else{
            this.isError = false;
        }
        if(this.isClean == true && this.options.static == false){
            return false;
        } 

        if(!this.options.static){
    		if(this.options.error && this.model.get('value') == this.options.errorValue){
                if(!this.$el.hasClass("ui-focus")){
                    this.$el.addClass('ui-error');   
                }
    			this.$errorMessage.html(message);
    			this.$el.append(this.$errorMessage);
    			return false;
    		}else{
    			this.options.error = null;
    		}
        }

		if(isError){
            if(!this.$el.hasClass("ui-focus")){
                this.$el.addClass('ui-error');   
            }
            this.$errorMessage.html(message);
            this.$el.append(this.$errorMessage);
		}else{
			this.$el.removeClass('ui-error');
			this.$errorMessage.remove();
		}
	},

	focus:function(event){
		this.$input.focus();
	},

    focusin:function(){
        this._super();
        this.$el.removeClass('ui-error-show');
        this.$el.addClass('ui-focus');
        this.trigger('focusin');
    },

    focusout:function(event){
        this.$el.removeClass('ui-focus');
        this.validateModel();
        this.trigger('focusout');
    },

	attachAccessory:function($element){
		if(process.browser){
			var id = _.uniqueId('acc');
			$element.attr('id', id);

			ui.$body.on('click', '#'+id, $.proxy(this.focus, this));
		}
	},

}, {

	templateHelper:function(data){
		data.hash.static = true;
        return ui.htmlSafe(new UIInput(data.hash).render().$el);
	},

    newFromHTML:function($element){
        var options = Base.optionsFromHTML($element);

        if($element.find('label').length){
            options.label = $element.find('label').text();
        }

        options.name = $element.find('input').attr('name');
        if($element.attr('data-ui-required')){
            options.required = true;
        }
        options.value = $element.find('input').val();

        var elementFromHTML = new UIInput(options).render();
    }

});

ui.registerHelper('ui-input', _.bind(UIInput.templateHelper, UIInput)); 

module.exports = UIInput;







}).call(this,require('_process'))
},{"./base2":49,"./ui":61,"_process":39,"cheerio":62,"underscore":"underscore"}],60:[function(require,module,exports){
var Base = require('./base2');
var $ = require('cheerio');
var _ = require('underscore');

var ui = require('./ui');

var UIToggle = Base.extend({

    UIType:"toggle",

    initialize:function(options){

        this._super(_.extend({
            className:'Toggle',
            defaultValue:"",
            label:"Toggle",
            style:"checkbox",
            onValue:'on',
            offValue:'off'
        }, options));

        this.$errorMessage = $('<div class="ui-error-message"></div>');

        this.toggleId = 'input_'+this.id;
        this.$toggle = $('<input/>');
        this.$toggle.attr('id', this.toggleId);


        if(this.options.description){
            this.$description = $('<div class="ui-description"></div>');
        }

        if(typeof this.options.name != 'undefined'){
            this.$toggle.attr('name', this.options.name);
        }

        if(this.options.style == "checkbox"){
            this.$toggle.attr('type', 'checkbox');
        } else if(this.options.style == "radio"){
            this.$toggle.attr('type', 'radio');
        }

        this.$label = $('<label/>');
        this.$label.attr('for', this.toggleId);

        this.$toggleIcon = $('<i class="fa fa-check ui-toggle-icon"></i>');

        if(this.options.inputacc){
            if(this.options.static){
                this.options.inputacc.static = true;
            }
            this.$inputAcc = new ui.Input(_.extend({

            }, this.options.inputacc));
        }


        this.on('render', function(){
            this.applyModelValueToDisplay();
            this.validateModel();
        });
    
    },



    render:function(){

        this.$el.empty();
        this.$el.addClass('ui-toggle');
        this.$el.addClass(this.options.classes);
        this.$el.attr("data-ui-style", this.options.style);
        if(typeof this.options.form == "string" && this.options.form != "" ){
            this.$el.attr("data-form", this.options.form);
        }
        if(this.options.style == "checkbox"){
            this.$el.addClass('ui-style-checkbox');
        } else if(this.options.style == "radio"){
            this.$el.addClass('ui-style-radio');
        }

        var classes = [];

        //append description
        if(this.options.description){
            this.$el.append(this.$description);
            this.$description.html(this.options.description);
        }

        //append input
        this.$el.append(this.$toggle);
        if(typeof this.options.value != 'undefined' && this.options.style == "radio"){
            this.$toggle.attr('value', this.options.value);
        }

        

        //append label
        if(this.options.label){
            this.$el.append(this.$label);
            this.$label.text(this.options.label); 
            this.$toggle.attr('data-label', this.options.label);
        }
        //append inputIcon
        this.$label.append(this.$toggleIcon);


        if(this.options.iconprefix){
            classes.push('ui-has-acc-left');
            var iconLeft = $('<div class="ui-acc ui-acc-icon ui-acc-left"><i class="'+this.options.iconprefix+'"></i></div>');
            this.attachAccessory(iconLeft);
            this.$toggle.before(iconLeft);
        }

        if(this.options.iconsuffix){
            classes.push('ui-has-acc-right');
            var iconRight = $('<div class="ui-acc ui-acc-icon ui-acc-right"><i class="'+this.options.iconsuffix+'"></i></div>');
            this.attachAccessory(iconRight);
            this.$toggle.after(iconRight);
        }

        if(this.options.prefix && _.isString(this.options.prefix)){
            classes.push('ui-has-acc-left');
            var prefix = $('<div class="ui-acc ui-acc-label ui-acc-left">'+this.options.prefix+'</div>');
            this.attachAccessory(prefix);
            this.$toggle.before(prefix);
        }
        if(this.options.suffix && _.isString(this.options.suffix)){
            classes.push('ui-has-acc-right');
            var suffix = $('<div class="ui-acc ui-acc-label ui-acc-right">'+this.options.suffix+'</div>');
            this.attachAccessory(suffix);
            this.$toggle.after(suffix);
        }


        if(this.options.suffix && this.options.suffix.$el){
            classes.push('ui-has-acc-right');

            if(this.options.suffix.UIElement && this.options.suffix.UIType == "button"){
                var suffix = $('<div class="ui-acc ui-acc-button ui-acc-right"></div>');
            }
            if(this.options.suffix.UIElement && this.options.suffix.UIType == "select"){
                var suffix = $('<div class="ui-acc ui-acc-select ui-acc-right"></div>');
            }
            suffix.append(this.options.suffix.render().$el);
            this.$toggle.after(suffix);
        }

        if(this.$inputAcc){
            this.$el.addClass('ui-acc-input');
            this.$label.html('&nbsp;');
            this.$el.append(this.$inputAcc.render().$el);
        }

        if(typeof this.options.errorMessage != 'undefined'){
            this.error(this.options.errorMessage);
        }


        if(!this.options.static){
            this.trigger('render');
            this.listenToDisplay();
        }else{
            if(this.options.value){
                this.setDisplayValue(this.options.value);
            }
        }

        return this;

    },


    displayError:function(isError, message){

        if(isError){
            this.isError = true;
        }else{
            this.isError = false;
            this.$el.removeClass('ui-error'); 
            this.$el.removeClass('ui-error-show');
        }

        if(this.isClean == true && this.options.static == false){
            return false;
        } 
        if(!this.options.static){
            if(this.options.error && this.model.get('value') == this.options.errorValue){
                if(!this.$el.hasClass("ui-focus")){
                    this.$el.addClass('ui-error');   
                }
                this.$errorMessage.html(message);
                this.$el.append(this.$errorMessage);
                return false;
            }else{
                this.options.error = null;
            }
        }
        if(isError){
            this.$errorMessage.html(message);
            this.$el.append(this.$errorMessage);
            if(!this.$el.hasClass("ui-focus")){
                this.$el.addClass('ui-error');   
            }
        }else{
            this.$el.removeClass('ui-error');
            this.$errorMessage.remove();
        }
    },


    listenToDisplay:function(){

        this.$el.addClass("ui-init");
        ui.$body.on('change click', '#'+this.toggleId, _.bind(function(event){
            this.applyDisplayValueToModel();
        }, this));


        if(this.$inputAcc){
            this.listenTo(this.$inputAcc, 'focusin', function(){
                this.$toggle.prop('checked', true);
                this.applyDisplayValueToModel();
            })
        }

    },

    setDisplayValue:function(value){
       
        if(this.$toggle){
            if(this.options.style == "radio"){
                // if(value == this.options.value){
                //     this.$toggle.attr('checked', '');
                // }else{
                //     this.$toggle.removeAttr('checked');
                // }
            }else{
                if(value == this.options.onValue){
                    this.$toggle.attr('checked', '');
                }else{
                    this.$toggle.removeAttr('checked');
                }
            }
        }

    },

    getDisplayValue:function(){

        var value = this.$toggle.prop('checked');

        if(this.options.style == "radio"){
            if(this.$toggle.prop('checked') == true){
                this.trigger('activate');
            }else{
                this.trigger('deactivate');
            }
            if(value){
                return this.options.value;
            }else{ 
                return false;
            }
        }else{
            if(value){
                return this.options.onValue;
            }else{ 
                return this.options.offValue;
            }
        }

    },

    _active:false,
    active:function(bool){
        if(bool == true){
            this._active = true;
            this.$el.addClass('ui-active');
            this.$toggle.attr('checked', '');
            if(!this.options.static){
                this.trigger('active');
            }
        }else if(bool == false){
            this._active = false;
            this.$toggle.removeAttr('checked');
            this.$el.removeClass('ui-active');
            if(!this.options.static){
                this.trigger('inactive');
            }
        }
        return this._active;
    },  

    onMouseDown:function(event){
        this.trigger('mousedown', event);
    },

    action:function(){

        if(this.menu){
            this.menu.toggleOpen();
        }
        this.trigger('action');

    },


}, {

        templateHelper:function(data){
            data.hash.static = true;
            return ui.htmlSafe(new UIToggle(data.hash).render().$el);
        },

        newFromHTML:function($element){
            var options = Base.optionsFromHTML($element);

            if($element.find('label').length){
                options.label = $element.find('label').text();
            }

            options.name = $element.find('input').attr('name');
            if($element.attr('data-ui-required')){
                options.required = true;
            }
            if($element.attr('data-ui-style') != null && $element.attr('data-ui-style') != ''){ 
                options.style = $element.attr('data-ui-style');
            }

            if(options.style != "radio"){
                options.value = $element.find('input')[0].checked;
            }

            var elementFromHTML = new UIToggle(options).render();
        }

});
 
ui.registerHelper('ui-toggle', _.bind(UIToggle.templateHelper, UIToggle)); 

module.exports = UIToggle;







},{"./base2":49,"./ui":61,"cheerio":62,"underscore":"underscore"}],61:[function(require,module,exports){
(function (process,global){
var $ = require('cheerio');
var _ = require('underscore');
var Handlebars = require('handlebars/runtime');
var Backbone = require('backbone');
Backbone.$ = $;
Backbone._ = _;
require('backbone-super');

if(!process.browser){
    //fix for cheerio
    Backbone.View.prototype._createElement = function(tagName){
        return $('<'+tagName+'/>')[0]; 
    }
}else{
    global.$ = $;
    global.Handlebars = Handlebars;
}

var jQuery = $;
var events = require('inherit/events');


_.mixin({
	toBoolean:function(value){
		if(value.toLowerCase() === "true"){
			return true;
		}else{
			return false;
		}
	}
});

if(!String.prototype.trim) {
    (function() {
        // Make sure we trim BOM and NBSP
        var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
        String.prototype.trim = function() {
          return this.replace(rtrim, '');
        };
    })();
}     



var UI = events.extend({

	initialize:function(options){

		options = options || {};
		this.options = _.extend({
			exportGlobal:false
		}, options);

        this.static = false;
        this.$body = $('body');
		this.on('destroy', this.destroy, this);

	}, 

	render:function($domScope){

		var self = this;

		$domScope = $domScope || $('body');

        ui.registerForms($domScope);

        //find all static elements and initialize them
		$domScope.find('*[data-ui]:not(.ui-init):not(.ui-child)').each(function(index, element){

			var $el = $(element);

			if(typeof ui[$el.attr('data-ui')] != 'undefined'){
                ui[$el.attr('data-ui')].newFromHTML($el);
			}
            
		});

		// _.each(this.selectors, function(selectorFunc, selectorStr){
		// 	$domScope.find(selectorStr).each(function(i,element){
		// 		selectorFunc($(element));
		// 	});
		// },this);

        

	},


	elements:{},

	get:function(id){
		return this.elements[id];
	},

	registerComponent:function(component){
		this.elements[component.id] = component;
	},

	destroy:function(element){
		delete this.elements[element.id];
	},

	selectors:{},
	registerSelector:function(selector, callback){
		this.selectors[selector] = callback;
	},

	helpers:{},
	registerHelper:function(selector, helperFunc){
		this.helpers[selector] = helperFunc;
	},
	registerHelpers:function(handlebarsInstance){
		if(typeof handlebarsInstance == "undefined"){
			handlebarsInstance = Handlebars;
		}
		_.each(this.helpers, function(helperFunc, selector){
			handlebarsInstance.registerHelper(selector, helperFunc);
		});
	},


    models:{},
    registerModel:function(modelObj){
        this.models[modelObj.name] = new ui.Model(modelObj.attributes, {
            validation:modelObj.validation
        });
    },
    registerModels:function(){
        _.each(window._ui_models, function(modelObj){
            this.registerModel(modelObj);
        },this);
    },

    forms:{},
    formElements:{},
    registerForms:function($domScope){
        
        $domScope = $domScope || $('body');
        var self = this;
        $domScope.find('form:not(.ui-init)').each(function(index, element){

            var $form = $(element);
            $form.addClass('ui-init');
            var id = element.id.toString() || _.uniqueId('form');
            var formModel = self.models[$form.attr('data-model')]
            self.forms[id] = $form;
            self.formElements[id] = {};

            $form.on('submit', function(e){
                _.each(self.formElements[id], function(el){
                    el.dirty();
                });
                var isValid = formModel.isValid();

                if(isValid == false){
                    e.preventDefault();
                    var delayCount = 0;
                    _.each(self.formElements[id], function(el){
                        if(el.isError){
                            delayCount += 1;
                            _.delay(function(){
                                el.flashError();
                            }, 100*delayCount);
                        }
                    });
                }else{
                    return true;
                }

            })

        });

    },



	html:function($element){
		if(process.browser){
			return $element[0].outerHTML;
		}else{
			return $.html($element);
		}
	}, 
	htmlSafe:function($element){
		return new Handlebars.SafeString(ui.html($element));
	}  


});

var ui = new UI();

module.exports = ui;


ui.Base = require('./base');
ui.Button = require('./button');
ui.Dialog = require('./dialog');
ui.Layout = require('./layout');
ui.Model = require('./model');
ui.Toggle = require('./toggle');
ui.RadioGroup = require('./radio-group');
ui.Menu = require('./menu');
ui.Input = require('./textfield');
    ui.Textfield = ui.Input;
ui.Select = require('./select');
ui.StaticForm = require('./static-form');

if(process.browser){
    ui.registerModels();
	ui.registerHelpers();
}


}).call(this,require('_process'),typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./base":48,"./button":50,"./dialog":51,"./layout":53,"./menu":54,"./model":55,"./radio-group":56,"./select":57,"./static-form":58,"./textfield":59,"./toggle":60,"_process":39,"backbone":"backbone","backbone-super":4,"cheerio":62,"handlebars/runtime":47,"inherit/events":"inherit/events","underscore":"underscore"}],62:[function(require,module,exports){
/*jshint sub:true, regexdash:true, laxbreak: true, expr: true*/
module.exports = window['$'] = window['jquip'] = (function(){
  var win = window,
      queryShimCdn = "http://cdnjs.cloudflare.com/ajax/libs/sizzle/1.4.4/sizzle.min.js",
      queryEngines = function(){ return win["Sizzle"] || win["qwery"]; },
      doc = document, docEl = doc.documentElement,
      scriptFns=[], load=[], sLoaded,
      runtil = /Until$/, rmultiselector = /,/,
      rparentsprev = /^(?:parents|prevUntil|prevAll)/,
      rtagname = /<([\w:]+)/,
      rclass = /[\n\t\r]/g,
      rtagSelector = /^[\w-]+$/,
      ridSelector = /^#[\w-]+$/,
      rclassSelector = /^\.[\w-]+$/,
      rspace = /\s+/,
      rdigit = /\d/,
      rnotwhite = /\S/,
      rReturn = /\r\n/g,
      rsingleTag = /^<(\w+)\s*\/?>(?:<\/\1>)?$/,
      rCRLF = /\r?\n/g,
      rselectTextarea = /^(?:select|textarea)/i,
      rinput = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,
      strim = String.prototype.trim, trim,
      trimLeft = /^\s+/,
      trimRight = /\s+$/,
      contains, sortOrder,
      guaranteedUnique = { children: true, contents: true, next: true, prev: true },
      toString = Object.prototype.toString,
      class2type = {},
      hasDup = false, baseHasDup = true,
      wrapMap = {
        option: [1, "<select multiple='multiple'>", "</select>"],
        legend: [1, "<fieldset>", "</fieldset>"],
        thead: [1, "<table>", "</table>"],
        tr: [2, "<table><tbody>", "</tbody></table>"],
        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
        col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
        area: [1, "<map>", "</map>"],
        _default: [0, "", ""] },
      breaker = {},
      ArrayProto = Array.prototype, ObjProto = Object.prototype,
      hasOwn = ObjProto.hasOwnProperty,
      slice = ArrayProto.slice,
      push = ArrayProto.push,
      indexOf = ArrayProto.indexOf,
      nativeForEach = ArrayProto.forEach,
      nativeFilter = ArrayProto.filter,
      nativeIndexOf = ArrayProto.indexOf,
      expando = 'jq-' + (+new Date()),
      fosterNode = doc.createElement('p');

  if (rnotwhite.test("\xA0")){
    trimLeft = /^[\s\xA0]+/;
    trimRight = /[\s\xA0]+$/;
  }

  /**
   * @constructor
   * @param {Object|Element|string|Array.<string>} sel
   * @param {Object=} ctx
   */
  function J(sel, ctx){
    var ret;
    for(var i = 0, l = ctors.length; i < l; i++)
      if (ctors[i].apply(this, arguments)) return this;

    if (!sel) return this;
    if (isF(sel)){
      if (sLoaded) sel();
      else scriptFns.push(sel);
      return this;
    } else if (isA(sel)) return this['make'](sel);
    if (sel.nodeType || isWin(sel)) return this['make']([sel]);
    if (sel == "body" && !ctx && doc.body) {
      this['context'] = sel['context'];
      this[0] = doc.body;
      this.length = 1;
      this['selector'] = sel;
      return this;
    }
    if (sel['selector'] !== undefined) {
      this['context'] = sel['context'];
      this['selector'] = sel['selector'];
      return this['make'](sel);
    }
    sel = isS(sel) && sel.charAt(0) === "<"
      ? (ret = rsingleTag.exec(sel))
        ? [doc.createElement(ret[1])]
        : htmlFrag(sel).childNodes
      : $$((this['selector'] = sel), ctx);

    this['make'](sel);
    if (isPlainObj(ctx)) this['attr'](ctx);
    return this;
  }

  var ctors=[], plugins={}, jquid=1, _cache={_id:0}, _display = {}, p;
  function $(sel, ctx){
    return new J(sel, ctx);
  }

  p = J.prototype = $.prototype = $['fn'] = {
    constructor: $,
    'selector': "",
    'length': 0,
    dm: function(args, tbl, cb){
      var value = args[0], parent, frag, first, l, i;
      if (value){
        if (this[0]) {
          if (!(frag = value.nodeType === 3 && value)){
            parent = value && value.parentNode;
            frag = parent && parent.nodeType === 11 && parent.childNodes.length === this.length
              ? parent
              : htmlFrag(value);
            first = frag.firstChild;
            if (frag.childNodes.length === 1) frag = first;
            if (!first) return this;
          }
          for (i=0, l=this.length; i<l; i++)
			cb.call(this[i],(i == 0 ? frag : frag.cloneNode(true)));
        }
      }
      return this;
    },
    /**
     * @param {Object} els
     * @param {string=} name
     * @param {string=} selector
     * */
    ps: function(els, name, selector){
      var ret = this.constructor();
      if (isA(els)) push.apply(ret, els);
      else merge(ret, els);
      ret.prevObject = this;
      ret.context = this.context;
      if (name === "find")
        ret.selector = this['selector'] + (this['selector'] ? " " : "") + selector;
      else if (name)
        ret.selector = this['selector'] + "." + name + "(" + selector + ")";
      return ret;
    }
  };

  p['make'] = function(els){
    make(this, els);
    return this;
  };
  p['toArray'] = function() {
    return slice.call(this, 0);
  };
  p['get'] = function(num){
    return isDefined(num)
      ? (num < 0 ? this[this.length + num] : this[num])
      : this['toArray']();
  };
  p['add'] = function(sel, ctx){
    var set = typeof sel == "string"
      ? $(sel, ctx)
      : makeArray(sel && sel.nodeType ? [sel] : sel),
      all = merge(this.get(), set);
    return this.ps(detached(set[0]) || detached(all[0]) ? all : unique(all));
  };
  function detached(el) {
    return !el || !el.parentNode || el.parentNode.nodeType == 11;
  }
  p['each'] = function(fn){
      if (!isF(fn)) return this;
      for(var i = 0, l = this.length; i < l; i++)
        fn.call(this[i], i, this[i]);
      return this;
  };
  p['attr'] = function(name, val){
    var el = this[0];
    return (isS(name) && val === undefined)
          ? attr(el, name)
      : this['each'](function(idx){
        var nt = this.nodeType;
        if (nt !== 3 && nt !== 8 && nt !== 2){
          if (isO(name)) for(var k in name)
            if (val === null)
              this.removeAttribute(name);
            else
              this.setAttribute(k, name[k]);
          else this.setAttribute(name, isF(val) ? val.call(this, idx, this.getAttribute(name)) : val);
      }
    });
  };
  p['removeAttr'] = function(name){
    return this['each'](function(){
      if (this.nodeType == 1) this.removeAttribute(name);
    });
  };
  p['data'] = function(name, setVal){
    return  (setVal === undefined)
            ? data(this[0], name)
            : this['each'](function(){
        data(this, name, setVal);
      });
  };
  p['append'] = function(){
    return this.dm(arguments, true, function(el){
      if (this.nodeType === 1)
        this.appendChild(el);
    });
  };
  p['prepend'] = function(){
    return this.dm(arguments, true, function(el){
      if (this.nodeType === 1)
        this.insertBefore(el, this.firstChild);
    });
  };
  p['before'] = function(){
    if (this[0] && this[0].parentNode) {
      return this.dm(arguments, false, function(el){
        this.parentNode.insertBefore(el, this);
      });
    }
    return this;
  };
  p['after'] = function(){
    if (this[0] && this[0].parentNode){
      return this.dm(arguments, false, function(el){
        this.parentNode.insertBefore(el, this.nextSibling);
      });
    }
    return this;
  };
  p['replaceWith'] = function(val){
    var self = this, isFunc = isF(val);
    return this['each'](function(i) {
        var next = this.nextSibling,
            parent = this.parentNode,
            value = isFunc ? val.call(this, i, this) : val;
        if (parent && parent.nodeType != 11) {
            parent.removeChild(this);
            (next ? $(next).before(value) : $(parent).append(value));
        } else { // detached
            self[i] = $(value).clone()[0];
        }
      });
  };
  p['hide'] = function(){
    return this['each'](function(){
      if (this.style.display == "none") return;
      cache(this, "display", this.style.display);
      this.style.display = "none";
    });
  };
  p['show'] = function(){
    return this['each'](function(){
      this.style.display = cache(this, "display") || display(this.tagName);
    });
  };
  p['toggle'] = function(){
    return this['each'](function(){
      var el = $(this);
      $['Expr']['hidden'](this) ? el.show() : el.hide();
    });
  };
  p['eq'] = function(i){
    return i === -1 ? this.slice(i) : this.slice(i, +i + 1);
  };
  p['first'] = function(){
    return this['eq'](0);
  };
  p['last'] = function(){
    return this['eq'](-1);
  };
  p['slice'] = function(){
    return this.ps(slice.apply(this, arguments),
      "slice", slice.call(arguments).join(","));
  };
  p['map'] = function(cb) {
    return this.ps(map(this, function(el, i) {
      return cb.call(el, i, el);
    }));
  };
  p['find'] = function(sel){
    var self = this, i, l;
    if (!isS(sel)){
      return $(sel).filter(function(){
        for(i = 0, l = self.length; i < l; i++)
          if (contains(self[i], this)) return true;
      });
    }
    var ret = this.ps("", "find", sel), len, n, r;
    for(i=0, l=this.length; i<l; i++){
      len = ret.length;
      merge(ret, $(sel, this[i]));
      if (i === 0){
        for(n = len; n < ret.length; n++)
          for(r = 0; r < len; r++)
            if (ret[r] === ret[n]){
              ret.splice(n--, 1);
              break;
            }
      }
    }
    return ret;
  };
  p['not'] = function(sel){
    return this.ps(winnow(this, sel, false), "not", sel);
  };
  p['filter'] = function(sel){
    return this.ps(winnow(this, sel, true), "filter", sel);
  };
  p['indexOf'] = function(val){
    return _indexOf(this, val);
  };
  p['is'] = function(sel){
    return this.length > 0 && $(this[0]).filter(sel).length > 0;
  };
  p['remove'] = function(){
    for(var i = 0, el; isDefined(el = this[i]); i++) {
      if (el.parentNode) el.parentNode.removeChild(el);
    }
    return this;
  };
  p['closest'] = function(sel, ctx) {
    var ret=[], i, l, cur;
    for (i=0, l=this.length; i<l; i++){
      cur = this[i];
      while (cur){
        if (filter(sel, [cur]).length>0){
          ret.push(cur);
          break;
        }else{
          cur = cur.parentNode;
          if (!cur || !cur.ownerDocument || cur === ctx|| cur.nodeType === 11)
            break;
        }
      }
    }
    ret = ret.length > 1 ? unique(ret) : ret;
    return this.ps(ret, "closest", sel);
  };
  p['val'] = function(setVal){
    if (!isDefined(setVal)) return (this[0] && this[0].value) || "";
    return this['each'](function(){
      this.value = setVal;
    });
  };
  p['html'] = function(setHtml){
    if (!isDefined(setHtml)) return (this[0] && this[0].innerHTML) || "";
    return this['each'](function(){
      this.innerHTML = setHtml;
    });
  };
  p['text'] = function(val){
    var el = this[0], nt;
    return isDefined(val)
      ? this['empty']()['append']((el && el.ownerDocument || doc).createTextNode(val))
      : (el && (nt = el.nodeType)
        ? ((nt === 1 || nt === 9)
          ? (isS(el.textContent) ? el.textContent : el.innerText.replace(rReturn, ''))
          : (nt === 3 || nt === 4) ? el.nodeValue : null)
        : null);
  };
  p['empty'] = function(){
    var i, el;
    for(i = 0; isDefined(el = this[i]); i++)
      while (el.firstChild)
        el.removeChild(el.firstChild);
    return this;
  };
  p['addClass'] = function(val){
    var cls, i, l, el, setClass, c, cl;
    if (isF(val))
      return this['each'](function(j){
        $(this)['addClass'](val.call(this, j, this.className));
      });
    if (val && isS(val)){
      cls = val.split(rspace);
      for(i = 0, l = this.length; i < l; i++){
        el = this[i];
        if (el && el.nodeType === 1){
          if (!el.className && cls.length === 1)
            el.className = val;
          else {
            setClass = " " + el.className + " ";
            for(c = 0, cl = cls.length; c < cl; c++){
              if (!~setClass.indexOf(" " + cls[c] + " "))
                setClass += cls[c] + " ";
            }
            el.className = trim(setClass);
          }
        }
      }
    }
    return this;
  };
  p['removeClass'] = function(val){
    var clss, i, l, el, cls, c, cl;
    if (isF(val)) return this['each'](function(j){
      $(this)['removeClass'](val.call(this, j, this.className));
    });
    if ((val && isS(val)) || val === undefined){
      clss = (val || "").split(rspace);
      for(i = 0, l = this.length; i < l; i++){
        el = this[i];
        if (el.nodeType === 1 && el.className){
          if (val){
            cls = (" " + el.className + " ").replace(rclass, " ");
            for(c = 0, cl = clss.length; c < cl; c++)
              cls = cls.replace(" " + clss[c] + " ", " ");
            el.className = trim(cls);
          }
          else el.className = "";
        }
      }
    }
    return this;
  };
  p['hasClass'] = function(sel){
    return hasClass(this, sel);
  };
  p['fadeIn'] = function(){
    this['each'](function(){
      $(this)['show']();
    });
  };
  p['fadeOut'] = function(){
    this['each'](function(){
      $(this)['hide']();
    });
  };
  p['serializeArray'] = function() {
    return this['map'](function(){
      return this.elements ? makeArray(this.elements) : this;
    }).filter(function(){
      return this.name && !this.disabled &&
        (this.checked || rselectTextarea.test(this.nodeName) || rinput.test(this.type));
    }).map(function(i, el){
      var val = $(this)['val']();
      return val == null || isA(val)
        ? map(val, function(val){
          return { name: el.name, value: val.replace(rCRLF, "\r\n") };
                  })
        : { name: el.name, value: val.replace(rCRLF, "\r\n") };
    }).get();
  };
  p['wrap'] = function(wrapper) {
    return this['each'](function() {
      var wrapperClone = $($(wrapper)[0].cloneNode(false));
      $(this).before(wrapperClone);
      wrapperClone.append($(this));
    });
  };
  p['prop'] = function(name, setVal) {
    if (isDefined(setVal))
      return this.each(function() { this[name] = setVal; });
    return this[0] && this[0][name];
  };
  p['clone'] = function() {
    return $(this.map(function() { return this.cloneNode(true); }));
  };
  p['toggleClass'] = function(className, val) {
    return this['each'](function() {
      var el = $(this);
      (isDefined(val) ? val : !el.hasClass(className))
        ? el.addClass(className) : el.removeClass(className);
    });
  };

  $['Expr'] = {
    'hidden': function(el){
      return ($["css"] && $["css"](el,"display") || el.style.display) === "none";
    },
    'visible': function(el) {
      return !$['Expr']['hidden'](el);
    }
  };

  function winnow(els, sel, keep){
    sel = sel || 0;
    if (isF(sel))
      return grep(els, function(el, i){
        return !!sel.call(el, i, el) === keep;
      });
    else if (sel.nodeType)
      return grep(els, function(el){
        return (el === sel) === keep;
      });
    else if (isS(sel)) {
      var expr = sel.charAt(0) == ":" && $['Expr'][sel.substring(1)];
      return grep(els, function(el) {
        var parentNode = el.parentNode,
            orphan = !parentNode,
            result;
        if (orphan) {
          parentNode = fosterNode;
          parentNode.appendChild(el);
        }
        result = expr
          ? expr(el)
          : el.parentNode && _indexOf($$(sel, el.parentNode), el) >= 0;
        orphan && parentNode.removeChild(el);
        return result;
      });
    }
    return grep(els, function(el) {
      return (_indexOf(sel, el) >= 0) === keep;
    });
  }
  function cache(el, name, val)
  {
    var id = el[expando];
    if (!isDefined(val))
      return id && _cache[id] && (name ? _cache[id][name] : _cache[id]);

    if (!id) id = el[expando] = jquid++;
    return (_cache[id] || (_cache[id]={}))[name] = val;
  }
  function display(tag) {
    if (!_display[tag]) {
      var el = $("<" + tag + ">")['appendTo'](doc.body),
        d = ($['css'] && $['css'](el[0], "display")) || el[0].style.display;
      el.remove();
      _display[tag] = d;
    }
    return _display[tag];
  }
  function make(arr, els){
    arr.length = (els && els.length || 0);
    if (arr.length === 0) return arr;
    for(var i = 0, l = els.length; i < l; i++)
      arr[i] = els[i];
    return arr;
  }
  function hasClass(els, cls){
    cls = " " + cls + " ";
    for(var i = 0, l = els.length; i < l; i++)
      if (eqClass(els[i], cls))
        return true;
    return false;
  } $['hasClass'] = hasClass;
  function eqClass(el, cls){
    return el.nodeType === 1 && (" " + el.className + " ").replace(rclass, " ").indexOf(cls) > -1;
  }
  function walk(fn, ctx, ret){
    ctx = ctx || doc;
    ret = ret || [];
    if (ctx.nodeType == 1)
      if (fn(ctx)) ret.push(ctx);
    var els = ctx.childNodes;
    for(var i = 0, l = els.length; i < l; i++){
      var subEl = els[i];
      if (subEl.nodeType == 1)
        walk(fn, subEl, ret);
    }
    return ret;
  } $['walk'] = walk;

  /**
   * @param {string} html
   * @param {Object=} ctx
   * @param {Object=} qry
   * */
  function $$(sel, ctx, qry){
    if (sel && isS(sel)){
      if (ctx instanceof $) ctx = ctx[0];
      ctx = ctx || doc;
      qry = qry || $['query'];
      var el, rest = sel.substring(1);
      return ridSelector.test(sel) && ctx === doc
        ? ((el = doc.getElementById(rest)) ? [el] : [])
        : makeArray(
          rclassSelector.test(sel) && ctx.getElementsByClassName
            ? ctx.getElementsByClassName(rest)
            : rtagSelector.test(sel)
              ? ctx.getElementsByTagName(sel)
              : qry(sel, ctx));
    }
    return sel.nodeType == 1 || sel.nodeType == 9 ? [sel] : [];
  } $['$$'] = $$;

  $['setQuery'] = function(qry){
    $['query'] = function(sel, ctx){
      return $$(sel, ctx, (qry || function(sel, ctx){ return ctx.querySelectorAll(sel); }));
    };
  };

  var useQuery = queryEngines();
  $['setQuery'](useQuery || function(sel, ctx){
    return (ctx = ctx || doc).querySelectorAll ? makeArray(ctx.querySelectorAll(sel)) : [];
  });

  function loadScript(url, cb, async){
    var h = doc.head || doc.getElementsByTagName('head')[0] || docEl,
      s = doc.createElement('script'), rs;
    if (async) s.async = "async";
      s.onreadystatechange = function () {
        if (!(rs = s.readyState) || rs == "loaded" || rs == "complete"){
          s.onload = s.onreadystatechange = null;
          if (h && s.parentNode)
              h.removeChild(s);
          s = undefined;
          if (cb) cb();
        }
      };
    s.onload = cb;
    s.src = url;
    h.insertBefore(s, h.firstChild);
  } $['loadScript'] = loadScript;

  /** @param {...string} var_args */
  function warn(var_args){ win.console && win.console.warn(arguments); }

  $['each'] = function(o, cb, args){
    var k, i = 0, l = o.length, isObj = l === undefined || isF(o);
    if (args){
      if (isObj) {
        for(k in o)
          if (cb.apply(o[k], args) === false) break;
      } else
        for(; i < l;)
          if (cb.apply(o[i++], args) === false) break;
    } else {
      if (isObj) {
        for(k in o)
          if (cb.call(o[k], k, o[k]) === false) break;
      }
      else
        for(; i < l;)
          if (cb.call(o[i], i, o[i++]) === false) break;
    }
    return o;
  };
  function _each(o, fn, ctx){
    if (o == null) return;
    if (nativeForEach && o.forEach === nativeForEach)
      o.forEach(fn, ctx);
    else if (o.length === +o.length){
      for(var i = 0, l = o.length; i < l; i++)
        if (i in o && fn.call(ctx, o[i], i, o) === breaker) return;
    } else {
      for(var key in o)
        if (hasOwn.call(o, key))
          if (fn.call(ctx, o[key], key, o) === breaker) return;
    }
  } $['_each'] = _each;
  function attr(el, name) {
    if (!el || !el.getAttribute || !name) return; // text nodes or comment nodes don't have attributes
    var ret = el.hasAttribute && el.hasAttribute(name) ? el.getAttribute(name) : el[name];
    return (ret === null ? undefined : ret); // el.getAttribute inconsistently return null for non-defined attributes
  }
  function filter(sel, els) {
    return isDefined(sel) ? $(els).filter(sel) : $(els);
  } $['filter'] = filter;
  function _indexOf(arr, val){
    if (arr == null) return -1;
    var i, l;
    if (nativeIndexOf && arr.indexOf === nativeIndexOf) return arr.indexOf(val);
    for(i = 0, l = arr.length; i < l; i++) if (arr[i] === val) return i;
    return -1;
  } $['_indexOf'] = _indexOf;
  $['_defaults'] = function(obj){
    _each(slice.call(arguments, 1), function(o){
      for(var k in o)
        if (obj[k] == null) obj[k] = o[k];
    });
    return obj;
  };
  function _filter(o, fn, ctx){
    var ret = [];
    if (o == null) return ret;
    if (nativeFilter && o.filter === nativeFilter) return o.filter(fn, ctx);
    _each(o, function(val, i, arr){
      if (fn.call(ctx, val, i, arr)) ret[ret.length] = val;
    });
    return ret;
  } $['_filter'] = _filter;
  $['proxy'] = function(fn, ctx){
    if (typeof ctx == "string"){
      var tmp = fn[ctx];
      ctx = fn;
      fn = tmp;
    }
    if (isF(fn)){
      var args = slice.call(arguments, 2),
        proxy = function(){
          return fn.apply(ctx, args.concat(slice.call(arguments))); };
      proxy.guid = fn.guid = fn.guid || proxy.guid || jquid++;
      return proxy;
    }
  };
  function dir(el, prop, until){
    var matched = [], cur = el[prop];
    while (cur && cur.nodeType !== 9 && (until === undefined || cur.nodeType !== 1 || !$(cur).is(until))){
      if (cur.nodeType === 1) matched.push(cur);
      cur = cur[prop];
    }
    return matched;
  } $['dir'] = dir;
  function nth(cur, res, dir){
    res = res || 1;
    var num = 0;
    for(; cur; cur = cur[dir])
      if (cur.nodeType === 1 && ++num === res) break;
    return cur;
  } $['nth'] = nth;
  function sibling(n, el){
    var r = [];
    for(; n; n = n.nextSibling) if (n.nodeType === 1 && n !== el) r.push(n);
    return r;
  } $['sibling'] = sibling;

  function grep(els, cb, inv){
    var ret = [], retVal;
    inv = !!inv;
    for(var i=0, l=els.length; i<l; i++){
      retVal = !!cb(els[i], i);
      if (inv !== retVal)
        ret.push(els[i]);
    }
    return ret;
  } $['grep'] = grep;
  /**
   * @param {Object} els
   * @param {function} cb
   * @param {Object=} arg
   * */
  function map(els, cb, arg){
    var value, key, ret = [], i = 0, length = els.length,
      isArray = els instanceof $
        || typeof length == "number"
        && ((length > 0 && els[0] && els[length - 1]) || length === 0 || isA(els));
    if (isArray){
      for(; i < length; i++){
        value = cb(els[i], i, arg);
        if (value != null)
          ret[ret.length] = value;
      }
    } else {
      for(key in els){
        value = cb(els[key], key, arg);
        if (value != null)
          ret[ret.length] = value;
      }
    }
    return ret.concat.apply([], ret);
  } $['map'] = map;
  function data(el, name, setVal){
    if (!el) return {};
    var res = cache(el, name, setVal);
    return res || attrs(el)['data-' + name];
  } $['data'] = data;
  function attrs(el){
    var o = {};
    if (el.nodeType == 1)
      for(var i = 0, elAttrs = el.attributes, len = elAttrs.length; i < len; i++)
        o[elAttrs.item(i).nodeName] = elAttrs.item(i).nodeValue;
    return o;
  } $['attrs'] = attrs;
  function eqSI(str1, str2){
    return !str1 || !str2 ? str1 == str2 : str1.toLowerCase() === str2.toLowerCase();
  } $['eqSI'] = eqSI;
 trim = strim
  ? function(text){ return text == null ? "" : strim.call(text); }
  : function(text){ return text == null ? "" : text.toString().replace(trimLeft, "").replace(trimRight, ""); };
  $['trim'] = trim;
  $['indexOf'] = $['inArray'] = function(el, arr){
    if (!arr) return -1;
    if (indexOf) return indexOf.call(arr, el);
    for(var i = 0, length = arr.length; i < length; i++)
      if (arr[i] === el)
        return i;
    return -1;
  };
  _each("Boolean Number String Function Array Date RegExp Object".split(" "), function(name){
    class2type["[object " + name + "]"] = name.toLowerCase();
    return this;
  });

  function typeOf(o){ return o == null ? String(o) : class2type[toString.call(o)] || "object"; } $['type'] = typeOf;
  function isDefined(o){ return o !== void 0; }
  function isS(o){ return typeof o == "string"; }
  function isO(o){ return typeof o == "object"; }
  function isF(o){ return typeof o == "function" || typeOf(o) === "function"; } $['isFunction'] = isF;
  function isA(o){ return typeOf(o) === "array"; } $['isArray'] = Array.isArray || isA;
  function isAL(o){ return !isS(o) && typeof o.length == 'number'; }
  function isWin(o){ return o && typeof o == "object" && "setInterval" in o; } $['isWindow'] = isWin;
  function isNan(obj){ return obj == null || !rdigit.test(obj) || isNaN(obj); } $['isNaN'] = isNan;
  function isPlainObj(o){
    if (!o || typeOf(o) !== "object" || o.nodeType || isWin(o)) return false;
    try{
      if (o.constructor && !hasOwn.call(o, "constructor") && !hasOwn.call(o.constructor.prototype, "isPrototypeOf"))
        return false;
    }catch(e){
      return false;
    }
    var key;
    for(key in o){}
    return key === undefined || hasOwn.call(o, key);
  }
  function merge(a1, a2){
    var i = a1.length, j = 0;
    if (typeof a2.length == "number")
      for(var l = a2.length; j < l; j++)
        a1[i++] = a2[j];
    else
      while (a2[j] !== undefined)
        a1[i++] = a2[j++];
    a1.length = i;
    return a1;
  } $['merge'] = merge;
  function extend(){
    var opt, name, src, copy, copyIsArr, clone, args = arguments,
      dst = args[0] || {}, i = 1, aLen = args.length, deep = false;
    if (typeof dst == "boolean"){ deep = dst; dst = args[1] || {}; i = 2; }
    if (typeof dst != "object" && !isF(dst)) dst = {};
    if (aLen === i){ dst = this; --i; }
    for(; i < aLen; i++){
      if ((opt = args[i]) != null){
        for(name in opt){
          src = dst[name];
          copy = opt[name];
          if (dst === copy) continue;
          if (deep && copy && (isPlainObj(copy) || (copyIsArr = isA(copy)))){
            if (copyIsArr){
              copyIsArr = false;
              clone = src && isA(src) ? src : [];
            } else
              clone = src && isPlainObj(src) ? src : {};
            dst[name] = extend(deep, clone, copy);
          } else if (copy !== undefined)
            dst[name] = copy;
        }
      }
    }
    return dst;
  } $['extend'] = $['fn']['extend'] = extend;
  function makeArray(arr, res){
    var ret = res || [];
    if (arr != null){
      var type = typeOf(arr);
      if (arr.length == null || type == "string" || type == "function" || type === "regexp" || isWin(arr))
        push.call(ret, arr);
      else
        merge(ret, arr);
    }
    return ret;
  } $['makeArray'] = makeArray;
  /**
   * @param {string} html
   * @param {Object=} ctx
   * @param {Object=} frag
   * */
  function htmlFrag(html, ctx, frag){
    ctx = ((ctx || doc) || ctx.ownerDocument || ctx[0] && ctx[0].ownerDocument || doc);
    frag = frag || ctx.createDocumentFragment();
    if (isAL(html))
      return clean(html, ctx, frag) && frag;
    var div = fragDiv(html);
    while (div.firstChild)
      frag.appendChild(div.firstChild);
    return frag;
  } $['htmlFrag'] = htmlFrag;
  /**
   * @param {string} html
   * @param {Object=} ctx
   * */
  function fragDiv(html, ctx){
    var div = (ctx||doc).createElement('div'),
      tag = (rtagname.exec(html) || ["", ""])[1].toLowerCase(),
      wrap = wrapMap[tag] || wrapMap._default,
      depth = wrap[0];
    div.innerHTML = wrap[1] + html + wrap[2];
    while (depth--)
      div = div.lastChild;
    return div;
  }
  function clean(els, ctx, frag){
    var ret=[],i,el;
    for (i=0; (el=els[i])!=null; i++){
      if (isS(el))
        el = fragDiv(el, ctx);
      if (el.nodeType)
        ret.push(el);
      else
        ret = merge(ret, el);
    }
    if (frag)
      for (i=0; i<ret.length; i++)
        if (ret[i].nodeType)
          frag.appendChild(ret[i]);
    return ret;
  }
  var sibChk = function(a, b, ret){
    if (a === b) return ret;
    var cur = a.nextSibling;
    while (cur){
      if (cur === b) return -1;
      cur = cur.nextSibling;
    }
    return 1;
  };
  contains = $['contains'] = docEl.contains
    ? function(a, b){
      return a !== b && (a.contains ? a.contains(b) : true); }
    : function(){ return false; };
  sortOrder = docEl.compareDocumentPosition
    ? (contains = function(a, b){ return !!(a.compareDocumentPosition(b) & 16); }) //assigning contains
      && function(a, b){
      if (a === b){ hasDup = true; return 0; }
      if (!a.compareDocumentPosition || !b.compareDocumentPosition)
        return a.compareDocumentPosition ? -1 : 1;
      return a.compareDocumentPosition(b) & 4 ? -1 : 1;
      }
    : function(a, b){
      if (a === b){ hasDup = true; return 0; }
      else if (a.sourceIndex && b.sourceIndex) return a.sourceIndex - b.sourceIndex;
      var al, bl, ap = [], bp = [], aup = a.parentNode, bup = b.parentNode, cur = aup;
      if (aup === bup) return sibChk(a, b);
      else if (!aup) return -1;
      else if (!bup) return 1;
      while (cur){ ap.unshift(cur); cur = cur.parentNode; }
      cur = bup;
      while (cur){ bp.unshift(cur); cur = cur.parentNode; }
      al = ap.length;
      bl = bp.length;
      for(var i = 0; i < al && i < bl; i++)
        if (ap[i] !== bp[i]) return sibChk(ap[i], bp[i]);
      return i === al ? sibChk(a, bp[i], -1) : sibChk(ap[i], b, 1);
     };
  function unique(els){
    if (sortOrder){
      hasDup = baseHasDup;
      els.sort(sortOrder);
      if (hasDup)
        for(var i = 1; i < els.length; i++)
          if (els[i] === els[i - 1]) els.splice(i--, 1);
    }
    return els;
  } $['unique'] = unique;
  _each({
    'parent': function(el){ var parent = el.parentNode; return parent && parent.nodeType !== 11 ? parent : null; },
    'parents': function(el){ return dir(el, "parentNode"); },
    'parentsUntil': function(el, i, until){ return dir(el, "parentNode", until); },
    'next': function(el){ return nth(el, 2, "nextSibling"); },
    'prev': function(el){ return nth(el, 2, "previousSibling"); },
    'nextAll': function(el){ return dir(el, "nextSibling"); },
    'prevAll': function(el){ return dir(el, "previousSibling"); },
    'nextUntil': function(el, i, until){ return dir(el, "nextSibling", until); },
    'prevUntil': function(el, i, until){ return dir(el, "previousSibling", until); },
    'siblings': function(el){ return sibling(el['parentNode']['firstChild'], el); },
    'children': function(el){ return sibling(el['firstChild']); },
    'contents': function(el){
      return el['nodeName'] === "iframe" ? el['contentDocument'] || el['contentWindow']['document '] : makeArray(el['childNodes']);
    }
  }, function(fn, name){
    $['fn'][name] = function(until, sel){
      var ret = map(this, fn, until), args = slice.call(arguments);
      if (!runtil.test(name)) sel = until;
      if (isS(sel)) ret = makeArray(filter(sel, ret));

      ret = this.length > 1 && !guaranteedUnique[name] ? unique(ret) : ret;
      if ((this.length > 1 || rmultiselector.test(sel)) && rparentsprev.test(name)) ret = ret.reverse();
      return this.ps(ret, name, args.join(","));
    };
  });
  _each({
    'appendTo': "append",
    'prependTo': "prepend",
    'insertBefore': "before",
    'insertAfter': "after"
  }, function(orig, name) {
    $['fn'][name] = function(sel){
      var ret = [], to = $(sel), i, els, l,
        p = this.length === 1 && this[0].parentNode;
      if (p && p.nodeType === 11 && p.childNodes.length === 1 && to.length === 1) {
        to[orig](this[0]);
        return this;
      }else{
        for(i=0, l=to.length; i<l; i++){
          els = (i > 0 ? this.clone(true) : this).get();
          $(to[i])[orig](els);
          ret = ret.concat(els);
        }
        return this.ps(ret, name, to['selector']);
      }
    };
  });

  function boxmodel(){
    if (!doc.body) return null; //in HEAD
    var d = doc.createElement('div');
    doc.body.appendChild(d);
    d.style.width = '20px';
    d.style.padding = '10px';
    var w = d.offsetWidth;
    doc.body.removeChild(d);
    return w == 40;
  }

  (function(){
    var div = document.createElement("div");
    div.style.display = "none";
    div.innerHTML = "   <link/><table></table><a href='/a' style='color:red;float:left;opacity:.55;'>a</a><input type='checkbox'/>";
    var a = div.getElementsByTagName("a")[0];
    $['support'] = {
      boxModel: null,
      opacity: /^0.55$/.test(a.style.opacity),
      cssFloat: !!a.style.cssFloat
    };

    var rwebkit = /(webkit)[ \/]([\w.]+)/,
      ropera = /(opera)(?:.*version)?[ \/]([\w.]+)/,
      rmsie = /(msie) ([\w.]+)/,
      rmozilla = /(mozilla)(?:.*? rv:([\w.]+))?/,
      ua = navigator.userAgent.toLowerCase(),
      match = rwebkit.exec(ua)
         || ropera.exec( ua )
         || rmsie.exec( ua )
         || ua.indexOf("compatible") < 0 && rmozilla.exec( ua ) || [],
      b;
    b = $['browser'] = { version: match[2] || "0" };
    b[match[1] || ""] = true;
  })();
  $['scriptsLoaded'] = function(cb) {
    if (isF(cb)) scriptFns.push(cb);
  };
  function loadAsync(url, cb){
    load.push({url:url,cb:cb});
  } $['loadAsync'] = loadAsync;

  if (!useQuery && !doc.querySelectorAll)
    loadAsync(queryShimCdn, function(){
      $['setQuery'](queryEngines());
    });

  function fireSL(){
    _each(scriptFns, function(cb){
      cb();
    });
    sLoaded = true;
  }

  $['init'] = false;
  $['onload'] = function(){
    if (!$['init'])
    try {
      $['support']['boxModel'] = boxmodel();
      var cbs = 0;
      _each(load, function(o){
        cbs++;
        loadScript(o.url, function(){
          try { if (o.cb) o.cb(); } catch(e){}
          if (!--cbs)fireSL();
        });
      });
      $['init'] = true;
      if (!cbs)fireSL();
    } catch(e){
      warn(e);
    }
  };
  if (doc['body'] && !$['init'])
    setTimeout($['onload'],1); //let plugins loadAsync

  $['hook'] = function(fn){
    ctors.push(fn);
  };
  $['plug'] = function(meta, fn){
    var name = isS(meta) ? meta : meta['name'];
    fn = isF(meta) ? meta : fn;
    if (!isF(fn)) throw "Plugin fn required";
    if (name && fn) plugins[name] = fn;
    fn($);
  };

  return $;
})();
;$['plug']("ajax", function ($) {
  var xhrs = [
           function () { return new XMLHttpRequest(); },
           function () { return new ActiveXObject("Microsoft.XMLHTTP"); },
           function () { return new ActiveXObject("MSXML2.XMLHTTP.3.0"); },
           function () { return new ActiveXObject("MSXML2.XMLHTTP"); }
      ],
      _xhrf = null;

  function _xhr() {
    if (_xhrf != null) return _xhrf();
    for (var i = 0, l = xhrs.length; i < l; i++) {
      try {
        var f = xhrs[i], req = f();
        if (req != null) {
          _xhrf = f;
          return req;
        }
      } catch (e){}
    }
    return function () { };
  } $['xhr'] = _xhr;

  function _xhrResp(xhr, dataType) {
    dataType = (dataType || xhr.getResponseHeader("Content-Type").split(";")[0]).toLowerCase();
    if (dataType.indexOf("json") >= 0){
      var j = false;
      if(window.JSON){
        j = window.JSON['parse'](xhr.responseText);
      }else{
        j = eval(xhr.responseText);
      }
      return j;
    }
    if (dataType.indexOf("script") >= 0)
      return eval(xhr.responseText);
    if (dataType.indexOf("xml") >= 0)
      return xhr.responseXML;
    return xhr.responseText;
  } $['_xhrResp'] = _xhrResp;

  $['formData'] = function formData(o) {
    var kvps = [], regEx = /%20/g;
    for (var k in o) kvps.push(encodeURIComponent(k).replace(regEx, "+") + "=" + encodeURIComponent(o[k].toString()).replace(regEx, "+"));
    return kvps.join('&');
  };

  $['each']("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "), function(i,o){
    $['fn'][o] = function(f){
      return this['bind'](o, f);
    };
  });

  function ajax(url, o) {
      var xhr = _xhr(), timer, n = 0;
      if (typeof url === "object") o = url;
      else o['url'] = url;
      o = $['_defaults'](o, { 'userAgent': "XMLHttpRequest", 'lang': "en", 'type': "GET", 'data': null, 'contentType': "application/x-www-form-urlencoded", 'dataType': null, 'processData': true, 'headers': {"X-Requested-With": "XMLHttpRequest" }, 'cache': true });
      if (o.timeout) timer = setTimeout(function () { xhr.abort(); if (o.timeoutFn) o.timeoutFn(o.url); }, o.timeout);
      var cbCtx = $(o['context'] || document), evtCtx = cbCtx;
      xhr.onreadystatechange = function() {
          if (xhr.readyState == 4){
              if (timer) clearTimeout(timer);
              if (xhr.status < 300){
                  var res, decode = true, dt = o.dataType || "";
                  try{
                      res = _xhrResp(xhr, dt, o);
                  }catch(e){
                      decode = false;
                      if (o.error)
                      o.error(xhr, xhr.status, xhr.statusText);
                  evtCtx['trigger'](cbCtx, "ajaxError", [xhr, xhr.statusText, o]);
                  }
                  if (o['success'] && decode && (dt.indexOf('json')>=0 || !!res))
                      o['success'](res);
                  evtCtx['trigger'](cbCtx,"ajaxSuccess", [xhr, res, o]);

              }
              else {
                  if (o.error)
                      o.error(xhr, xhr.status, xhr.statusText);
                  evtCtx['trigger'](cbCtx, "ajaxError", [xhr, xhr.statusText, o]);
              }
              if (o['complete'])
                  o['complete'](xhr, xhr.statusText);
              evtCtx['trigger'](cbCtx, "ajaxComplete", [xhr, o]);
          }
          else if (o['progress']) o['progress'](++n);
      };
      var url = o['url'], data = null;
      var cache = o['cache']==true;
      var isPost = o['type'] == "POST" || o['type'] == "PUT";
      if( o['data'] && o['processData'] && typeof o['data'] == 'object' )
          data = $['formData'](o['data']);

      if (!isPost && data) {
          url += "?" + data;
          data = null;
          if(!cache)
            url=url+"&_="+(new Date().getTime());
      }else(!isPost && !cache)
          url=url+"?_="+(new Date().getTime());
      cache=null;
      xhr.open(o['type'], url);

      try {
          for (var i in o.headers)
              xhr.setRequestHeader(i, o.headers[i]);
      } catch(_) { console.log(_) }

      if (isPost) {
          if(o['contentType'].indexOf('json')>=0)
              data = o['data'];
          xhr.setRequestHeader("Content-Type", o['contentType']);
      }

      xhr.send(data);
  } $['ajax'] = ajax;

  $['getJSON'] = function (url, data, success, error) {
    if ($['isFunction'](data)){
      error = success;
      success = data;
      data = null;
    }
    ajax({'url': url, 'data': data, 'success': success, 'dataType': 'json'});
  };

  $['get'] = function (url, data, success, dataType) {
    if ($['isFunction'](data)) {
      dataType = success;
      success = data;
      data = null;
    }
    ajax({'url': url, 'type': "GET", 'data': data, 'success': success, 'dataType': dataType || "text/plain"});
  };

  $['post'] = function (url, data, success, dataType) {
    if ($['isFunction'](data)) {
      dataType = success;
      success = data;
      data = null;
    }
    ajax({'url': url, 'type': "POST", 'data': data, 'success': success, 'dataType': dataType || "text/plain"});
  };

  $['getScript'] = function (url, success) {
    return $['get'](url, undefined, success, "script");
  };

  if (!window.JSON)
    $['loadAsync']("http://ajax.cdnjs.com/ajax/libs/json2/20110223/json2.js");
});
;$['plug']("css", function ($) {
  var doc = document,
      docEl = doc.documentElement,
      ralpha = /alpha\([^)]*\)/i,
      ropacity = /opacity=([^)]*)/,
      rdashAlpha = /-([a-z])/ig,
      rupper = /([A-Z])/g,
      rnumpx = /^-?\d+(?:px)?$/i,
      rnum = /^-?\d/,
      rroot = /^(?:body|html)$/i,
      cssShow = { position: "absolute", visibility: "hidden", display: "block" },
      cssWidth = [ "Left", "Right" ],
      cssHeight = [ "Top", "Bottom" ],
      curCSS,
      getComputedStyle,
      currentStyle,
      fcamelCase = function (all, l) { return l.toUpperCase(); };

  $['cssHooks'] = {
    'opacity': {
      'get': function (el, comp) {
        if (!comp) return el.style.opacity;
        var ret = curCSS(el, "opacity", "opacity");
        return ret === "" ? "1" : ret;
      }
    }
  };

  $['_each'](["height", "width"], function(k) {
    $['cssHooks'][k] = {
      get: function(el, comp, extra) {
        var val;
        if (comp) {
          if (el.offsetWidth !== 0)
            return getWH(el, k, extra);

          swap(el, cssShow, function() {
            val = getWH( el, k, extra );
          });
          return val;
        }
      },
      set: function(el, val) {
        if (rnumpx.test(val)) {
          val = parseFloat( val );

          if (val >= 0)
            return val + "px";
        } else
          return val;
      }
    };
  });

  function getWH(el, name, extra) {
    var val = name === "width" ? el.offsetWidth : el.offsetHeight,
      which = name === "width" ? cssWidth : cssHeight;
    if (val > 0) {
      if (extra !== "border") {
        $['each']( which, function() {
          if ( !extra )
            val -= parseFloat(css(el, "padding" + this) ) || 0;
          if ( extra === "margin" )
            val += parseFloat(css(el, extra + this) ) || 0;
          else
            val -= parseFloat(css(el, "border" + this + "Width") ) || 0;
        });
      }
      return val + "px";
    }
    return "";
  }

  if (!$['support']['opacity']) {
    $['support']['opacity'] = {
          get: function (el, computed) {
              return ropacity.test((computed && el.currentStyle ? el.currentStyle.filter : el.style.filter) || "")
                  ? (parseFloat(RegExp.$1) / 100) + ""
                  : computed ? "1" : "";
          },
          set: function (el, value) {
              var s = el.style;
              s.zoom = 1;
              var opacity = $['isNaN'](value) ? "" : "alpha(opacity=" + value * 100 + ")", filter = s.filter || "";
              s.filter = ralpha.test(filter) ?
        filter.replace(ralpha, opacity) :
        s.filter + ' ' + opacity;
          }
      };
  }

  if (doc.defaultView && doc.defaultView.getComputedStyle) {
    getComputedStyle = function (el, newName, name) {
      var ret, defaultView, computedStyle;
      name = name.replace(rupper, "-$1").toLowerCase();
      if (!(defaultView = el.ownerDocument.defaultView)) return undefined;
      if ((computedStyle = defaultView.getComputedStyle(el, null))) {
        ret = computedStyle.getPropertyValue(name);
        if (ret === "" && !$['contains'](el.ownerDocument.documentElement, el))
          ret = $['style'](el, name);
      }
      return ret;
    };
  }

  if (doc.documentElement.currentStyle) {
      currentStyle = function (el, name) {
          var left,
          ret = el.currentStyle && el.currentStyle[name],
          rsLeft = el.runtimeStyle && el.runtimeStyle[name],
          style = el.style;
          if (!rnumpx.test(ret) && rnum.test(ret)) {
              left = style.left;
              if (rsLeft) el.runtimeStyle.left = el.currentStyle.left;
              style.left = name === "fontSize" ? "1em" : (ret || 0);
              ret = style.pixelLeft + "px";
              style.left = left;
              if (rsLeft) el.runtimeStyle.left = rsLeft;
          }
          return ret === "" ? "auto" : ret;
      };
  }
  curCSS = getComputedStyle || currentStyle;

  $['fn']['css'] = function (name, value) {
      if (arguments.length === 2 && value === undefined) return this;

      return access(this, name, value, true, function (el, name, value) {
          return value !== undefined ? style(el, name, value) : css(el, name);
      });
  };
  $['cssNumber'] = { "zIndex": true, "fontWeight": true, "opacity": true, "zoom": true, "lineHeight": true };
  $['cssProps'] = { "float": $['support']['cssFloat'] ? "cssFloat" : "styleFloat" };
  function style(el, name, value, extra) {
      if (!el || el.nodeType === 3 || el.nodeType === 8 || !el.style) return;
      var ret, origName = camelCase(name), style = el.style, hooks = $['cssHooks'][origName];
      name = $['cssProps'][origName] || origName;
      if (value !== undefined) {
          if (typeof value === "number" && isNaN(value) || value == null) return;
          if (typeof value === "number" && !$['cssNumber'][origName]) value += "px";
          if (!hooks || !("set" in hooks) || (value = hooks.set(el, value)) !== undefined) {
              try {
                  style[name] = value;
              } catch (e) { }
          }
      } else {
          if (hooks && "get" in hooks && (ret = hooks.get(el, false, extra)) !== undefined)
              return ret;
          return style[name];
      }
  } $['style'] = style;

  function css(el, name, extra) {
      var ret, origName = camelCase(name), hooks = $['cssHooks'][origName];
      name = $['cssProps'][origName] || origName;
      if (hooks && "get" in hooks && (ret = hooks.get(el, true, extra)) !== undefined) return ret;
      else if (curCSS) return curCSS(el, name, origName);
  }$['css'] = css;

  function swap(el, opt, cb) {
      var old = {},k;
      for (var k in opt) {
          old[k] = el.style[k];
          el.style[k] = opt[k];
      }
      cb.call(el);
      for (k in opt) el.style[k] = old[k];
  }$['swap'] = swap;

  function camelCase(s) { return s.replace(rdashAlpha, fcamelCase); } $['camelCase'] = camelCase;

  function access(els, key, value, exec, fn, pass) {
      var l = els.length;
      if (typeof key === "object") {
          for (var k in key) {
              access(els, k, key[k], exec, fn, value);
          }
          return els;
      }
      if (value !== undefined) {
          exec = !pass && exec && $['isFunction'](value);
          for (var i = 0; i < l; i++)
              fn(els[i], key, exec ? value.call(els[i], i, fn(els[i], key)) : value, pass);
          return els;
      }
      return l ? fn(els[0], key) : undefined;
  }

  var init, noMarginBodyOff, subBorderForOverflow, suppFixedPos, noAddBorder, noAddBorderForTables,
      initialize = function() {
        if (init) return;
        var body = doc.body, c = doc.createElement("div"), iDiv, cDiv , table, td, bodyMarginTop = parseFloat(css(body, "marginTop")) || 0,
          html = "<div style='position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;'><div></div></div><table style='position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;' cellpadding='0' cellspacing='0'><tr><td></td></tr></table>";
        $['extend'](c.style, { position: "absolute", top: 0, left: 0, margin: 0, border: 0, width: "1px", height: "1px", visibility: "hidden" });
        c.innerHTML = html;
        body.insertBefore(c, body.firstChild);
        iDiv = c.firstChild;
        cDiv = iDiv.firstChild;
        td = iDiv.nextSibling.firstChild.firstChild;
        noAddBorder = (cDiv .offsetTop !== 5);
        noAddBorderForTables = (td.offsetTop === 5);
        cDiv .style.position = "fixed";
        cDiv .style.top = "20px";
        suppFixedPos = (cDiv .offsetTop === 20 || cDiv .offsetTop === 15);
        cDiv .style.position = cDiv .style.top = "";
        iDiv.style.overflow = "hidden";
        iDiv.style.position = "relative";
        subBorderForOverflow = (cDiv .offsetTop === -5);
        noMarginBodyOff = (body.offsetTop !== bodyMarginTop);
        body.removeChild(c);
        init = true;
      },
      bodyOffset = function(body){
        var top = body.offsetTop, left = body.offsetLeft;
        initialize();
        if (noMarginBodyOff){
          top  += parseFloat( css(body, "marginTop") ) || 0;
          left += parseFloat( css(body, "marginLeft") ) || 0;
        }
        return { top: top, left: left };
      };

  $['fn']['offset'] = function(){
    var el = this[0], box;
    if (!el || !el.ownerDocument) return null;
    if (el === el.ownerDocument.body) return bodyOffset(el);
    try {
      box = el.getBoundingClientRect();
    } catch(e) {}
    if (!box || !$['contains'](docEl, el))
      return box ? { top: box.top, left: box.left } : { top: 0, left: 0 };
    var body = doc.body,
      win = getWin(doc),
      clientTop  = docEl.clientTop  || body.clientTop  || 0,
      clientLeft = docEl.clientLeft || body.clientLeft || 0,
      scrollTop  = win['pageYOffset'] || $['support']['boxModel'] && docEl.scrollTop  || body.scrollTop,
      scrollLeft = win['pageXOffset'] || $['support']['boxModel'] && docEl.scrollLeft || body.scrollLeft,
      top  = box.top + scrollTop - clientTop,
      left = box.left + scrollLeft - clientLeft;
    return { top: top, left: left };
  };

  $['fn']['position'] = function() {
    if (!this[0]) return null;
    var el = this[0],
    offPar = this['offsetParent'](),
    off = this['offset'](),
    parOff = rroot.test(offPar[0].nodeName) ? { top: 0, left: 0 } : offPar['offset']();
    off.top -= parseFloat(css(el, "marginTop")) || 0;
    off.left -= parseFloat(css(el, "marginLeft")) || 0;
    parOff.top += parseFloat(css(offPar[0], "borderTopWidth")) || 0;
    parOff.left += parseFloat(css(offPar[0], "borderLeftWidth")) || 0;
    return { top: off.top - parOff.top, left: off.left - parOff.left };
  };

  $['fn']['offsetParent'] = function(){
    return this['map'](function(){
      var op = this.offsetParent || doc.body;
      while (op && (!rroot.test(op.nodeName) && css(op,"position") === "static"))
        op = op.offsetParent;
      return op;
    });
  };

  $['_each'](["Height", "Width"], function (name, i) {
      var type = name.toLowerCase();
      $['fn']["inner" + name] = function () {
          var el = this[0];
          return el && el.style ? parseFloat(css(el, type, "padding")) : null;
      };
      $['fn']["outer" + name] = function (margin) {
          var el = this[0];
          return el && el.style ? parseFloat(css(el, type, margin ? "margin" : "border")) : null;
      };
      $['fn'][type] = function (size) {
          var el = this[0];
          if (!el) return size == null ? null : this;
          if ($['isFunction'](size))
              return this['each'](function (i) {
                  var self = $(this);
                  self[type](size.call(this, i, self[type]()));
              });
          if ($['isWindow'](el)) {
              var docElProp = el.document.documentElement["client" + name], body = el.document.body;
              return el.document.compatMode === "CSS1Compat" && docElProp || body && body["client" + name] || docElProp;
          } else if (el.nodeType === 9) {
              return Math.max(
            el.documentElement["client" + name],
            el.body["scroll" + name], el.documentElement["scroll" + name],
            el.body["offset" + name], el.documentElement["offset" + name]);
          } else if (size === undefined) {
              var orig = css(el, type),
                  ret = parseFloat(orig);
              return $['isNaN'](ret) ? orig : ret;
          }
          else return this['css'](type, typeof size === "string" ? size : size + "px");
      };
  });

  function getWin(el) { return $['isWindow'](el) ? el : el.nodeType === 9 ? el.defaultView || el.parentWindow : false; }

  $['_each'](["Left", "Top"], function (name, i) {
      var method = "scroll" + name;
      $['fn'][method] = function (val) {
          var el, win;
          if (val === undefined) {
              el = this[0];
              if (!el) return null;
              win = getWin(el);
              return win ? ("pageXOffset" in win)
                  ? win[i ? "pageYOffset" : "pageXOffset"]
                  : $['support']['boxModel'] && win.document.documentElement[method] || win.document.body[method] : el[method];
          }
          return this['each'](function() {
              win = getWin(this);
              if (win)
                  win['scrollTo'](!i ? val : $(win)['scrollLeft'](), i ? val : $(win)['scrollTop']());
              else
                  this[method] = val;
          });
      };
  });

});
;$['plug']("custom", function($){
  var win=window, doc=document, qsMap = {}, 
      vars = win.location.search.substring(1).split("&"),
      Key = $['Key'] = function (keyCode) { this.keyCode = keyCode; };

  for (var i = 0; i < vars.length; i++) {
      var kvp = vars[i].split("=");
      qsMap[kvp[0]] = unescape(kvp[1]);
  }

  $['queryString'] = function (name) { return qsMap[name]; };

  Key.namedKeys = {
    Backspace: 8, Tab: 9, Enter: 13, Shift: 16, Ctrl: 17, Alt: 18, Pause: 19, Capslock: 20, Escape: 27, PageUp: 33, 
    PageDown: 34, End: 35, Home: 36, LeftArrow: 37, UpArrow: 38, RightArrow: 39, DownArrow: 40, Insert: 45, Delete: 46
  };

  $['_each'](Key.namedKeys, function (val, key) {
    var keyCode = val;
    Key.prototype['is' + key] = function () { return this.keyCode === keyCode; };
  });

  $.key = function (e) {
    e = e || window.event;
    return new Key(e.keyCode || e.which);
  };

  $['cancelEvent'] = function (e) {
    if (!e) e = window.event;
    e.cancelBubble = true;
    e.returnValue = false;
    if (e.stopPropagation) {
      e.stopPropagation();
      e.preventDefault();
    }
    return false;
  };

  $['templateSettings'] = {
    evaluate    : /<%([\s\S]+?)%>/g,
    interpolate : /<%=([\s\S]+?)%>/g,
    escape      : /<%-([\s\S]+?)%>/g
  };

  $['_template'] = function(str, data) {
      var c  = $['templateSettings'];
      var tmpl = 'var __p=[],print=function(){__p.push.apply(__p,arguments);};' +
        'with(obj||{}){__p.push(\'' +
        str.replace(/\\/g, '\\\\')
           .replace(/'/g, "\\'")
           .replace(c.escape, function(match, code) {
             return "',_.escape(" + code.replace(/\\'/g, "'") + "),'";
           })
           .replace(c.interpolate, function(match, code) {
             return "'," + code.replace(/\\'/g, "'") + ",'";
           })
           .replace(c.evaluate || null, function(match, code) {
             return "');" + code.replace(/\\'/g, "'")
                                .replace(/[\r\n\t]/g, ' ') + ";__p.push('";
           })
           .replace(/\r/g, '\\r')
           .replace(/\n/g, '\\n')
           .replace(/\t/g, '\\t')
           + "');}return __p.join('');";
      var func = new Function('obj', '$', tmpl);
      return data ? func(data, $) : function(data) { return func(data, $) };
  };
});
;$['plug']("docready", function ($) {
  var win = window,
      doc = document,
      DOMContentLoaded,
      readyBound,
      readyList = [],
      isReady = false,
      readyWait = 1;

  $['hook'](function (sel, ctx) {
      if (typeof sel == "function") {
          this['ready'](sel);
          return true;
      }
  });

  function doScrollCheck() {
    if (isReady) return;
    try {
      doc.documentElement.doScroll("left");
    } catch (e) {
      setTimeout(doScrollCheck, 1);
      return;
    }
    ready();
  }

  function ready(wait) {
    if (wait === true) readyWait--;
    if (!readyWait || (wait !== true && !isReady)) {
      if (!doc.body) return setTimeout(ready, 1);
      isReady = true;
      if (wait !== true && --readyWait > 0) return;
      if (readyList) {
        var fn, i = 0, ready = readyList;
        readyList = null;
        while ((fn = ready[i++])) fn.call(doc, $);
        if ($['fn']['trigger']) $(doc)['trigger']("ready")['unbind']("ready");
      }
    }
  } $['ready'] = ready;

  DOMContentLoaded = doc.addEventListener
  ? function () {
    doc.removeEventListener("DOMContentLoaded", DOMContentLoaded, false);
    ready(); }
  : function () {
    if (doc.readyState === "complete") {
      doc.detachEvent("onreadystatechange", DOMContentLoaded);
      ready();
    }
  };

  $['bindReady'] = function() {
    if (readyBound) return;
    readyBound = true;
    if (doc.readyState === "complete") return setTimeout(ready, 1);

    if (doc.addEventListener) {
      doc.addEventListener("DOMContentLoaded", DOMContentLoaded, false);
      win.addEventListener("load", ready, false);
    } else if (doc.attachEvent) {
      doc.attachEvent("onreadystatechange", DOMContentLoaded);
      win.attachEvent("onload", ready);
      var toplevel = false;
      try { toplevel = window.frameElement == null; } catch (e) { }
      if (doc.documentElement.doScroll && toplevel) doScrollCheck();
    }
  };

  $['fn']['ready'] = function (fn) {
    $['bindReady']();
       if (isReady) fn.call(doc, $);
       else if (readyList) readyList.push(fn);
       return this;
   };

  if (!$['init']) $(document)['ready']($['onload']);
});
;$['plug']("events", function($){
  var doc = document, handlers = {}, _jquid = 1;
  function jquid(el){
    return el._jquid || (el._jquid = _jquid++);
  }
  function bind(o, type, fn){
    if (o.addEventListener)
      o.addEventListener(type, fn, false);
    else {
      o['e' + type + fn] = fn;
      o[type + fn] = function(){
        o['e' + type + fn](window.event);
      };
      o.attachEvent('on' + type, o[type + fn]);
    }
  } $['bind'] = bind;
  function unbind(o, type, fn){
    if (o.removeEventListener)
      o.removeEventListener(type, fn, false);
    else {
      o.detachEvent('on' + type, o[type + fn]);
      o[type + fn] = null;
    }
  } $['unbind'] = unbind;
  function parseEvt(evt){
    var parts = ('' + evt).split('.');
    return {e: parts[0], ns: parts.slice(1).sort().join(' ')};
  }
  function matcherFor(ns){
    return new RegExp('(?:^| )' + ns.replace(' ', ' .* ?') + '(?: |$)');
  }
  function findHdls(el, evt, fn, sel){
    evt = parseEvt(evt);
    if (evt.ns) var m = matcherFor(evt.ns);
    return $['_filter'](handlers[jquid(el)] || [], function(hdl){
      return hdl
        && (!evt.e  || hdl.e == evt.e)
        && (!evt.ns || m.test(hdl.ns))
        && (!fn     || hdl.fn == fn)
        && (!sel    || hdl.sel == sel);
    });
  }
  function addEvt(el, evts, fn, sel, delegate){
    var id = jquid(el), set = (handlers[id] || (handlers[id] = []));
    $['_each'](evts.split(/\s/), function(evt){
      var handler = $['extend'](parseEvt(evt), {fn: fn, sel: sel, del: delegate, i: set.length});
      set.push(handler);
      bind(el, handler.e, delegate || fn);
    });
    el = null;
  }
  function remEvt(el, evts, fn, sel){
    var id = jquid(el);
    $['_each']((evts || '').split(/\s/), function(evt){
      $['_each'](findHdls(el, evt, fn, sel), function(hdl){
        delete handlers[id][hdl.i];
        unbind(el, hdl.e, hdl.del || hdl.fn);
      });
    });
  }
  var evtMethods = ['preventDefault', 'stopImmediatePropagation', 'stopPropagation'];
  function createProxy(evt){
    var proxy = $['extend']({originalEvent: evt}, evt);
    $['_each'](evtMethods, function(key){
      if(evt[key]){
        proxy[key] = function(){
          return evt[key].apply(evt, arguments);
        };
      }
    });
    return proxy;
  }
  var p = $['fn'];
  $['_each'](("blur focus focusin focusout load resize scroll unload click dblclick " +
    "mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
    "change select submit keydown keypress keyup error").split(" "),
    function(name){
      p[name] = function(fn, data){
        return arguments.length > 0 ? this['bind'](name, fn, data) : this['trigger'](name);
      };
    }
  );
  p['bind'] = function(type, cb){
    return this['each'](function(){
      addEvt(this, type, cb);
    });
  };
  p['unbind'] = function(type, cb){
    return this['each'](function(){
       remEvt(this, type, cb);
    });
  };
  p['one'] = function(evt, cb){
    return this['each'](function(){
      var self = this;
      addEvt(this, evt, function wrapper(){
        cb.apply(self, arguments);
        remEvt(self, evt, arguments.callee);
      });
    });
  };
  p['delegate'] = function(sel, evt, cb){
    return this['each'](function(i, el){
      addEvt(el, evt, cb, sel, function(e){
        var target = e.target||e.srcElement, nodes = $['$$'](sel, el);
        while (target && $['_indexOf'](nodes, target) < 0)
          target = target.parentNode;
        if (target && !(target === el) && !(target === document)){
          cb.call(target, $['extend'](createProxy(e||window.event), {
            currentTarget: target, liveFired: el
          }));
        }
      });
    });
  };
  p['undelegate'] = function(sel, evt, cb){
    return this['each'](function(){
       remEvt(this, evt, cb, sel);
    });
  };
  p['live'] = function(evt, cb){
    $(doc.body)['delegate'](this['selector'], evt, cb);
    return this;
  };
  p['die'] = function(evt, cb){
    $(doc.body)['undelegate'](this['selector'], evt, cb);
    return this;
  };

  p['on'] = function(evt, sel, cb){
    return typeof sel === 'function' ? this.bind(evt, sel) : this.delegate(sel, evt, cb);
  };

  p['off'] = function(evt, sel, cb){
    return typeof sel === 'string' ? this.undelegate(sel, evt, cb) : this.unbind(evt, cb);
  };
    p['trigger'] = function (evt) {
        return this['each'](function () {
            if ((evt == "click" || evt == "blur" || evt == "focus") && this[evt])
                return this[evt]();
            if (doc.createEvent) {
                var e = doc.createEvent('Events');
                this.dispatchEvent(e, e.initEvent(evt, true, true));
            } else if (this.fireEvent)
                try {
                    if (evt !== "ready") {
                        this.fireEvent("on" + evt);
                    }
                } catch (e) { }
        });
    };
  if (!$['init']) $(window)['bind']("load",$['onload']);
});
;
},{}],"backbone":[function(require,module,exports){
(function (global){
//     Backbone.js 1.2.1

//     (c) 2010-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Backbone may be freely distributed under the MIT license.
//     For all details and documentation:
//     http://backbonejs.org

(function(factory) {

  // Establish the root object, `window` (`self`) in the browser, or `global` on the server.
  // We use `self` instead of `window` for `WebWorker` support.
  var root = (typeof self == 'object' && self.self == self && self) ||
            (typeof global == 'object' && global.global == global && global);

  // Set up Backbone appropriately for the environment. Start with AMD.
  if (typeof define === 'function' && define.amd) {
    define(['underscore', 'jquery', 'exports'], function(_, $, exports) {
      // Export global even in AMD case in case this script is loaded with
      // others that may still expect a global Backbone.
      root.Backbone = factory(root, exports, _, $);
    });

  // Next for Node.js or CommonJS. jQuery may not be needed as a module.
  } else if (typeof exports !== 'undefined') {
    var _ = require('underscore'), $;
    try { $ = require('jquery'); } catch(e) {}
    factory(root, exports, _, $);

  // Finally, as a browser global.
  } else {
    root.Backbone = factory(root, {}, root._, (root.jQuery || root.Zepto || root.ender || root.$));
  }

}(function(root, Backbone, _, $) {

  // Initial Setup
  // -------------

  // Save the previous value of the `Backbone` variable, so that it can be
  // restored later on, if `noConflict` is used.
  var previousBackbone = root.Backbone;

  // Create a local reference to a common array method we'll want to use later.
  var slice = [].slice;

  // Current version of the library. Keep in sync with `package.json`.
  Backbone.VERSION = '1.2.1';

  // For Backbone's purposes, jQuery, Zepto, Ender, or My Library (kidding) owns
  // the `$` variable.
  Backbone.$ = $;

  // Runs Backbone.js in *noConflict* mode, returning the `Backbone` variable
  // to its previous owner. Returns a reference to this Backbone object.
  Backbone.noConflict = function() {
    root.Backbone = previousBackbone;
    return this;
  };

  // Turn on `emulateHTTP` to support legacy HTTP servers. Setting this option
  // will fake `"PATCH"`, `"PUT"` and `"DELETE"` requests via the `_method` parameter and
  // set a `X-Http-Method-Override` header.
  Backbone.emulateHTTP = false;

  // Turn on `emulateJSON` to support legacy servers that can't deal with direct
  // `application/json` requests ... this will encode the body as
  // `application/x-www-form-urlencoded` instead and will send the model in a
  // form param named `model`.
  Backbone.emulateJSON = false;

  // Proxy Underscore methods to a Backbone class' prototype using a
  // particular attribute as the data argument
  var addMethod = function(length, method, attribute) {
    switch (length) {
      case 1: return function() {
        return _[method](this[attribute]);
      };
      case 2: return function(value) {
        return _[method](this[attribute], value);
      };
      case 3: return function(iteratee, context) {
        return _[method](this[attribute], iteratee, context);
      };
      case 4: return function(iteratee, defaultVal, context) {
        return _[method](this[attribute], iteratee, defaultVal, context);
      };
      default: return function() {
        var args = slice.call(arguments);
        args.unshift(this[attribute]);
        return _[method].apply(_, args);
      };
    }
  };
  var addUnderscoreMethods = function(Class, methods, attribute) {
    _.each(methods, function(length, method) {
      if (_[method]) Class.prototype[method] = addMethod(length, method, attribute);
    });
  };

  // Backbone.Events
  // ---------------

  // A module that can be mixed in to *any object* in order to provide it with
  // custom events. You may bind with `on` or remove with `off` callback
  // functions to an event; `trigger`-ing an event fires all callbacks in
  // succession.
  //
  //     var object = {};
  //     _.extend(object, Backbone.Events);
  //     object.on('expand', function(){ alert('expanded'); });
  //     object.trigger('expand');
  //
  var Events = Backbone.Events = {};

  // Regular expression used to split event strings.
  var eventSplitter = /\s+/;

  // Iterates over the standard `event, callback` (as well as the fancy multiple
  // space-separated events `"change blur", callback` and jQuery-style event
  // maps `{event: callback}`), reducing them by manipulating `memo`.
  // Passes a normalized single event name and callback, as well as any
  // optional `opts`.
  var eventsApi = function(iteratee, memo, name, callback, opts) {
    var i = 0, names;
    if (name && typeof name === 'object') {
      // Handle event maps.
      if (callback !== void 0 && 'context' in opts && opts.context === void 0) opts.context = callback;
      for (names = _.keys(name); i < names.length ; i++) {
        memo = iteratee(memo, names[i], name[names[i]], opts);
      }
    } else if (name && eventSplitter.test(name)) {
      // Handle space separated event names.
      for (names = name.split(eventSplitter); i < names.length; i++) {
        memo = iteratee(memo, names[i], callback, opts);
      }
    } else {
      memo = iteratee(memo, name, callback, opts);
    }
    return memo;
  };

  // Bind an event to a `callback` function. Passing `"all"` will bind
  // the callback to all events fired.
  Events.on = function(name, callback, context) {
    return internalOn(this, name, callback, context);
  };

  // An internal use `on` function, used to guard the `listening` argument from
  // the public API.
  var internalOn = function(obj, name, callback, context, listening) {
    obj._events = eventsApi(onApi, obj._events || {}, name, callback, {
        context: context,
        ctx: obj,
        listening: listening
    });

    if (listening) {
      var listeners = obj._listeners || (obj._listeners = {});
      listeners[listening.id] = listening;
    }

    return obj;
  };

  // Inversion-of-control versions of `on`. Tell *this* object to listen to
  // an event in another object... keeping track of what it's listening to.
  Events.listenTo =  function(obj, name, callback) {
    if (!obj) return this;
    var id = obj._listenId || (obj._listenId = _.uniqueId('l'));
    var listeningTo = this._listeningTo || (this._listeningTo = {});
    var listening = listeningTo[id];

    // This object is not listening to any other events on `obj` yet.
    // Setup the necessary references to track the listening callbacks.
    if (!listening) {
      var thisId = this._listenId || (this._listenId = _.uniqueId('l'));
      listening = listeningTo[id] = {obj: obj, objId: id, id: thisId, listeningTo: listeningTo, count: 0};
    }

    // Bind callbacks on obj, and keep track of them on listening.
    internalOn(obj, name, callback, this, listening);
    return this;
  };

  // The reducing API that adds a callback to the `events` object.
  var onApi = function(events, name, callback, options) {
    if (callback) {
      var handlers = events[name] || (events[name] = []);
      var context = options.context, ctx = options.ctx, listening = options.listening;
      if (listening) listening.count++;

      handlers.push({ callback: callback, context: context, ctx: context || ctx, listening: listening });
    }
    return events;
  };

  // Remove one or many callbacks. If `context` is null, removes all
  // callbacks with that function. If `callback` is null, removes all
  // callbacks for the event. If `name` is null, removes all bound
  // callbacks for all events.
  Events.off =  function(name, callback, context) {
    if (!this._events) return this;
    this._events = eventsApi(offApi, this._events, name, callback, {
        context: context,
        listeners: this._listeners
    });
    return this;
  };

  // Tell this object to stop listening to either specific events ... or
  // to every object it's currently listening to.
  Events.stopListening =  function(obj, name, callback) {
    var listeningTo = this._listeningTo;
    if (!listeningTo) return this;

    var ids = obj ? [obj._listenId] : _.keys(listeningTo);

    for (var i = 0; i < ids.length; i++) {
      var listening = listeningTo[ids[i]];

      // If listening doesn't exist, this object is not currently
      // listening to obj. Break out early.
      if (!listening) break;

      listening.obj.off(name, callback, this);
    }
    if (_.isEmpty(listeningTo)) this._listeningTo = void 0;

    return this;
  };

  // The reducing API that removes a callback from the `events` object.
  var offApi = function(events, name, callback, options) {
    // No events to consider.
    if (!events) return;

    var i = 0, listening;
    var context = options.context, listeners = options.listeners;

    // Delete all events listeners and "drop" events.
    if (!name && !callback && !context) {
      var ids = _.keys(listeners);
      for (; i < ids.length; i++) {
        listening = listeners[ids[i]];
        delete listeners[listening.id];
        delete listening.listeningTo[listening.objId];
      }
      return;
    }

    var names = name ? [name] : _.keys(events);
    for (; i < names.length; i++) {
      name = names[i];
      var handlers = events[name];

      // Bail out if there are no events stored.
      if (!handlers) break;

      // Replace events if there are any remaining.  Otherwise, clean up.
      var remaining = [];
      for (var j = 0; j < handlers.length; j++) {
        var handler = handlers[j];
        if (
          callback && callback !== handler.callback &&
            callback !== handler.callback._callback ||
              context && context !== handler.context
        ) {
          remaining.push(handler);
        } else {
          listening = handler.listening;
          if (listening && --listening.count === 0) {
            delete listeners[listening.id];
            delete listening.listeningTo[listening.objId];
          }
        }
      }

      // Update tail event if the list has any events.  Otherwise, clean up.
      if (remaining.length) {
        events[name] = remaining;
      } else {
        delete events[name];
      }
    }
    if (_.size(events)) return events;
  };

  // Bind an event to only be triggered a single time. After the first time
  // the callback is invoked, it will be removed. When multiple events are
  // passed in using the space-separated syntax, the event will fire once for every
  // event you passed in, not once for a combination of all events
  Events.once =  function(name, callback, context) {
    // Map the event into a `{event: once}` object.
    var events = eventsApi(onceMap, {}, name, callback, _.bind(this.off, this));
    return this.on(events, void 0, context);
  };

  // Inversion-of-control versions of `once`.
  Events.listenToOnce =  function(obj, name, callback) {
    // Map the event into a `{event: once}` object.
    var events = eventsApi(onceMap, {}, name, callback, _.bind(this.stopListening, this, obj));
    return this.listenTo(obj, events);
  };

  // Reduces the event callbacks into a map of `{event: onceWrapper}`.
  // `offer` unbinds the `onceWrapper` after it has been called.
  var onceMap = function(map, name, callback, offer) {
    if (callback) {
      var once = map[name] = _.once(function() {
        offer(name, once);
        callback.apply(this, arguments);
      });
      once._callback = callback;
    }
    return map;
  };

  // Trigger one or many events, firing all bound callbacks. Callbacks are
  // passed the same arguments as `trigger` is, apart from the event name
  // (unless you're listening on `"all"`, which will cause your callback to
  // receive the true name of the event as the first argument).
  Events.trigger =  function(name) {
    if (!this._events) return this;

    var length = Math.max(0, arguments.length - 1);
    var args = Array(length);
    for (var i = 0; i < length; i++) args[i] = arguments[i + 1];

    eventsApi(triggerApi, this._events, name, void 0, args);
    return this;
  };

  // Handles triggering the appropriate event callbacks.
  var triggerApi = function(objEvents, name, cb, args) {
    if (objEvents) {
      var events = objEvents[name];
      var allEvents = objEvents.all;
      if (events && allEvents) allEvents = allEvents.slice();
      if (events) triggerEvents(events, args);
      if (allEvents) triggerEvents(allEvents, [name].concat(args));
    }
    return objEvents;
  };

  // A difficult-to-believe, but optimized internal dispatch function for
  // triggering events. Tries to keep the usual cases speedy (most internal
  // Backbone events have 3 arguments).
  var triggerEvents = function(events, args) {
    var ev, i = -1, l = events.length, a1 = args[0], a2 = args[1], a3 = args[2];
    switch (args.length) {
      case 0: while (++i < l) (ev = events[i]).callback.call(ev.ctx); return;
      case 1: while (++i < l) (ev = events[i]).callback.call(ev.ctx, a1); return;
      case 2: while (++i < l) (ev = events[i]).callback.call(ev.ctx, a1, a2); return;
      case 3: while (++i < l) (ev = events[i]).callback.call(ev.ctx, a1, a2, a3); return;
      default: while (++i < l) (ev = events[i]).callback.apply(ev.ctx, args); return;
    }
  };

  // Aliases for backwards compatibility.
  Events.bind   = Events.on;
  Events.unbind = Events.off;

  // Allow the `Backbone` object to serve as a global event bus, for folks who
  // want global "pubsub" in a convenient place.
  _.extend(Backbone, Events);

  // Backbone.Model
  // --------------

  // Backbone **Models** are the basic data object in the framework --
  // frequently representing a row in a table in a database on your server.
  // A discrete chunk of data and a bunch of useful, related methods for
  // performing computations and transformations on that data.

  // Create a new model with the specified attributes. A client id (`cid`)
  // is automatically generated and assigned for you.
  var Model = Backbone.Model = function(attributes, options) {
    var attrs = attributes || {};
    options || (options = {});
    this.cid = _.uniqueId(this.cidPrefix);
    this.attributes = {};
    if (options.collection) this.collection = options.collection;
    if (options.parse) attrs = this.parse(attrs, options) || {};
    attrs = _.defaults({}, attrs, _.result(this, 'defaults'));
    this.set(attrs, options);
    this.changed = {};
    this.initialize.apply(this, arguments);
  };

  // Attach all inheritable methods to the Model prototype.
  _.extend(Model.prototype, Events, {

    // A hash of attributes whose current and previous value differ.
    changed: null,

    // The value returned during the last failed validation.
    validationError: null,

    // The default name for the JSON `id` attribute is `"id"`. MongoDB and
    // CouchDB users may want to set this to `"_id"`.
    idAttribute: 'id',

    // The prefix is used to create the client id which is used to identify models locally.
    // You may want to override this if you're experiencing name clashes with model ids.
    cidPrefix: 'c',

    // Initialize is an empty function by default. Override it with your own
    // initialization logic.
    initialize: function(){},

    // Return a copy of the model's `attributes` object.
    toJSON: function(options) {
      return _.clone(this.attributes);
    },

    // Proxy `Backbone.sync` by default -- but override this if you need
    // custom syncing semantics for *this* particular model.
    sync: function() {
      return Backbone.sync.apply(this, arguments);
    },

    // Get the value of an attribute.
    get: function(attr) {
      return this.attributes[attr];
    },

    // Get the HTML-escaped value of an attribute.
    escape: function(attr) {
      return _.escape(this.get(attr));
    },

    // Returns `true` if the attribute contains a value that is not null
    // or undefined.
    has: function(attr) {
      return this.get(attr) != null;
    },

    // Special-cased proxy to underscore's `_.matches` method.
    matches: function(attrs) {
      return !!_.iteratee(attrs, this)(this.attributes);
    },

    // Set a hash of model attributes on the object, firing `"change"`. This is
    // the core primitive operation of a model, updating the data and notifying
    // anyone who needs to know about the change in state. The heart of the beast.
    set: function(key, val, options) {
      if (key == null) return this;

      // Handle both `"key", value` and `{key: value}` -style arguments.
      var attrs;
      if (typeof key === 'object') {
        attrs = key;
        options = val;
      } else {
        (attrs = {})[key] = val;
      }

      options || (options = {});

      // Run validation.
      if (!this._validate(attrs, options)) return false;

      // Extract attributes and options.
      var unset      = options.unset;
      var silent     = options.silent;
      var changes    = [];
      var changing   = this._changing;
      this._changing = true;

      if (!changing) {
        this._previousAttributes = _.clone(this.attributes);
        this.changed = {};
      }

      var current = this.attributes;
      var changed = this.changed;
      var prev    = this._previousAttributes;

      // Check for changes of `id`.
      if (this.idAttribute in attrs) this.id = attrs[this.idAttribute];

      // For each `set` attribute, update or delete the current value.
      for (var attr in attrs) {
        val = attrs[attr];
        if (!_.isEqual(current[attr], val)) changes.push(attr);
        if (!_.isEqual(prev[attr], val)) {
          changed[attr] = val;
        } else {
          delete changed[attr];
        }
        unset ? delete current[attr] : current[attr] = val;
      }

      // Trigger all relevant attribute changes.
      if (!silent) {
        if (changes.length) this._pending = options;
        for (var i = 0; i < changes.length; i++) {
          this.trigger('change:' + changes[i], this, current[changes[i]], options);
        }
      }

      // You might be wondering why there's a `while` loop here. Changes can
      // be recursively nested within `"change"` events.
      if (changing) return this;
      if (!silent) {
        while (this._pending) {
          options = this._pending;
          this._pending = false;
          this.trigger('change', this, options);
        }
      }
      this._pending = false;
      this._changing = false;
      return this;
    },

    // Remove an attribute from the model, firing `"change"`. `unset` is a noop
    // if the attribute doesn't exist.
    unset: function(attr, options) {
      return this.set(attr, void 0, _.extend({}, options, {unset: true}));
    },

    // Clear all attributes on the model, firing `"change"`.
    clear: function(options) {
      var attrs = {};
      for (var key in this.attributes) attrs[key] = void 0;
      return this.set(attrs, _.extend({}, options, {unset: true}));
    },

    // Determine if the model has changed since the last `"change"` event.
    // If you specify an attribute name, determine if that attribute has changed.
    hasChanged: function(attr) {
      if (attr == null) return !_.isEmpty(this.changed);
      return _.has(this.changed, attr);
    },

    // Return an object containing all the attributes that have changed, or
    // false if there are no changed attributes. Useful for determining what
    // parts of a view need to be updated and/or what attributes need to be
    // persisted to the server. Unset attributes will be set to undefined.
    // You can also pass an attributes object to diff against the model,
    // determining if there *would be* a change.
    changedAttributes: function(diff) {
      if (!diff) return this.hasChanged() ? _.clone(this.changed) : false;
      var old = this._changing ? this._previousAttributes : this.attributes;
      var changed = {};
      for (var attr in diff) {
        var val = diff[attr];
        if (_.isEqual(old[attr], val)) continue;
        changed[attr] = val;
      }
      return _.size(changed) ? changed : false;
    },

    // Get the previous value of an attribute, recorded at the time the last
    // `"change"` event was fired.
    previous: function(attr) {
      if (attr == null || !this._previousAttributes) return null;
      return this._previousAttributes[attr];
    },

    // Get all of the attributes of the model at the time of the previous
    // `"change"` event.
    previousAttributes: function() {
      return _.clone(this._previousAttributes);
    },

    // Fetch the model from the server, merging the response with the model's
    // local attributes. Any changed attributes will trigger a "change" event.
    fetch: function(options) {
      options = _.extend({parse: true}, options);
      var model = this;
      var success = options.success;
      options.success = function(resp) {
        var serverAttrs = options.parse ? model.parse(resp, options) : resp;
        if (!model.set(serverAttrs, options)) return false;
        if (success) success.call(options.context, model, resp, options);
        model.trigger('sync', model, resp, options);
      };
      wrapError(this, options);
      return this.sync('read', this, options);
    },

    // Set a hash of model attributes, and sync the model to the server.
    // If the server returns an attributes hash that differs, the model's
    // state will be `set` again.
    save: function(key, val, options) {
      // Handle both `"key", value` and `{key: value}` -style arguments.
      var attrs;
      if (key == null || typeof key === 'object') {
        attrs = key;
        options = val;
      } else {
        (attrs = {})[key] = val;
      }

      options = _.extend({validate: true, parse: true}, options);
      var wait = options.wait;

      // If we're not waiting and attributes exist, save acts as
      // `set(attr).save(null, opts)` with validation. Otherwise, check if
      // the model will be valid when the attributes, if any, are set.
      if (attrs && !wait) {
        if (!this.set(attrs, options)) return false;
      } else {
        if (!this._validate(attrs, options)) return false;
      }

      // After a successful server-side save, the client is (optionally)
      // updated with the server-side state.
      var model = this;
      var success = options.success;
      var attributes = this.attributes;
      options.success = function(resp) {
        // Ensure attributes are restored during synchronous saves.
        model.attributes = attributes;
        var serverAttrs = options.parse ? model.parse(resp, options) : resp;
        if (wait) serverAttrs = _.extend({}, attrs, serverAttrs);
        if (serverAttrs && !model.set(serverAttrs, options)) return false;
        if (success) success.call(options.context, model, resp, options);
        model.trigger('sync', model, resp, options);
      };
      wrapError(this, options);

      // Set temporary attributes if `{wait: true}` to properly find new ids.
      if (attrs && wait) this.attributes = _.extend({}, attributes, attrs);

      var method = this.isNew() ? 'create' : (options.patch ? 'patch' : 'update');
      if (method === 'patch' && !options.attrs) options.attrs = attrs;
      var xhr = this.sync(method, this, options);

      // Restore attributes.
      this.attributes = attributes;

      return xhr;
    },

    // Destroy this model on the server if it was already persisted.
    // Optimistically removes the model from its collection, if it has one.
    // If `wait: true` is passed, waits for the server to respond before removal.
    destroy: function(options) {
      options = options ? _.clone(options) : {};
      var model = this;
      var success = options.success;
      var wait = options.wait;

      var destroy = function() {
        model.stopListening();
        model.trigger('destroy', model, model.collection, options);
      };

      options.success = function(resp) {
        if (wait) destroy();
        if (success) success.call(options.context, model, resp, options);
        if (!model.isNew()) model.trigger('sync', model, resp, options);
      };

      var xhr = false;
      if (this.isNew()) {
        _.defer(options.success);
      } else {
        wrapError(this, options);
        xhr = this.sync('delete', this, options);
      }
      if (!wait) destroy();
      return xhr;
    },

    // Default URL for the model's representation on the server -- if you're
    // using Backbone's restful methods, override this to change the endpoint
    // that will be called.
    url: function() {
      var base =
        _.result(this, 'urlRoot') ||
        _.result(this.collection, 'url') ||
        urlError();
      if (this.isNew()) return base;
      var id = this.get(this.idAttribute);
      return base.replace(/[^\/]$/, '$&/') + encodeURIComponent(id);
    },

    // **parse** converts a response into the hash of attributes to be `set` on
    // the model. The default implementation is just to pass the response along.
    parse: function(resp, options) {
      return resp;
    },

    // Create a new model with identical attributes to this one.
    clone: function() {
      return new this.constructor(this.attributes);
    },

    // A model is new if it has never been saved to the server, and lacks an id.
    isNew: function() {
      return !this.has(this.idAttribute);
    },

    // Check if the model is currently in a valid state.
    isValid: function(options) {
      return this._validate({}, _.defaults({validate: true}, options));
    },

    // Run validation against the next complete set of model attributes,
    // returning `true` if all is well. Otherwise, fire an `"invalid"` event.
    _validate: function(attrs, options) {
      if (!options.validate || !this.validate) return true;
      attrs = _.extend({}, this.attributes, attrs);
      var error = this.validationError = this.validate(attrs, options) || null;
      if (!error) return true;
      this.trigger('invalid', this, error, _.extend(options, {validationError: error}));
      return false;
    }

  });

  // Underscore methods that we want to implement on the Model.
  var modelMethods = { keys: 1, values: 1, pairs: 1, invert: 1, pick: 0,
      omit: 0, chain: 1, isEmpty: 1 };

  // Mix in each Underscore method as a proxy to `Model#attributes`.
  addUnderscoreMethods(Model, modelMethods, 'attributes');

  // Backbone.Collection
  // -------------------

  // If models tend to represent a single row of data, a Backbone Collection is
  // more analogous to a table full of data ... or a small slice or page of that
  // table, or a collection of rows that belong together for a particular reason
  // -- all of the messages in this particular folder, all of the documents
  // belonging to this particular author, and so on. Collections maintain
  // indexes of their models, both in order, and for lookup by `id`.

  // Create a new **Collection**, perhaps to contain a specific type of `model`.
  // If a `comparator` is specified, the Collection will maintain
  // its models in sort order, as they're added and removed.
  var Collection = Backbone.Collection = function(models, options) {
    options || (options = {});
    if (options.model) this.model = options.model;
    if (options.comparator !== void 0) this.comparator = options.comparator;
    this._reset();
    this.initialize.apply(this, arguments);
    if (models) this.reset(models, _.extend({silent: true}, options));
  };

  // Default options for `Collection#set`.
  var setOptions = {add: true, remove: true, merge: true};
  var addOptions = {add: true, remove: false};

  // Define the Collection's inheritable methods.
  _.extend(Collection.prototype, Events, {

    // The default model for a collection is just a **Backbone.Model**.
    // This should be overridden in most cases.
    model: Model,

    // Initialize is an empty function by default. Override it with your own
    // initialization logic.
    initialize: function(){},

    // The JSON representation of a Collection is an array of the
    // models' attributes.
    toJSON: function(options) {
      return this.map(function(model) { return model.toJSON(options); });
    },

    // Proxy `Backbone.sync` by default.
    sync: function() {
      return Backbone.sync.apply(this, arguments);
    },

    // Add a model, or list of models to the set.
    add: function(models, options) {
      return this.set(models, _.extend({merge: false}, options, addOptions));
    },

    // Remove a model, or a list of models from the set.
    remove: function(models, options) {
      options = _.extend({}, options);
      var singular = !_.isArray(models);
      models = singular ? [models] : _.clone(models);
      var removed = this._removeModels(models, options);
      if (!options.silent && removed) this.trigger('update', this, options);
      return singular ? removed[0] : removed;
    },

    // Update a collection by `set`-ing a new list of models, adding new ones,
    // removing models that are no longer present, and merging models that
    // already exist in the collection, as necessary. Similar to **Model#set**,
    // the core operation for updating the data contained by the collection.
    set: function(models, options) {
      options = _.defaults({}, options, setOptions);
      if (options.parse && !this._isModel(models)) models = this.parse(models, options);
      var singular = !_.isArray(models);
      models = singular ? (models ? [models] : []) : models.slice();
      var id, model, attrs, existing, sort;
      var at = options.at;
      if (at != null) at = +at;
      if (at < 0) at += this.length + 1;
      var sortable = this.comparator && (at == null) && options.sort !== false;
      var sortAttr = _.isString(this.comparator) ? this.comparator : null;
      var toAdd = [], toRemove = [], modelMap = {};
      var add = options.add, merge = options.merge, remove = options.remove;
      var order = !sortable && add && remove ? [] : false;
      var orderChanged = false;

      // Turn bare objects into model references, and prevent invalid models
      // from being added.
      for (var i = 0; i < models.length; i++) {
        attrs = models[i];

        // If a duplicate is found, prevent it from being added and
        // optionally merge it into the existing model.
        if (existing = this.get(attrs)) {
          if (remove) modelMap[existing.cid] = true;
          if (merge && attrs !== existing) {
            attrs = this._isModel(attrs) ? attrs.attributes : attrs;
            if (options.parse) attrs = existing.parse(attrs, options);
            existing.set(attrs, options);
            if (sortable && !sort && existing.hasChanged(sortAttr)) sort = true;
          }
          models[i] = existing;

        // If this is a new, valid model, push it to the `toAdd` list.
        } else if (add) {
          model = models[i] = this._prepareModel(attrs, options);
          if (!model) continue;
          toAdd.push(model);
          this._addReference(model, options);
        }

        // Do not add multiple models with the same `id`.
        model = existing || model;
        if (!model) continue;
        id = this.modelId(model.attributes);
        if (order && (model.isNew() || !modelMap[id])) {
          order.push(model);

          // Check to see if this is actually a new model at this index.
          orderChanged = orderChanged || !this.models[i] || model.cid !== this.models[i].cid;
        }

        modelMap[id] = true;
      }

      // Remove nonexistent models if appropriate.
      if (remove) {
        for (var i = 0; i < this.length; i++) {
          if (!modelMap[(model = this.models[i]).cid]) toRemove.push(model);
        }
        if (toRemove.length) this._removeModels(toRemove, options);
      }

      // See if sorting is needed, update `length` and splice in new models.
      if (toAdd.length || orderChanged) {
        if (sortable) sort = true;
        this.length += toAdd.length;
        if (at != null) {
          for (var i = 0; i < toAdd.length; i++) {
            this.models.splice(at + i, 0, toAdd[i]);
          }
        } else {
          if (order) this.models.length = 0;
          var orderedModels = order || toAdd;
          for (var i = 0; i < orderedModels.length; i++) {
            this.models.push(orderedModels[i]);
          }
        }
      }

      // Silently sort the collection if appropriate.
      if (sort) this.sort({silent: true});

      // Unless silenced, it's time to fire all appropriate add/sort events.
      if (!options.silent) {
        var addOpts = at != null ? _.clone(options) : options;
        for (var i = 0; i < toAdd.length; i++) {
          if (at != null) addOpts.index = at + i;
          (model = toAdd[i]).trigger('add', model, this, addOpts);
        }
        if (sort || orderChanged) this.trigger('sort', this, options);
        if (toAdd.length || toRemove.length) this.trigger('update', this, options);
      }

      // Return the added (or merged) model (or models).
      return singular ? models[0] : models;
    },

    // When you have more items than you want to add or remove individually,
    // you can reset the entire set with a new list of models, without firing
    // any granular `add` or `remove` events. Fires `reset` when finished.
    // Useful for bulk operations and optimizations.
    reset: function(models, options) {
      options = options ? _.clone(options) : {};
      for (var i = 0; i < this.models.length; i++) {
        this._removeReference(this.models[i], options);
      }
      options.previousModels = this.models;
      this._reset();
      models = this.add(models, _.extend({silent: true}, options));
      if (!options.silent) this.trigger('reset', this, options);
      return models;
    },

    // Add a model to the end of the collection.
    push: function(model, options) {
      return this.add(model, _.extend({at: this.length}, options));
    },

    // Remove a model from the end of the collection.
    pop: function(options) {
      var model = this.at(this.length - 1);
      return this.remove(model, options);
    },

    // Add a model to the beginning of the collection.
    unshift: function(model, options) {
      return this.add(model, _.extend({at: 0}, options));
    },

    // Remove a model from the beginning of the collection.
    shift: function(options) {
      var model = this.at(0);
      return this.remove(model, options);
    },

    // Slice out a sub-array of models from the collection.
    slice: function() {
      return slice.apply(this.models, arguments);
    },

    // Get a model from the set by id.
    get: function(obj) {
      if (obj == null) return void 0;
      var id = this.modelId(this._isModel(obj) ? obj.attributes : obj);
      return this._byId[obj] || this._byId[id] || this._byId[obj.cid];
    },

    // Get the model at the given index.
    at: function(index) {
      if (index < 0) index += this.length;
      return this.models[index];
    },

    // Return models with matching attributes. Useful for simple cases of
    // `filter`.
    where: function(attrs, first) {
      var matches = _.matches(attrs);
      return this[first ? 'find' : 'filter'](function(model) {
        return matches(model.attributes);
      });
    },

    // Return the first model with matching attributes. Useful for simple cases
    // of `find`.
    findWhere: function(attrs) {
      return this.where(attrs, true);
    },

    // Force the collection to re-sort itself. You don't need to call this under
    // normal circumstances, as the set will maintain sort order as each item
    // is added.
    sort: function(options) {
      if (!this.comparator) throw new Error('Cannot sort a set without a comparator');
      options || (options = {});

      // Run sort based on type of `comparator`.
      if (_.isString(this.comparator) || this.comparator.length === 1) {
        this.models = this.sortBy(this.comparator, this);
      } else {
        this.models.sort(_.bind(this.comparator, this));
      }

      if (!options.silent) this.trigger('sort', this, options);
      return this;
    },

    // Pluck an attribute from each model in the collection.
    pluck: function(attr) {
      return _.invoke(this.models, 'get', attr);
    },

    // Fetch the default set of models for this collection, resetting the
    // collection when they arrive. If `reset: true` is passed, the response
    // data will be passed through the `reset` method instead of `set`.
    fetch: function(options) {
      options = _.extend({parse: true}, options);
      var success = options.success;
      var collection = this;
      options.success = function(resp) {
        var method = options.reset ? 'reset' : 'set';
        collection[method](resp, options);
        if (success) success.call(options.context, collection, resp, options);
        collection.trigger('sync', collection, resp, options);
      };
      wrapError(this, options);
      return this.sync('read', this, options);
    },

    // Create a new instance of a model in this collection. Add the model to the
    // collection immediately, unless `wait: true` is passed, in which case we
    // wait for the server to agree.
    create: function(model, options) {
      options = options ? _.clone(options) : {};
      var wait = options.wait;
      model = this._prepareModel(model, options);
      if (!model) return false;
      if (!wait) this.add(model, options);
      var collection = this;
      var success = options.success;
      options.success = function(model, resp, callbackOpts) {
        if (wait) collection.add(model, callbackOpts);
        if (success) success.call(callbackOpts.context, model, resp, callbackOpts);
      };
      model.save(null, options);
      return model;
    },

    // **parse** converts a response into a list of models to be added to the
    // collection. The default implementation is just to pass it through.
    parse: function(resp, options) {
      return resp;
    },

    // Create a new collection with an identical list of models as this one.
    clone: function() {
      return new this.constructor(this.models, {
        model: this.model,
        comparator: this.comparator
      });
    },

    // Define how to uniquely identify models in the collection.
    modelId: function (attrs) {
      return attrs[this.model.prototype.idAttribute || 'id'];
    },

    // Private method to reset all internal state. Called when the collection
    // is first initialized or reset.
    _reset: function() {
      this.length = 0;
      this.models = [];
      this._byId  = {};
    },

    // Prepare a hash of attributes (or other model) to be added to this
    // collection.
    _prepareModel: function(attrs, options) {
      if (this._isModel(attrs)) {
        if (!attrs.collection) attrs.collection = this;
        return attrs;
      }
      options = options ? _.clone(options) : {};
      options.collection = this;
      var model = new this.model(attrs, options);
      if (!model.validationError) return model;
      this.trigger('invalid', this, model.validationError, options);
      return false;
    },

    // Internal method called by both remove and set.
    // Returns removed models, or false if nothing is removed.
    _removeModels: function(models, options) {
      var removed = [];
      for (var i = 0; i < models.length; i++) {
        var model = this.get(models[i]);
        if (!model) continue;

        var index = this.indexOf(model);
        this.models.splice(index, 1);
        this.length--;

        if (!options.silent) {
          options.index = index;
          model.trigger('remove', model, this, options);
        }

        removed.push(model);
        this._removeReference(model, options);
      }
      return removed.length ? removed : false;
    },

    // Method for checking whether an object should be considered a model for
    // the purposes of adding to the collection.
    _isModel: function (model) {
      return model instanceof Model;
    },

    // Internal method to create a model's ties to a collection.
    _addReference: function(model, options) {
      this._byId[model.cid] = model;
      var id = this.modelId(model.attributes);
      if (id != null) this._byId[id] = model;
      model.on('all', this._onModelEvent, this);
    },

    // Internal method to sever a model's ties to a collection.
    _removeReference: function(model, options) {
      delete this._byId[model.cid];
      var id = this.modelId(model.attributes);
      if (id != null) delete this._byId[id];
      if (this === model.collection) delete model.collection;
      model.off('all', this._onModelEvent, this);
    },

    // Internal method called every time a model in the set fires an event.
    // Sets need to update their indexes when models change ids. All other
    // events simply proxy through. "add" and "remove" events that originate
    // in other collections are ignored.
    _onModelEvent: function(event, model, collection, options) {
      if ((event === 'add' || event === 'remove') && collection !== this) return;
      if (event === 'destroy') this.remove(model, options);
      if (event === 'change') {
        var prevId = this.modelId(model.previousAttributes());
        var id = this.modelId(model.attributes);
        if (prevId !== id) {
          if (prevId != null) delete this._byId[prevId];
          if (id != null) this._byId[id] = model;
        }
      }
      this.trigger.apply(this, arguments);
    }

  });

  // Underscore methods that we want to implement on the Collection.
  // 90% of the core usefulness of Backbone Collections is actually implemented
  // right here:
  var collectionMethods = { forEach: 3, each: 3, map: 3, collect: 3, reduce: 4,
      foldl: 4, inject: 4, reduceRight: 4, foldr: 4, find: 3, detect: 3, filter: 3,
      select: 3, reject: 3, every: 3, all: 3, some: 3, any: 3, include: 2,
      contains: 2, invoke: 0, max: 3, min: 3, toArray: 1, size: 1, first: 3,
      head: 3, take: 3, initial: 3, rest: 3, tail: 3, drop: 3, last: 3,
      without: 0, difference: 0, indexOf: 3, shuffle: 1, lastIndexOf: 3,
      isEmpty: 1, chain: 1, sample: 3, partition: 3 };

  // Mix in each Underscore method as a proxy to `Collection#models`.
  addUnderscoreMethods(Collection, collectionMethods, 'models');

  // Underscore methods that take a property name as an argument.
  var attributeMethods = ['groupBy', 'countBy', 'sortBy', 'indexBy'];

  // Use attributes instead of properties.
  _.each(attributeMethods, function(method) {
    if (!_[method]) return;
    Collection.prototype[method] = function(value, context) {
      var iterator = _.isFunction(value) ? value : function(model) {
        return model.get(value);
      };
      return _[method](this.models, iterator, context);
    };
  });

  // Backbone.View
  // -------------

  // Backbone Views are almost more convention than they are actual code. A View
  // is simply a JavaScript object that represents a logical chunk of UI in the
  // DOM. This might be a single item, an entire list, a sidebar or panel, or
  // even the surrounding frame which wraps your whole app. Defining a chunk of
  // UI as a **View** allows you to define your DOM events declaratively, without
  // having to worry about render order ... and makes it easy for the view to
  // react to specific changes in the state of your models.

  // Creating a Backbone.View creates its initial element outside of the DOM,
  // if an existing element is not provided...
  var View = Backbone.View = function(options) {
    this.cid = _.uniqueId('view');
    _.extend(this, _.pick(options, viewOptions));
    this._ensureElement();
    this.initialize.apply(this, arguments);
  };

  // Cached regex to split keys for `delegate`.
  var delegateEventSplitter = /^(\S+)\s*(.*)$/;

  // List of view options to be merged as properties.
  var viewOptions = ['model', 'collection', 'el', 'id', 'attributes', 'className', 'tagName', 'events'];

  // Set up all inheritable **Backbone.View** properties and methods.
  _.extend(View.prototype, Events, {

    // The default `tagName` of a View's element is `"div"`.
    tagName: 'div',

    // jQuery delegate for element lookup, scoped to DOM elements within the
    // current view. This should be preferred to global lookups where possible.
    $: function(selector) {
      return this.$el.find(selector);
    },

    // Initialize is an empty function by default. Override it with your own
    // initialization logic.
    initialize: function(){},

    // **render** is the core function that your view should override, in order
    // to populate its element (`this.el`), with the appropriate HTML. The
    // convention is for **render** to always return `this`.
    render: function() {
      return this;
    },

    // Remove this view by taking the element out of the DOM, and removing any
    // applicable Backbone.Events listeners.
    remove: function() {
      this._removeElement();
      this.stopListening();
      return this;
    },

    // Remove this view's element from the document and all event listeners
    // attached to it. Exposed for subclasses using an alternative DOM
    // manipulation API.
    _removeElement: function() {
      this.$el.remove();
    },

    // Change the view's element (`this.el` property) and re-delegate the
    // view's events on the new element.
    setElement: function(element) {
      this.undelegateEvents();
      this._setElement(element);
      this.delegateEvents();
      return this;
    },

    // Creates the `this.el` and `this.$el` references for this view using the
    // given `el`. `el` can be a CSS selector or an HTML string, a jQuery
    // context or an element. Subclasses can override this to utilize an
    // alternative DOM manipulation API and are only required to set the
    // `this.el` property.
    _setElement: function(el) {
      this.$el = el instanceof Backbone.$ ? el : Backbone.$(el);
      this.el = this.$el[0];
    },

    // Set callbacks, where `this.events` is a hash of
    //
    // *{"event selector": "callback"}*
    //
    //     {
    //       'mousedown .title':  'edit',
    //       'click .button':     'save',
    //       'click .open':       function(e) { ... }
    //     }
    //
    // pairs. Callbacks will be bound to the view, with `this` set properly.
    // Uses event delegation for efficiency.
    // Omitting the selector binds the event to `this.el`.
    delegateEvents: function(events) {
      events || (events = _.result(this, 'events'));
      if (!events) return this;
      this.undelegateEvents();
      for (var key in events) {
        var method = events[key];
        if (!_.isFunction(method)) method = this[method];
        if (!method) continue;
        var match = key.match(delegateEventSplitter);
        this.delegate(match[1], match[2], _.bind(method, this));
      }
      return this;
    },

    // Add a single event listener to the view's element (or a child element
    // using `selector`). This only works for delegate-able events: not `focus`,
    // `blur`, and not `change`, `submit`, and `reset` in Internet Explorer.
    delegate: function(eventName, selector, listener) {
      this.$el.on(eventName + '.delegateEvents' + this.cid, selector, listener);
      return this;
    },

    // Clears all callbacks previously bound to the view by `delegateEvents`.
    // You usually don't need to use this, but may wish to if you have multiple
    // Backbone views attached to the same DOM element.
    undelegateEvents: function() {
      if (this.$el) this.$el.off('.delegateEvents' + this.cid);
      return this;
    },

    // A finer-grained `undelegateEvents` for removing a single delegated event.
    // `selector` and `listener` are both optional.
    undelegate: function(eventName, selector, listener) {
      this.$el.off(eventName + '.delegateEvents' + this.cid, selector, listener);
      return this;
    },

    // Produces a DOM element to be assigned to your view. Exposed for
    // subclasses using an alternative DOM manipulation API.
    _createElement: function(tagName) {
      return document.createElement(tagName);
    },

    // Ensure that the View has a DOM element to render into.
    // If `this.el` is a string, pass it through `$()`, take the first
    // matching element, and re-assign it to `el`. Otherwise, create
    // an element from the `id`, `className` and `tagName` properties.
    _ensureElement: function() {
      if (!this.el) {
        var attrs = _.extend({}, _.result(this, 'attributes'));
        if (this.id) attrs.id = _.result(this, 'id');
        if (this.className) attrs['class'] = _.result(this, 'className');
        this.setElement(this._createElement(_.result(this, 'tagName')));
        this._setAttributes(attrs);
      } else {
        this.setElement(_.result(this, 'el'));
      }
    },

    // Set attributes from a hash on this view's element.  Exposed for
    // subclasses using an alternative DOM manipulation API.
    _setAttributes: function(attributes) {
      this.$el.attr(attributes);
    }

  });

  // Backbone.sync
  // -------------

  // Override this function to change the manner in which Backbone persists
  // models to the server. You will be passed the type of request, and the
  // model in question. By default, makes a RESTful Ajax request
  // to the model's `url()`. Some possible customizations could be:
  //
  // * Use `setTimeout` to batch rapid-fire updates into a single request.
  // * Send up the models as XML instead of JSON.
  // * Persist models via WebSockets instead of Ajax.
  //
  // Turn on `Backbone.emulateHTTP` in order to send `PUT` and `DELETE` requests
  // as `POST`, with a `_method` parameter containing the true HTTP method,
  // as well as all requests with the body as `application/x-www-form-urlencoded`
  // instead of `application/json` with the model in a param named `model`.
  // Useful when interfacing with server-side languages like **PHP** that make
  // it difficult to read the body of `PUT` requests.
  Backbone.sync = function(method, model, options) {
    var type = methodMap[method];

    // Default options, unless specified.
    _.defaults(options || (options = {}), {
      emulateHTTP: Backbone.emulateHTTP,
      emulateJSON: Backbone.emulateJSON
    });

    // Default JSON-request options.
    var params = {type: type, dataType: 'json'};

    // Ensure that we have a URL.
    if (!options.url) {
      params.url = _.result(model, 'url') || urlError();
    }

    // Ensure that we have the appropriate request data.
    if (options.data == null && model && (method === 'create' || method === 'update' || method === 'patch')) {
      params.contentType = 'application/json';
      params.data = JSON.stringify(options.attrs || model.toJSON(options));
    }

    // For older servers, emulate JSON by encoding the request into an HTML-form.
    if (options.emulateJSON) {
      params.contentType = 'application/x-www-form-urlencoded';
      params.data = params.data ? {model: params.data} : {};
    }

    // For older servers, emulate HTTP by mimicking the HTTP method with `_method`
    // And an `X-HTTP-Method-Override` header.
    if (options.emulateHTTP && (type === 'PUT' || type === 'DELETE' || type === 'PATCH')) {
      params.type = 'POST';
      if (options.emulateJSON) params.data._method = type;
      var beforeSend = options.beforeSend;
      options.beforeSend = function(xhr) {
        xhr.setRequestHeader('X-HTTP-Method-Override', type);
        if (beforeSend) return beforeSend.apply(this, arguments);
      };
    }

    // Don't process data on a non-GET request.
    if (params.type !== 'GET' && !options.emulateJSON) {
      params.processData = false;
    }

    // Pass along `textStatus` and `errorThrown` from jQuery.
    var error = options.error;
    options.error = function(xhr, textStatus, errorThrown) {
      options.textStatus = textStatus;
      options.errorThrown = errorThrown;
      if (error) error.call(options.context, xhr, textStatus, errorThrown);
    };

    // Make the request, allowing the user to override any Ajax options.
    var xhr = options.xhr = Backbone.ajax(_.extend(params, options));
    model.trigger('request', model, xhr, options);
    return xhr;
  };

  // Map from CRUD to HTTP for our default `Backbone.sync` implementation.
  var methodMap = {
    'create': 'POST',
    'update': 'PUT',
    'patch':  'PATCH',
    'delete': 'DELETE',
    'read':   'GET'
  };

  // Set the default implementation of `Backbone.ajax` to proxy through to `$`.
  // Override this if you'd like to use a different library.
  Backbone.ajax = function() {
    return Backbone.$.ajax.apply(Backbone.$, arguments);
  };

  // Backbone.Router
  // ---------------

  // Routers map faux-URLs to actions, and fire events when routes are
  // matched. Creating a new one sets its `routes` hash, if not set statically.
  var Router = Backbone.Router = function(options) {
    options || (options = {});
    if (options.routes) this.routes = options.routes;
    this._bindRoutes();
    this.initialize.apply(this, arguments);
  };

  // Cached regular expressions for matching named param parts and splatted
  // parts of route strings.
  var optionalParam = /\((.*?)\)/g;
  var namedParam    = /(\(\?)?:\w+/g;
  var splatParam    = /\*\w+/g;
  var escapeRegExp  = /[\-{}\[\]+?.,\\\^$|#\s]/g;

  // Set up all inheritable **Backbone.Router** properties and methods.
  _.extend(Router.prototype, Events, {

    // Initialize is an empty function by default. Override it with your own
    // initialization logic.
    initialize: function(){},

    // Manually bind a single named route to a callback. For example:
    //
    //     this.route('search/:query/p:num', 'search', function(query, num) {
    //       ...
    //     });
    //
    route: function(route, name, callback) {
      if (!_.isRegExp(route)) route = this._routeToRegExp(route);
      if (_.isFunction(name)) {
        callback = name;
        name = '';
      }
      if (!callback) callback = this[name];
      var router = this;
      Backbone.history.route(route, function(fragment) {
        var args = router._extractParameters(route, fragment);
        if (router.execute(callback, args, name) !== false) {
          router.trigger.apply(router, ['route:' + name].concat(args));
          router.trigger('route', name, args);
          Backbone.history.trigger('route', router, name, args);
        }
      });
      return this;
    },

    // Execute a route handler with the provided parameters.  This is an
    // excellent place to do pre-route setup or post-route cleanup.
    execute: function(callback, args, name) {
      if (callback) callback.apply(this, args);
    },

    // Simple proxy to `Backbone.history` to save a fragment into the history.
    navigate: function(fragment, options) {
      Backbone.history.navigate(fragment, options);
      return this;
    },

    // Bind all defined routes to `Backbone.history`. We have to reverse the
    // order of the routes here to support behavior where the most general
    // routes can be defined at the bottom of the route map.
    _bindRoutes: function() {
      if (!this.routes) return;
      this.routes = _.result(this, 'routes');
      var route, routes = _.keys(this.routes);
      while ((route = routes.pop()) != null) {
        this.route(route, this.routes[route]);
      }
    },

    // Convert a route string into a regular expression, suitable for matching
    // against the current location hash.
    _routeToRegExp: function(route) {
      route = route.replace(escapeRegExp, '\\$&')
                   .replace(optionalParam, '(?:$1)?')
                   .replace(namedParam, function(match, optional) {
                     return optional ? match : '([^/?]+)';
                   })
                   .replace(splatParam, '([^?]*?)');
      return new RegExp('^' + route + '(?:\\?([\\s\\S]*))?$');
    },

    // Given a route, and a URL fragment that it matches, return the array of
    // extracted decoded parameters. Empty or unmatched parameters will be
    // treated as `null` to normalize cross-browser behavior.
    _extractParameters: function(route, fragment) {
      var params = route.exec(fragment).slice(1);
      return _.map(params, function(param, i) {
        // Don't decode the search params.
        if (i === params.length - 1) return param || null;
        return param ? decodeURIComponent(param) : null;
      });
    }

  });

  // Backbone.History
  // ----------------

  // Handles cross-browser history management, based on either
  // [pushState](http://diveintohtml5.info/history.html) and real URLs, or
  // [onhashchange](https://developer.mozilla.org/en-US/docs/DOM/window.onhashchange)
  // and URL fragments. If the browser supports neither (old IE, natch),
  // falls back to polling.
  var History = Backbone.History = function() {
    this.handlers = [];
    _.bindAll(this, 'checkUrl');

    // Ensure that `History` can be used outside of the browser.
    if (typeof window !== 'undefined') {
      this.location = window.location;
      this.history = window.history;
    }
  };

  // Cached regex for stripping a leading hash/slash and trailing space.
  var routeStripper = /^[#\/]|\s+$/g;

  // Cached regex for stripping leading and trailing slashes.
  var rootStripper = /^\/+|\/+$/g;

  // Cached regex for stripping urls of hash.
  var pathStripper = /#.*$/;

  // Has the history handling already been started?
  History.started = false;

  // Set up all inheritable **Backbone.History** properties and methods.
  _.extend(History.prototype, Events, {

    // The default interval to poll for hash changes, if necessary, is
    // twenty times a second.
    interval: 50,

    // Are we at the app root?
    atRoot: function() {
      var path = this.location.pathname.replace(/[^\/]$/, '$&/');
      return path === this.root && !this.getSearch();
    },

    // Does the pathname match the root?
    matchRoot: function() {
      var path = this.decodeFragment(this.location.pathname);
      var root = path.slice(0, this.root.length - 1) + '/';
      return root === this.root;
    },

    // Unicode characters in `location.pathname` are percent encoded so they're
    // decoded for comparison. `%25` should not be decoded since it may be part
    // of an encoded parameter.
    decodeFragment: function(fragment) {
      return decodeURI(fragment.replace(/%25/g, '%2525'));
    },

    // In IE6, the hash fragment and search params are incorrect if the
    // fragment contains `?`.
    getSearch: function() {
      var match = this.location.href.replace(/#.*/, '').match(/\?.+/);
      return match ? match[0] : '';
    },

    // Gets the true hash value. Cannot use location.hash directly due to bug
    // in Firefox where location.hash will always be decoded.
    getHash: function(window) {
      var match = (window || this).location.href.match(/#(.*)$/);
      return match ? match[1] : '';
    },

    // Get the pathname and search params, without the root.
    getPath: function() {
      var path = this.decodeFragment(
        this.location.pathname + this.getSearch()
      ).slice(this.root.length - 1);
      return path.charAt(0) === '/' ? path.slice(1) : path;
    },

    // Get the cross-browser normalized URL fragment from the path or hash.
    getFragment: function(fragment) {
      if (fragment == null) {
        if (this._usePushState || !this._wantsHashChange) {
          fragment = this.getPath();
        } else {
          fragment = this.getHash();
        }
      }
      return fragment.replace(routeStripper, '');
    },

    // Start the hash change handling, returning `true` if the current URL matches
    // an existing route, and `false` otherwise.
    start: function(options) {
      if (History.started) throw new Error('Backbone.history has already been started');
      History.started = true;

      // Figure out the initial configuration. Do we need an iframe?
      // Is pushState desired ... is it available?
      this.options          = _.extend({root: '/'}, this.options, options);
      this.root             = this.options.root;
      this._wantsHashChange = this.options.hashChange !== false;
      this._hasHashChange   = 'onhashchange' in window;
      this._useHashChange   = this._wantsHashChange && this._hasHashChange;
      this._wantsPushState  = !!this.options.pushState;
      this._hasPushState    = !!(this.history && this.history.pushState);
      this._usePushState    = this._wantsPushState && this._hasPushState;
      this.fragment         = this.getFragment();

      // Normalize root to always include a leading and trailing slash.
      this.root = ('/' + this.root + '/').replace(rootStripper, '/');

      // Transition from hashChange to pushState or vice versa if both are
      // requested.
      if (this._wantsHashChange && this._wantsPushState) {

        // If we've started off with a route from a `pushState`-enabled
        // browser, but we're currently in a browser that doesn't support it...
        if (!this._hasPushState && !this.atRoot()) {
          var root = this.root.slice(0, -1) || '/';
          this.location.replace(root + '#' + this.getPath());
          // Return immediately as browser will do redirect to new url
          return true;

        // Or if we've started out with a hash-based route, but we're currently
        // in a browser where it could be `pushState`-based instead...
        } else if (this._hasPushState && this.atRoot()) {
          this.navigate(this.getHash(), {replace: true});
        }

      }

      // Proxy an iframe to handle location events if the browser doesn't
      // support the `hashchange` event, HTML5 history, or the user wants
      // `hashChange` but not `pushState`.
      if (!this._hasHashChange && this._wantsHashChange && !this._usePushState) {
        this.iframe = document.createElement('iframe');
        this.iframe.src = 'javascript:0';
        this.iframe.style.display = 'none';
        this.iframe.tabIndex = -1;
        var body = document.body;
        // Using `appendChild` will throw on IE < 9 if the document is not ready.
        var iWindow = body.insertBefore(this.iframe, body.firstChild).contentWindow;
        iWindow.document.open();
        iWindow.document.close();
        iWindow.location.hash = '#' + this.fragment;
      }

      // Add a cross-platform `addEventListener` shim for older browsers.
      var addEventListener = window.addEventListener || function (eventName, listener) {
        return attachEvent('on' + eventName, listener);
      };

      // Depending on whether we're using pushState or hashes, and whether
      // 'onhashchange' is supported, determine how we check the URL state.
      if (this._usePushState) {
        addEventListener('popstate', this.checkUrl, false);
      } else if (this._useHashChange && !this.iframe) {
        addEventListener('hashchange', this.checkUrl, false);
      } else if (this._wantsHashChange) {
        this._checkUrlInterval = setInterval(this.checkUrl, this.interval);
      }

      if (!this.options.silent) return this.loadUrl();
    },

    // Disable Backbone.history, perhaps temporarily. Not useful in a real app,
    // but possibly useful for unit testing Routers.
    stop: function() {
      // Add a cross-platform `removeEventListener` shim for older browsers.
      var removeEventListener = window.removeEventListener || function (eventName, listener) {
        return detachEvent('on' + eventName, listener);
      };

      // Remove window listeners.
      if (this._usePushState) {
        removeEventListener('popstate', this.checkUrl, false);
      } else if (this._useHashChange && !this.iframe) {
        removeEventListener('hashchange', this.checkUrl, false);
      }

      // Clean up the iframe if necessary.
      if (this.iframe) {
        document.body.removeChild(this.iframe);
        this.iframe = null;
      }

      // Some environments will throw when clearing an undefined interval.
      if (this._checkUrlInterval) clearInterval(this._checkUrlInterval);
      History.started = false;
    },

    // Add a route to be tested when the fragment changes. Routes added later
    // may override previous routes.
    route: function(route, callback) {
      this.handlers.unshift({route: route, callback: callback});
    },

    // Checks the current URL to see if it has changed, and if it has,
    // calls `loadUrl`, normalizing across the hidden iframe.
    checkUrl: function(e) {
      var current = this.getFragment();

      // If the user pressed the back button, the iframe's hash will have
      // changed and we should use that for comparison.
      if (current === this.fragment && this.iframe) {
        current = this.getHash(this.iframe.contentWindow);
      }

      if (current === this.fragment) return false;
      if (this.iframe) this.navigate(current);
      this.loadUrl();
    },

    // Attempt to load the current URL fragment. If a route succeeds with a
    // match, returns `true`. If no defined routes matches the fragment,
    // returns `false`.
    loadUrl: function(fragment) {
      // If the root doesn't match, no routes can match either.
      if (!this.matchRoot()) return false;
      fragment = this.fragment = this.getFragment(fragment);
      return _.any(this.handlers, function(handler) {
        if (handler.route.test(fragment)) {
          handler.callback(fragment);
          return true;
        }
      });
    },

    // Save a fragment into the hash history, or replace the URL state if the
    // 'replace' option is passed. You are responsible for properly URL-encoding
    // the fragment in advance.
    //
    // The options object can contain `trigger: true` if you wish to have the
    // route callback be fired (not usually desirable), or `replace: true`, if
    // you wish to modify the current URL without adding an entry to the history.
    navigate: function(fragment, options) {
      if (!History.started) return false;
      if (!options || options === true) options = {trigger: !!options};

      // Normalize the fragment.
      fragment = this.getFragment(fragment || '');

      // Don't include a trailing slash on the root.
      var root = this.root;
      if (fragment === '' || fragment.charAt(0) === '?') {
        root = root.slice(0, -1) || '/';
      }
      var url = root + fragment;

      // Strip the hash and decode for matching.
      fragment = this.decodeFragment(fragment.replace(pathStripper, ''));

      if (this.fragment === fragment) return;
      this.fragment = fragment;

      // If pushState is available, we use it to set the fragment as a real URL.
      if (this._usePushState) {
        this.history[options.replace ? 'replaceState' : 'pushState']({}, document.title, url);

      // If hash changes haven't been explicitly disabled, update the hash
      // fragment to store history.
      } else if (this._wantsHashChange) {
        this._updateHash(this.location, fragment, options.replace);
        if (this.iframe && (fragment !== this.getHash(this.iframe.contentWindow))) {
          var iWindow = this.iframe.contentWindow;

          // Opening and closing the iframe tricks IE7 and earlier to push a
          // history entry on hash-tag change.  When replace is true, we don't
          // want this.
          if (!options.replace) {
            iWindow.document.open();
            iWindow.document.close();
          }

          this._updateHash(iWindow.location, fragment, options.replace);
        }

      // If you've told us that you explicitly don't want fallback hashchange-
      // based history, then `navigate` becomes a page refresh.
      } else {
        return this.location.assign(url);
      }
      if (options.trigger) return this.loadUrl(fragment);
    },

    // Update the hash location, either replacing the current entry, or adding
    // a new one to the browser history.
    _updateHash: function(location, fragment, replace) {
      if (replace) {
        var href = location.href.replace(/(javascript:|#).*$/, '');
        location.replace(href + '#' + fragment);
      } else {
        // Some browsers require that `hash` contains a leading #.
        location.hash = '#' + fragment;
      }
    }

  });

  // Create the default Backbone.history.
  Backbone.history = new History;

  // Helpers
  // -------

  // Helper function to correctly set up the prototype chain for subclasses.
  // Similar to `goog.inherits`, but uses a hash of prototype properties and
  // class properties to be extended.
  var extend = function(protoProps, staticProps) {
    var parent = this;
    var child;

    // The constructor function for the new subclass is either defined by you
    // (the "constructor" property in your `extend` definition), or defaulted
    // by us to simply call the parent constructor.
    if (protoProps && _.has(protoProps, 'constructor')) {
      child = protoProps.constructor;
    } else {
      child = function(){ return parent.apply(this, arguments); };
    }

    // Add static properties to the constructor function, if supplied.
    _.extend(child, parent, staticProps);

    // Set the prototype chain to inherit from `parent`, without calling
    // `parent` constructor function.
    var Surrogate = function(){ this.constructor = child; };
    Surrogate.prototype = parent.prototype;
    child.prototype = new Surrogate;

    // Add prototype properties (instance properties) to the subclass,
    // if supplied.
    if (protoProps) _.extend(child.prototype, protoProps);

    // Set a convenience property in case the parent's prototype is needed
    // later.
    child.__super__ = parent.prototype;

    return child;
  };

  // Set up inheritance for the model, collection, router, view and history.
  Model.extend = Collection.extend = Router.extend = View.extend = History.extend = extend;

  // Throw an error when a URL is needed, and none is supplied.
  var urlError = function() {
    throw new Error('A "url" property or function must be specified');
  };

  // Wrap an optional error callback with a fallback error event.
  var wrapError = function(model, options) {
    var error = options.error;
    options.error = function(resp) {
      if (error) error.call(options.context, model, resp, options);
      model.trigger('error', model, resp, options);
    };
  };

  return Backbone;

}));

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"jquery":"jquery","underscore":"underscore"}],"inherit/events":[function(require,module,exports){
var Backbone = require("backbone");
var _ = require("underscore");
var Base = require('./index');
require('backbone-super');

var EventDispatcher = Base.extend({
	initialize:function(attributes, options){
        
    }
});
_.extend(EventDispatcher.prototype, Backbone.Events);


module.exports = EventDispatcher;
},{"./index":"inherit","backbone":"backbone","backbone-super":4,"underscore":"underscore"}],"inherit":[function(require,module,exports){
var Backbone = require("backbone");
var _ = require("underscore");
require('backbone-super');

var Base = function(attributes, options) {
    this.initialize.apply(this, arguments);
};
Base.extend = Backbone.Model.extend;

module.exports = Base;






},{"backbone":"backbone","backbone-super":4,"underscore":"underscore"}],"jquery":[function(require,module,exports){
arguments[4][62][0].apply(exports,arguments)
},{"dup":62}],"underscore":[function(require,module,exports){
//     Underscore.js 1.8.3
//     http://underscorejs.org
//     (c) 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.

(function() {

  // Baseline setup
  // --------------

  // Establish the root object, `window` in the browser, or `exports` on the server.
  var root = this;

  // Save the previous value of the `_` variable.
  var previousUnderscore = root._;

  // Save bytes in the minified (but not gzipped) version:
  var ArrayProto = Array.prototype, ObjProto = Object.prototype, FuncProto = Function.prototype;

  // Create quick reference variables for speed access to core prototypes.
  var
    push             = ArrayProto.push,
    slice            = ArrayProto.slice,
    toString         = ObjProto.toString,
    hasOwnProperty   = ObjProto.hasOwnProperty;

  // All **ECMAScript 5** native function implementations that we hope to use
  // are declared here.
  var
    nativeIsArray      = Array.isArray,
    nativeKeys         = Object.keys,
    nativeBind         = FuncProto.bind,
    nativeCreate       = Object.create;

  // Naked function reference for surrogate-prototype-swapping.
  var Ctor = function(){};

  // Create a safe reference to the Underscore object for use below.
  var _ = function(obj) {
    if (obj instanceof _) return obj;
    if (!(this instanceof _)) return new _(obj);
    this._wrapped = obj;
  };

  // Export the Underscore object for **Node.js**, with
  // backwards-compatibility for the old `require()` API. If we're in
  // the browser, add `_` as a global object.
  if (typeof exports !== 'undefined') {
    if (typeof module !== 'undefined' && module.exports) {
      exports = module.exports = _;
    }
    exports._ = _;
  } else {
    root._ = _;
  }

  // Current version.
  _.VERSION = '1.8.3';

  // Internal function that returns an efficient (for current engines) version
  // of the passed-in callback, to be repeatedly applied in other Underscore
  // functions.
  var optimizeCb = function(func, context, argCount) {
    if (context === void 0) return func;
    switch (argCount == null ? 3 : argCount) {
      case 1: return function(value) {
        return func.call(context, value);
      };
      case 2: return function(value, other) {
        return func.call(context, value, other);
      };
      case 3: return function(value, index, collection) {
        return func.call(context, value, index, collection);
      };
      case 4: return function(accumulator, value, index, collection) {
        return func.call(context, accumulator, value, index, collection);
      };
    }
    return function() {
      return func.apply(context, arguments);
    };
  };

  // A mostly-internal function to generate callbacks that can be applied
  // to each element in a collection, returning the desired result — either
  // identity, an arbitrary callback, a property matcher, or a property accessor.
  var cb = function(value, context, argCount) {
    if (value == null) return _.identity;
    if (_.isFunction(value)) return optimizeCb(value, context, argCount);
    if (_.isObject(value)) return _.matcher(value);
    return _.property(value);
  };
  _.iteratee = function(value, context) {
    return cb(value, context, Infinity);
  };

  // An internal function for creating assigner functions.
  var createAssigner = function(keysFunc, undefinedOnly) {
    return function(obj) {
      var length = arguments.length;
      if (length < 2 || obj == null) return obj;
      for (var index = 1; index < length; index++) {
        var source = arguments[index],
            keys = keysFunc(source),
            l = keys.length;
        for (var i = 0; i < l; i++) {
          var key = keys[i];
          if (!undefinedOnly || obj[key] === void 0) obj[key] = source[key];
        }
      }
      return obj;
    };
  };

  // An internal function for creating a new object that inherits from another.
  var baseCreate = function(prototype) {
    if (!_.isObject(prototype)) return {};
    if (nativeCreate) return nativeCreate(prototype);
    Ctor.prototype = prototype;
    var result = new Ctor;
    Ctor.prototype = null;
    return result;
  };

  var property = function(key) {
    return function(obj) {
      return obj == null ? void 0 : obj[key];
    };
  };

  // Helper for collection methods to determine whether a collection
  // should be iterated as an array or as an object
  // Related: http://people.mozilla.org/~jorendorff/es6-draft.html#sec-tolength
  // Avoids a very nasty iOS 8 JIT bug on ARM-64. #2094
  var MAX_ARRAY_INDEX = Math.pow(2, 53) - 1;
  var getLength = property('length');
  var isArrayLike = function(collection) {
    var length = getLength(collection);
    return typeof length == 'number' && length >= 0 && length <= MAX_ARRAY_INDEX;
  };

  // Collection Functions
  // --------------------

  // The cornerstone, an `each` implementation, aka `forEach`.
  // Handles raw objects in addition to array-likes. Treats all
  // sparse array-likes as if they were dense.
  _.each = _.forEach = function(obj, iteratee, context) {
    iteratee = optimizeCb(iteratee, context);
    var i, length;
    if (isArrayLike(obj)) {
      for (i = 0, length = obj.length; i < length; i++) {
        iteratee(obj[i], i, obj);
      }
    } else {
      var keys = _.keys(obj);
      for (i = 0, length = keys.length; i < length; i++) {
        iteratee(obj[keys[i]], keys[i], obj);
      }
    }
    return obj;
  };

  // Return the results of applying the iteratee to each element.
  _.map = _.collect = function(obj, iteratee, context) {
    iteratee = cb(iteratee, context);
    var keys = !isArrayLike(obj) && _.keys(obj),
        length = (keys || obj).length,
        results = Array(length);
    for (var index = 0; index < length; index++) {
      var currentKey = keys ? keys[index] : index;
      results[index] = iteratee(obj[currentKey], currentKey, obj);
    }
    return results;
  };

  // Create a reducing function iterating left or right.
  function createReduce(dir) {
    // Optimized iterator function as using arguments.length
    // in the main function will deoptimize the, see #1991.
    function iterator(obj, iteratee, memo, keys, index, length) {
      for (; index >= 0 && index < length; index += dir) {
        var currentKey = keys ? keys[index] : index;
        memo = iteratee(memo, obj[currentKey], currentKey, obj);
      }
      return memo;
    }

    return function(obj, iteratee, memo, context) {
      iteratee = optimizeCb(iteratee, context, 4);
      var keys = !isArrayLike(obj) && _.keys(obj),
          length = (keys || obj).length,
          index = dir > 0 ? 0 : length - 1;
      // Determine the initial value if none is provided.
      if (arguments.length < 3) {
        memo = obj[keys ? keys[index] : index];
        index += dir;
      }
      return iterator(obj, iteratee, memo, keys, index, length);
    };
  }

  // **Reduce** builds up a single result from a list of values, aka `inject`,
  // or `foldl`.
  _.reduce = _.foldl = _.inject = createReduce(1);

  // The right-associative version of reduce, also known as `foldr`.
  _.reduceRight = _.foldr = createReduce(-1);

  // Return the first value which passes a truth test. Aliased as `detect`.
  _.find = _.detect = function(obj, predicate, context) {
    var key;
    if (isArrayLike(obj)) {
      key = _.findIndex(obj, predicate, context);
    } else {
      key = _.findKey(obj, predicate, context);
    }
    if (key !== void 0 && key !== -1) return obj[key];
  };

  // Return all the elements that pass a truth test.
  // Aliased as `select`.
  _.filter = _.select = function(obj, predicate, context) {
    var results = [];
    predicate = cb(predicate, context);
    _.each(obj, function(value, index, list) {
      if (predicate(value, index, list)) results.push(value);
    });
    return results;
  };

  // Return all the elements for which a truth test fails.
  _.reject = function(obj, predicate, context) {
    return _.filter(obj, _.negate(cb(predicate)), context);
  };

  // Determine whether all of the elements match a truth test.
  // Aliased as `all`.
  _.every = _.all = function(obj, predicate, context) {
    predicate = cb(predicate, context);
    var keys = !isArrayLike(obj) && _.keys(obj),
        length = (keys || obj).length;
    for (var index = 0; index < length; index++) {
      var currentKey = keys ? keys[index] : index;
      if (!predicate(obj[currentKey], currentKey, obj)) return false;
    }
    return true;
  };

  // Determine if at least one element in the object matches a truth test.
  // Aliased as `any`.
  _.some = _.any = function(obj, predicate, context) {
    predicate = cb(predicate, context);
    var keys = !isArrayLike(obj) && _.keys(obj),
        length = (keys || obj).length;
    for (var index = 0; index < length; index++) {
      var currentKey = keys ? keys[index] : index;
      if (predicate(obj[currentKey], currentKey, obj)) return true;
    }
    return false;
  };

  // Determine if the array or object contains a given item (using `===`).
  // Aliased as `includes` and `include`.
  _.contains = _.includes = _.include = function(obj, item, fromIndex, guard) {
    if (!isArrayLike(obj)) obj = _.values(obj);
    if (typeof fromIndex != 'number' || guard) fromIndex = 0;
    return _.indexOf(obj, item, fromIndex) >= 0;
  };

  // Invoke a method (with arguments) on every item in a collection.
  _.invoke = function(obj, method) {
    var args = slice.call(arguments, 2);
    var isFunc = _.isFunction(method);
    return _.map(obj, function(value) {
      var func = isFunc ? method : value[method];
      return func == null ? func : func.apply(value, args);
    });
  };

  // Convenience version of a common use case of `map`: fetching a property.
  _.pluck = function(obj, key) {
    return _.map(obj, _.property(key));
  };

  // Convenience version of a common use case of `filter`: selecting only objects
  // containing specific `key:value` pairs.
  _.where = function(obj, attrs) {
    return _.filter(obj, _.matcher(attrs));
  };

  // Convenience version of a common use case of `find`: getting the first object
  // containing specific `key:value` pairs.
  _.findWhere = function(obj, attrs) {
    return _.find(obj, _.matcher(attrs));
  };

  // Return the maximum element (or element-based computation).
  _.max = function(obj, iteratee, context) {
    var result = -Infinity, lastComputed = -Infinity,
        value, computed;
    if (iteratee == null && obj != null) {
      obj = isArrayLike(obj) ? obj : _.values(obj);
      for (var i = 0, length = obj.length; i < length; i++) {
        value = obj[i];
        if (value > result) {
          result = value;
        }
      }
    } else {
      iteratee = cb(iteratee, context);
      _.each(obj, function(value, index, list) {
        computed = iteratee(value, index, list);
        if (computed > lastComputed || computed === -Infinity && result === -Infinity) {
          result = value;
          lastComputed = computed;
        }
      });
    }
    return result;
  };

  // Return the minimum element (or element-based computation).
  _.min = function(obj, iteratee, context) {
    var result = Infinity, lastComputed = Infinity,
        value, computed;
    if (iteratee == null && obj != null) {
      obj = isArrayLike(obj) ? obj : _.values(obj);
      for (var i = 0, length = obj.length; i < length; i++) {
        value = obj[i];
        if (value < result) {
          result = value;
        }
      }
    } else {
      iteratee = cb(iteratee, context);
      _.each(obj, function(value, index, list) {
        computed = iteratee(value, index, list);
        if (computed < lastComputed || computed === Infinity && result === Infinity) {
          result = value;
          lastComputed = computed;
        }
      });
    }
    return result;
  };

  // Shuffle a collection, using the modern version of the
  // [Fisher-Yates shuffle](http://en.wikipedia.org/wiki/Fisher–Yates_shuffle).
  _.shuffle = function(obj) {
    var set = isArrayLike(obj) ? obj : _.values(obj);
    var length = set.length;
    var shuffled = Array(length);
    for (var index = 0, rand; index < length; index++) {
      rand = _.random(0, index);
      if (rand !== index) shuffled[index] = shuffled[rand];
      shuffled[rand] = set[index];
    }
    return shuffled;
  };

  // Sample **n** random values from a collection.
  // If **n** is not specified, returns a single random element.
  // The internal `guard` argument allows it to work with `map`.
  _.sample = function(obj, n, guard) {
    if (n == null || guard) {
      if (!isArrayLike(obj)) obj = _.values(obj);
      return obj[_.random(obj.length - 1)];
    }
    return _.shuffle(obj).slice(0, Math.max(0, n));
  };

  // Sort the object's values by a criterion produced by an iteratee.
  _.sortBy = function(obj, iteratee, context) {
    iteratee = cb(iteratee, context);
    return _.pluck(_.map(obj, function(value, index, list) {
      return {
        value: value,
        index: index,
        criteria: iteratee(value, index, list)
      };
    }).sort(function(left, right) {
      var a = left.criteria;
      var b = right.criteria;
      if (a !== b) {
        if (a > b || a === void 0) return 1;
        if (a < b || b === void 0) return -1;
      }
      return left.index - right.index;
    }), 'value');
  };

  // An internal function used for aggregate "group by" operations.
  var group = function(behavior) {
    return function(obj, iteratee, context) {
      var result = {};
      iteratee = cb(iteratee, context);
      _.each(obj, function(value, index) {
        var key = iteratee(value, index, obj);
        behavior(result, value, key);
      });
      return result;
    };
  };

  // Groups the object's values by a criterion. Pass either a string attribute
  // to group by, or a function that returns the criterion.
  _.groupBy = group(function(result, value, key) {
    if (_.has(result, key)) result[key].push(value); else result[key] = [value];
  });

  // Indexes the object's values by a criterion, similar to `groupBy`, but for
  // when you know that your index values will be unique.
  _.indexBy = group(function(result, value, key) {
    result[key] = value;
  });

  // Counts instances of an object that group by a certain criterion. Pass
  // either a string attribute to count by, or a function that returns the
  // criterion.
  _.countBy = group(function(result, value, key) {
    if (_.has(result, key)) result[key]++; else result[key] = 1;
  });

  // Safely create a real, live array from anything iterable.
  _.toArray = function(obj) {
    if (!obj) return [];
    if (_.isArray(obj)) return slice.call(obj);
    if (isArrayLike(obj)) return _.map(obj, _.identity);
    return _.values(obj);
  };

  // Return the number of elements in an object.
  _.size = function(obj) {
    if (obj == null) return 0;
    return isArrayLike(obj) ? obj.length : _.keys(obj).length;
  };

  // Split a collection into two arrays: one whose elements all satisfy the given
  // predicate, and one whose elements all do not satisfy the predicate.
  _.partition = function(obj, predicate, context) {
    predicate = cb(predicate, context);
    var pass = [], fail = [];
    _.each(obj, function(value, key, obj) {
      (predicate(value, key, obj) ? pass : fail).push(value);
    });
    return [pass, fail];
  };

  // Array Functions
  // ---------------

  // Get the first element of an array. Passing **n** will return the first N
  // values in the array. Aliased as `head` and `take`. The **guard** check
  // allows it to work with `_.map`.
  _.first = _.head = _.take = function(array, n, guard) {
    if (array == null) return void 0;
    if (n == null || guard) return array[0];
    return _.initial(array, array.length - n);
  };

  // Returns everything but the last entry of the array. Especially useful on
  // the arguments object. Passing **n** will return all the values in
  // the array, excluding the last N.
  _.initial = function(array, n, guard) {
    return slice.call(array, 0, Math.max(0, array.length - (n == null || guard ? 1 : n)));
  };

  // Get the last element of an array. Passing **n** will return the last N
  // values in the array.
  _.last = function(array, n, guard) {
    if (array == null) return void 0;
    if (n == null || guard) return array[array.length - 1];
    return _.rest(array, Math.max(0, array.length - n));
  };

  // Returns everything but the first entry of the array. Aliased as `tail` and `drop`.
  // Especially useful on the arguments object. Passing an **n** will return
  // the rest N values in the array.
  _.rest = _.tail = _.drop = function(array, n, guard) {
    return slice.call(array, n == null || guard ? 1 : n);
  };

  // Trim out all falsy values from an array.
  _.compact = function(array) {
    return _.filter(array, _.identity);
  };

  // Internal implementation of a recursive `flatten` function.
  var flatten = function(input, shallow, strict, startIndex) {
    var output = [], idx = 0;
    for (var i = startIndex || 0, length = getLength(input); i < length; i++) {
      var value = input[i];
      if (isArrayLike(value) && (_.isArray(value) || _.isArguments(value))) {
        //flatten current level of array or arguments object
        if (!shallow) value = flatten(value, shallow, strict);
        var j = 0, len = value.length;
        output.length += len;
        while (j < len) {
          output[idx++] = value[j++];
        }
      } else if (!strict) {
        output[idx++] = value;
      }
    }
    return output;
  };

  // Flatten out an array, either recursively (by default), or just one level.
  _.flatten = function(array, shallow) {
    return flatten(array, shallow, false);
  };

  // Return a version of the array that does not contain the specified value(s).
  _.without = function(array) {
    return _.difference(array, slice.call(arguments, 1));
  };

  // Produce a duplicate-free version of the array. If the array has already
  // been sorted, you have the option of using a faster algorithm.
  // Aliased as `unique`.
  _.uniq = _.unique = function(array, isSorted, iteratee, context) {
    if (!_.isBoolean(isSorted)) {
      context = iteratee;
      iteratee = isSorted;
      isSorted = false;
    }
    if (iteratee != null) iteratee = cb(iteratee, context);
    var result = [];
    var seen = [];
    for (var i = 0, length = getLength(array); i < length; i++) {
      var value = array[i],
          computed = iteratee ? iteratee(value, i, array) : value;
      if (isSorted) {
        if (!i || seen !== computed) result.push(value);
        seen = computed;
      } else if (iteratee) {
        if (!_.contains(seen, computed)) {
          seen.push(computed);
          result.push(value);
        }
      } else if (!_.contains(result, value)) {
        result.push(value);
      }
    }
    return result;
  };

  // Produce an array that contains the union: each distinct element from all of
  // the passed-in arrays.
  _.union = function() {
    return _.uniq(flatten(arguments, true, true));
  };

  // Produce an array that contains every item shared between all the
  // passed-in arrays.
  _.intersection = function(array) {
    var result = [];
    var argsLength = arguments.length;
    for (var i = 0, length = getLength(array); i < length; i++) {
      var item = array[i];
      if (_.contains(result, item)) continue;
      for (var j = 1; j < argsLength; j++) {
        if (!_.contains(arguments[j], item)) break;
      }
      if (j === argsLength) result.push(item);
    }
    return result;
  };

  // Take the difference between one array and a number of other arrays.
  // Only the elements present in just the first array will remain.
  _.difference = function(array) {
    var rest = flatten(arguments, true, true, 1);
    return _.filter(array, function(value){
      return !_.contains(rest, value);
    });
  };

  // Zip together multiple lists into a single array -- elements that share
  // an index go together.
  _.zip = function() {
    return _.unzip(arguments);
  };

  // Complement of _.zip. Unzip accepts an array of arrays and groups
  // each array's elements on shared indices
  _.unzip = function(array) {
    var length = array && _.max(array, getLength).length || 0;
    var result = Array(length);

    for (var index = 0; index < length; index++) {
      result[index] = _.pluck(array, index);
    }
    return result;
  };

  // Converts lists into objects. Pass either a single array of `[key, value]`
  // pairs, or two parallel arrays of the same length -- one of keys, and one of
  // the corresponding values.
  _.object = function(list, values) {
    var result = {};
    for (var i = 0, length = getLength(list); i < length; i++) {
      if (values) {
        result[list[i]] = values[i];
      } else {
        result[list[i][0]] = list[i][1];
      }
    }
    return result;
  };

  // Generator function to create the findIndex and findLastIndex functions
  function createPredicateIndexFinder(dir) {
    return function(array, predicate, context) {
      predicate = cb(predicate, context);
      var length = getLength(array);
      var index = dir > 0 ? 0 : length - 1;
      for (; index >= 0 && index < length; index += dir) {
        if (predicate(array[index], index, array)) return index;
      }
      return -1;
    };
  }

  // Returns the first index on an array-like that passes a predicate test
  _.findIndex = createPredicateIndexFinder(1);
  _.findLastIndex = createPredicateIndexFinder(-1);

  // Use a comparator function to figure out the smallest index at which
  // an object should be inserted so as to maintain order. Uses binary search.
  _.sortedIndex = function(array, obj, iteratee, context) {
    iteratee = cb(iteratee, context, 1);
    var value = iteratee(obj);
    var low = 0, high = getLength(array);
    while (low < high) {
      var mid = Math.floor((low + high) / 2);
      if (iteratee(array[mid]) < value) low = mid + 1; else high = mid;
    }
    return low;
  };

  // Generator function to create the indexOf and lastIndexOf functions
  function createIndexFinder(dir, predicateFind, sortedIndex) {
    return function(array, item, idx) {
      var i = 0, length = getLength(array);
      if (typeof idx == 'number') {
        if (dir > 0) {
            i = idx >= 0 ? idx : Math.max(idx + length, i);
        } else {
            length = idx >= 0 ? Math.min(idx + 1, length) : idx + length + 1;
        }
      } else if (sortedIndex && idx && length) {
        idx = sortedIndex(array, item);
        return array[idx] === item ? idx : -1;
      }
      if (item !== item) {
        idx = predicateFind(slice.call(array, i, length), _.isNaN);
        return idx >= 0 ? idx + i : -1;
      }
      for (idx = dir > 0 ? i : length - 1; idx >= 0 && idx < length; idx += dir) {
        if (array[idx] === item) return idx;
      }
      return -1;
    };
  }

  // Return the position of the first occurrence of an item in an array,
  // or -1 if the item is not included in the array.
  // If the array is large and already in sort order, pass `true`
  // for **isSorted** to use binary search.
  _.indexOf = createIndexFinder(1, _.findIndex, _.sortedIndex);
  _.lastIndexOf = createIndexFinder(-1, _.findLastIndex);

  // Generate an integer Array containing an arithmetic progression. A port of
  // the native Python `range()` function. See
  // [the Python documentation](http://docs.python.org/library/functions.html#range).
  _.range = function(start, stop, step) {
    if (stop == null) {
      stop = start || 0;
      start = 0;
    }
    step = step || 1;

    var length = Math.max(Math.ceil((stop - start) / step), 0);
    var range = Array(length);

    for (var idx = 0; idx < length; idx++, start += step) {
      range[idx] = start;
    }

    return range;
  };

  // Function (ahem) Functions
  // ------------------

  // Determines whether to execute a function as a constructor
  // or a normal function with the provided arguments
  var executeBound = function(sourceFunc, boundFunc, context, callingContext, args) {
    if (!(callingContext instanceof boundFunc)) return sourceFunc.apply(context, args);
    var self = baseCreate(sourceFunc.prototype);
    var result = sourceFunc.apply(self, args);
    if (_.isObject(result)) return result;
    return self;
  };

  // Create a function bound to a given object (assigning `this`, and arguments,
  // optionally). Delegates to **ECMAScript 5**'s native `Function.bind` if
  // available.
  _.bind = function(func, context) {
    if (nativeBind && func.bind === nativeBind) return nativeBind.apply(func, slice.call(arguments, 1));
    if (!_.isFunction(func)) throw new TypeError('Bind must be called on a function');
    var args = slice.call(arguments, 2);
    var bound = function() {
      return executeBound(func, bound, context, this, args.concat(slice.call(arguments)));
    };
    return bound;
  };

  // Partially apply a function by creating a version that has had some of its
  // arguments pre-filled, without changing its dynamic `this` context. _ acts
  // as a placeholder, allowing any combination of arguments to be pre-filled.
  _.partial = function(func) {
    var boundArgs = slice.call(arguments, 1);
    var bound = function() {
      var position = 0, length = boundArgs.length;
      var args = Array(length);
      for (var i = 0; i < length; i++) {
        args[i] = boundArgs[i] === _ ? arguments[position++] : boundArgs[i];
      }
      while (position < arguments.length) args.push(arguments[position++]);
      return executeBound(func, bound, this, this, args);
    };
    return bound;
  };

  // Bind a number of an object's methods to that object. Remaining arguments
  // are the method names to be bound. Useful for ensuring that all callbacks
  // defined on an object belong to it.
  _.bindAll = function(obj) {
    var i, length = arguments.length, key;
    if (length <= 1) throw new Error('bindAll must be passed function names');
    for (i = 1; i < length; i++) {
      key = arguments[i];
      obj[key] = _.bind(obj[key], obj);
    }
    return obj;
  };

  // Memoize an expensive function by storing its results.
  _.memoize = function(func, hasher) {
    var memoize = function(key) {
      var cache = memoize.cache;
      var address = '' + (hasher ? hasher.apply(this, arguments) : key);
      if (!_.has(cache, address)) cache[address] = func.apply(this, arguments);
      return cache[address];
    };
    memoize.cache = {};
    return memoize;
  };

  // Delays a function for the given number of milliseconds, and then calls
  // it with the arguments supplied.
  _.delay = function(func, wait) {
    var args = slice.call(arguments, 2);
    return setTimeout(function(){
      return func.apply(null, args);
    }, wait);
  };

  // Defers a function, scheduling it to run after the current call stack has
  // cleared.
  _.defer = _.partial(_.delay, _, 1);

  // Returns a function, that, when invoked, will only be triggered at most once
  // during a given window of time. Normally, the throttled function will run
  // as much as it can, without ever going more than once per `wait` duration;
  // but if you'd like to disable the execution on the leading edge, pass
  // `{leading: false}`. To disable execution on the trailing edge, ditto.
  _.throttle = function(func, wait, options) {
    var context, args, result;
    var timeout = null;
    var previous = 0;
    if (!options) options = {};
    var later = function() {
      previous = options.leading === false ? 0 : _.now();
      timeout = null;
      result = func.apply(context, args);
      if (!timeout) context = args = null;
    };
    return function() {
      var now = _.now();
      if (!previous && options.leading === false) previous = now;
      var remaining = wait - (now - previous);
      context = this;
      args = arguments;
      if (remaining <= 0 || remaining > wait) {
        if (timeout) {
          clearTimeout(timeout);
          timeout = null;
        }
        previous = now;
        result = func.apply(context, args);
        if (!timeout) context = args = null;
      } else if (!timeout && options.trailing !== false) {
        timeout = setTimeout(later, remaining);
      }
      return result;
    };
  };

  // Returns a function, that, as long as it continues to be invoked, will not
  // be triggered. The function will be called after it stops being called for
  // N milliseconds. If `immediate` is passed, trigger the function on the
  // leading edge, instead of the trailing.
  _.debounce = function(func, wait, immediate) {
    var timeout, args, context, timestamp, result;

    var later = function() {
      var last = _.now() - timestamp;

      if (last < wait && last >= 0) {
        timeout = setTimeout(later, wait - last);
      } else {
        timeout = null;
        if (!immediate) {
          result = func.apply(context, args);
          if (!timeout) context = args = null;
        }
      }
    };

    return function() {
      context = this;
      args = arguments;
      timestamp = _.now();
      var callNow = immediate && !timeout;
      if (!timeout) timeout = setTimeout(later, wait);
      if (callNow) {
        result = func.apply(context, args);
        context = args = null;
      }

      return result;
    };
  };

  // Returns the first function passed as an argument to the second,
  // allowing you to adjust arguments, run code before and after, and
  // conditionally execute the original function.
  _.wrap = function(func, wrapper) {
    return _.partial(wrapper, func);
  };

  // Returns a negated version of the passed-in predicate.
  _.negate = function(predicate) {
    return function() {
      return !predicate.apply(this, arguments);
    };
  };

  // Returns a function that is the composition of a list of functions, each
  // consuming the return value of the function that follows.
  _.compose = function() {
    var args = arguments;
    var start = args.length - 1;
    return function() {
      var i = start;
      var result = args[start].apply(this, arguments);
      while (i--) result = args[i].call(this, result);
      return result;
    };
  };

  // Returns a function that will only be executed on and after the Nth call.
  _.after = function(times, func) {
    return function() {
      if (--times < 1) {
        return func.apply(this, arguments);
      }
    };
  };

  // Returns a function that will only be executed up to (but not including) the Nth call.
  _.before = function(times, func) {
    var memo;
    return function() {
      if (--times > 0) {
        memo = func.apply(this, arguments);
      }
      if (times <= 1) func = null;
      return memo;
    };
  };

  // Returns a function that will be executed at most one time, no matter how
  // often you call it. Useful for lazy initialization.
  _.once = _.partial(_.before, 2);

  // Object Functions
  // ----------------

  // Keys in IE < 9 that won't be iterated by `for key in ...` and thus missed.
  var hasEnumBug = !{toString: null}.propertyIsEnumerable('toString');
  var nonEnumerableProps = ['valueOf', 'isPrototypeOf', 'toString',
                      'propertyIsEnumerable', 'hasOwnProperty', 'toLocaleString'];

  function collectNonEnumProps(obj, keys) {
    var nonEnumIdx = nonEnumerableProps.length;
    var constructor = obj.constructor;
    var proto = (_.isFunction(constructor) && constructor.prototype) || ObjProto;

    // Constructor is a special case.
    var prop = 'constructor';
    if (_.has(obj, prop) && !_.contains(keys, prop)) keys.push(prop);

    while (nonEnumIdx--) {
      prop = nonEnumerableProps[nonEnumIdx];
      if (prop in obj && obj[prop] !== proto[prop] && !_.contains(keys, prop)) {
        keys.push(prop);
      }
    }
  }

  // Retrieve the names of an object's own properties.
  // Delegates to **ECMAScript 5**'s native `Object.keys`
  _.keys = function(obj) {
    if (!_.isObject(obj)) return [];
    if (nativeKeys) return nativeKeys(obj);
    var keys = [];
    for (var key in obj) if (_.has(obj, key)) keys.push(key);
    // Ahem, IE < 9.
    if (hasEnumBug) collectNonEnumProps(obj, keys);
    return keys;
  };

  // Retrieve all the property names of an object.
  _.allKeys = function(obj) {
    if (!_.isObject(obj)) return [];
    var keys = [];
    for (var key in obj) keys.push(key);
    // Ahem, IE < 9.
    if (hasEnumBug) collectNonEnumProps(obj, keys);
    return keys;
  };

  // Retrieve the values of an object's properties.
  _.values = function(obj) {
    var keys = _.keys(obj);
    var length = keys.length;
    var values = Array(length);
    for (var i = 0; i < length; i++) {
      values[i] = obj[keys[i]];
    }
    return values;
  };

  // Returns the results of applying the iteratee to each element of the object
  // In contrast to _.map it returns an object
  _.mapObject = function(obj, iteratee, context) {
    iteratee = cb(iteratee, context);
    var keys =  _.keys(obj),
          length = keys.length,
          results = {},
          currentKey;
      for (var index = 0; index < length; index++) {
        currentKey = keys[index];
        results[currentKey] = iteratee(obj[currentKey], currentKey, obj);
      }
      return results;
  };

  // Convert an object into a list of `[key, value]` pairs.
  _.pairs = function(obj) {
    var keys = _.keys(obj);
    var length = keys.length;
    var pairs = Array(length);
    for (var i = 0; i < length; i++) {
      pairs[i] = [keys[i], obj[keys[i]]];
    }
    return pairs;
  };

  // Invert the keys and values of an object. The values must be serializable.
  _.invert = function(obj) {
    var result = {};
    var keys = _.keys(obj);
    for (var i = 0, length = keys.length; i < length; i++) {
      result[obj[keys[i]]] = keys[i];
    }
    return result;
  };

  // Return a sorted list of the function names available on the object.
  // Aliased as `methods`
  _.functions = _.methods = function(obj) {
    var names = [];
    for (var key in obj) {
      if (_.isFunction(obj[key])) names.push(key);
    }
    return names.sort();
  };

  // Extend a given object with all the properties in passed-in object(s).
  _.extend = createAssigner(_.allKeys);

  // Assigns a given object with all the own properties in the passed-in object(s)
  // (https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)
  _.extendOwn = _.assign = createAssigner(_.keys);

  // Returns the first key on an object that passes a predicate test
  _.findKey = function(obj, predicate, context) {
    predicate = cb(predicate, context);
    var keys = _.keys(obj), key;
    for (var i = 0, length = keys.length; i < length; i++) {
      key = keys[i];
      if (predicate(obj[key], key, obj)) return key;
    }
  };

  // Return a copy of the object only containing the whitelisted properties.
  _.pick = function(object, oiteratee, context) {
    var result = {}, obj = object, iteratee, keys;
    if (obj == null) return result;
    if (_.isFunction(oiteratee)) {
      keys = _.allKeys(obj);
      iteratee = optimizeCb(oiteratee, context);
    } else {
      keys = flatten(arguments, false, false, 1);
      iteratee = function(value, key, obj) { return key in obj; };
      obj = Object(obj);
    }
    for (var i = 0, length = keys.length; i < length; i++) {
      var key = keys[i];
      var value = obj[key];
      if (iteratee(value, key, obj)) result[key] = value;
    }
    return result;
  };

   // Return a copy of the object without the blacklisted properties.
  _.omit = function(obj, iteratee, context) {
    if (_.isFunction(iteratee)) {
      iteratee = _.negate(iteratee);
    } else {
      var keys = _.map(flatten(arguments, false, false, 1), String);
      iteratee = function(value, key) {
        return !_.contains(keys, key);
      };
    }
    return _.pick(obj, iteratee, context);
  };

  // Fill in a given object with default properties.
  _.defaults = createAssigner(_.allKeys, true);

  // Creates an object that inherits from the given prototype object.
  // If additional properties are provided then they will be added to the
  // created object.
  _.create = function(prototype, props) {
    var result = baseCreate(prototype);
    if (props) _.extendOwn(result, props);
    return result;
  };

  // Create a (shallow-cloned) duplicate of an object.
  _.clone = function(obj) {
    if (!_.isObject(obj)) return obj;
    return _.isArray(obj) ? obj.slice() : _.extend({}, obj);
  };

  // Invokes interceptor with the obj, and then returns obj.
  // The primary purpose of this method is to "tap into" a method chain, in
  // order to perform operations on intermediate results within the chain.
  _.tap = function(obj, interceptor) {
    interceptor(obj);
    return obj;
  };

  // Returns whether an object has a given set of `key:value` pairs.
  _.isMatch = function(object, attrs) {
    var keys = _.keys(attrs), length = keys.length;
    if (object == null) return !length;
    var obj = Object(object);
    for (var i = 0; i < length; i++) {
      var key = keys[i];
      if (attrs[key] !== obj[key] || !(key in obj)) return false;
    }
    return true;
  };


  // Internal recursive comparison function for `isEqual`.
  var eq = function(a, b, aStack, bStack) {
    // Identical objects are equal. `0 === -0`, but they aren't identical.
    // See the [Harmony `egal` proposal](http://wiki.ecmascript.org/doku.php?id=harmony:egal).
    if (a === b) return a !== 0 || 1 / a === 1 / b;
    // A strict comparison is necessary because `null == undefined`.
    if (a == null || b == null) return a === b;
    // Unwrap any wrapped objects.
    if (a instanceof _) a = a._wrapped;
    if (b instanceof _) b = b._wrapped;
    // Compare `[[Class]]` names.
    var className = toString.call(a);
    if (className !== toString.call(b)) return false;
    switch (className) {
      // Strings, numbers, regular expressions, dates, and booleans are compared by value.
      case '[object RegExp]':
      // RegExps are coerced to strings for comparison (Note: '' + /a/i === '/a/i')
      case '[object String]':
        // Primitives and their corresponding object wrappers are equivalent; thus, `"5"` is
        // equivalent to `new String("5")`.
        return '' + a === '' + b;
      case '[object Number]':
        // `NaN`s are equivalent, but non-reflexive.
        // Object(NaN) is equivalent to NaN
        if (+a !== +a) return +b !== +b;
        // An `egal` comparison is performed for other numeric values.
        return +a === 0 ? 1 / +a === 1 / b : +a === +b;
      case '[object Date]':
      case '[object Boolean]':
        // Coerce dates and booleans to numeric primitive values. Dates are compared by their
        // millisecond representations. Note that invalid dates with millisecond representations
        // of `NaN` are not equivalent.
        return +a === +b;
    }

    var areArrays = className === '[object Array]';
    if (!areArrays) {
      if (typeof a != 'object' || typeof b != 'object') return false;

      // Objects with different constructors are not equivalent, but `Object`s or `Array`s
      // from different frames are.
      var aCtor = a.constructor, bCtor = b.constructor;
      if (aCtor !== bCtor && !(_.isFunction(aCtor) && aCtor instanceof aCtor &&
                               _.isFunction(bCtor) && bCtor instanceof bCtor)
                          && ('constructor' in a && 'constructor' in b)) {
        return false;
      }
    }
    // Assume equality for cyclic structures. The algorithm for detecting cyclic
    // structures is adapted from ES 5.1 section 15.12.3, abstract operation `JO`.

    // Initializing stack of traversed objects.
    // It's done here since we only need them for objects and arrays comparison.
    aStack = aStack || [];
    bStack = bStack || [];
    var length = aStack.length;
    while (length--) {
      // Linear search. Performance is inversely proportional to the number of
      // unique nested structures.
      if (aStack[length] === a) return bStack[length] === b;
    }

    // Add the first object to the stack of traversed objects.
    aStack.push(a);
    bStack.push(b);

    // Recursively compare objects and arrays.
    if (areArrays) {
      // Compare array lengths to determine if a deep comparison is necessary.
      length = a.length;
      if (length !== b.length) return false;
      // Deep compare the contents, ignoring non-numeric properties.
      while (length--) {
        if (!eq(a[length], b[length], aStack, bStack)) return false;
      }
    } else {
      // Deep compare objects.
      var keys = _.keys(a), key;
      length = keys.length;
      // Ensure that both objects contain the same number of properties before comparing deep equality.
      if (_.keys(b).length !== length) return false;
      while (length--) {
        // Deep compare each member
        key = keys[length];
        if (!(_.has(b, key) && eq(a[key], b[key], aStack, bStack))) return false;
      }
    }
    // Remove the first object from the stack of traversed objects.
    aStack.pop();
    bStack.pop();
    return true;
  };

  // Perform a deep comparison to check if two objects are equal.
  _.isEqual = function(a, b) {
    return eq(a, b);
  };

  // Is a given array, string, or object empty?
  // An "empty" object has no enumerable own-properties.
  _.isEmpty = function(obj) {
    if (obj == null) return true;
    if (isArrayLike(obj) && (_.isArray(obj) || _.isString(obj) || _.isArguments(obj))) return obj.length === 0;
    return _.keys(obj).length === 0;
  };

  // Is a given value a DOM element?
  _.isElement = function(obj) {
    return !!(obj && obj.nodeType === 1);
  };

  // Is a given value an array?
  // Delegates to ECMA5's native Array.isArray
  _.isArray = nativeIsArray || function(obj) {
    return toString.call(obj) === '[object Array]';
  };

  // Is a given variable an object?
  _.isObject = function(obj) {
    var type = typeof obj;
    return type === 'function' || type === 'object' && !!obj;
  };

  // Add some isType methods: isArguments, isFunction, isString, isNumber, isDate, isRegExp, isError.
  _.each(['Arguments', 'Function', 'String', 'Number', 'Date', 'RegExp', 'Error'], function(name) {
    _['is' + name] = function(obj) {
      return toString.call(obj) === '[object ' + name + ']';
    };
  });

  // Define a fallback version of the method in browsers (ahem, IE < 9), where
  // there isn't any inspectable "Arguments" type.
  if (!_.isArguments(arguments)) {
    _.isArguments = function(obj) {
      return _.has(obj, 'callee');
    };
  }

  // Optimize `isFunction` if appropriate. Work around some typeof bugs in old v8,
  // IE 11 (#1621), and in Safari 8 (#1929).
  if (typeof /./ != 'function' && typeof Int8Array != 'object') {
    _.isFunction = function(obj) {
      return typeof obj == 'function' || false;
    };
  }

  // Is a given object a finite number?
  _.isFinite = function(obj) {
    return isFinite(obj) && !isNaN(parseFloat(obj));
  };

  // Is the given value `NaN`? (NaN is the only number which does not equal itself).
  _.isNaN = function(obj) {
    return _.isNumber(obj) && obj !== +obj;
  };

  // Is a given value a boolean?
  _.isBoolean = function(obj) {
    return obj === true || obj === false || toString.call(obj) === '[object Boolean]';
  };

  // Is a given value equal to null?
  _.isNull = function(obj) {
    return obj === null;
  };

  // Is a given variable undefined?
  _.isUndefined = function(obj) {
    return obj === void 0;
  };

  // Shortcut function for checking if an object has a given property directly
  // on itself (in other words, not on a prototype).
  _.has = function(obj, key) {
    return obj != null && hasOwnProperty.call(obj, key);
  };

  // Utility Functions
  // -----------------

  // Run Underscore.js in *noConflict* mode, returning the `_` variable to its
  // previous owner. Returns a reference to the Underscore object.
  _.noConflict = function() {
    root._ = previousUnderscore;
    return this;
  };

  // Keep the identity function around for default iteratees.
  _.identity = function(value) {
    return value;
  };

  // Predicate-generating functions. Often useful outside of Underscore.
  _.constant = function(value) {
    return function() {
      return value;
    };
  };

  _.noop = function(){};

  _.property = property;

  // Generates a function for a given object that returns a given property.
  _.propertyOf = function(obj) {
    return obj == null ? function(){} : function(key) {
      return obj[key];
    };
  };

  // Returns a predicate for checking whether an object has a given set of
  // `key:value` pairs.
  _.matcher = _.matches = function(attrs) {
    attrs = _.extendOwn({}, attrs);
    return function(obj) {
      return _.isMatch(obj, attrs);
    };
  };

  // Run a function **n** times.
  _.times = function(n, iteratee, context) {
    var accum = Array(Math.max(0, n));
    iteratee = optimizeCb(iteratee, context, 1);
    for (var i = 0; i < n; i++) accum[i] = iteratee(i);
    return accum;
  };

  // Return a random integer between min and max (inclusive).
  _.random = function(min, max) {
    if (max == null) {
      max = min;
      min = 0;
    }
    return min + Math.floor(Math.random() * (max - min + 1));
  };

  // A (possibly faster) way to get the current timestamp as an integer.
  _.now = Date.now || function() {
    return new Date().getTime();
  };

   // List of HTML entities for escaping.
  var escapeMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#x27;',
    '`': '&#x60;'
  };
  var unescapeMap = _.invert(escapeMap);

  // Functions for escaping and unescaping strings to/from HTML interpolation.
  var createEscaper = function(map) {
    var escaper = function(match) {
      return map[match];
    };
    // Regexes for identifying a key that needs to be escaped
    var source = '(?:' + _.keys(map).join('|') + ')';
    var testRegexp = RegExp(source);
    var replaceRegexp = RegExp(source, 'g');
    return function(string) {
      string = string == null ? '' : '' + string;
      return testRegexp.test(string) ? string.replace(replaceRegexp, escaper) : string;
    };
  };
  _.escape = createEscaper(escapeMap);
  _.unescape = createEscaper(unescapeMap);

  // If the value of the named `property` is a function then invoke it with the
  // `object` as context; otherwise, return it.
  _.result = function(object, property, fallback) {
    var value = object == null ? void 0 : object[property];
    if (value === void 0) {
      value = fallback;
    }
    return _.isFunction(value) ? value.call(object) : value;
  };

  // Generate a unique integer id (unique within the entire client session).
  // Useful for temporary DOM ids.
  var idCounter = 0;
  _.uniqueId = function(prefix) {
    var id = ++idCounter + '';
    return prefix ? prefix + id : id;
  };

  // By default, Underscore uses ERB-style template delimiters, change the
  // following template settings to use alternative delimiters.
  _.templateSettings = {
    evaluate    : /<%([\s\S]+?)%>/g,
    interpolate : /<%=([\s\S]+?)%>/g,
    escape      : /<%-([\s\S]+?)%>/g
  };

  // When customizing `templateSettings`, if you don't want to define an
  // interpolation, evaluation or escaping regex, we need one that is
  // guaranteed not to match.
  var noMatch = /(.)^/;

  // Certain characters need to be escaped so that they can be put into a
  // string literal.
  var escapes = {
    "'":      "'",
    '\\':     '\\',
    '\r':     'r',
    '\n':     'n',
    '\u2028': 'u2028',
    '\u2029': 'u2029'
  };

  var escaper = /\\|'|\r|\n|\u2028|\u2029/g;

  var escapeChar = function(match) {
    return '\\' + escapes[match];
  };

  // JavaScript micro-templating, similar to John Resig's implementation.
  // Underscore templating handles arbitrary delimiters, preserves whitespace,
  // and correctly escapes quotes within interpolated code.
  // NB: `oldSettings` only exists for backwards compatibility.
  _.template = function(text, settings, oldSettings) {
    if (!settings && oldSettings) settings = oldSettings;
    settings = _.defaults({}, settings, _.templateSettings);

    // Combine delimiters into one regular expression via alternation.
    var matcher = RegExp([
      (settings.escape || noMatch).source,
      (settings.interpolate || noMatch).source,
      (settings.evaluate || noMatch).source
    ].join('|') + '|$', 'g');

    // Compile the template source, escaping string literals appropriately.
    var index = 0;
    var source = "__p+='";
    text.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
      source += text.slice(index, offset).replace(escaper, escapeChar);
      index = offset + match.length;

      if (escape) {
        source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
      } else if (interpolate) {
        source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
      } else if (evaluate) {
        source += "';\n" + evaluate + "\n__p+='";
      }

      // Adobe VMs need the match returned to produce the correct offest.
      return match;
    });
    source += "';\n";

    // If a variable is not specified, place data values in local scope.
    if (!settings.variable) source = 'with(obj||{}){\n' + source + '}\n';

    source = "var __t,__p='',__j=Array.prototype.join," +
      "print=function(){__p+=__j.call(arguments,'');};\n" +
      source + 'return __p;\n';

    try {
      var render = new Function(settings.variable || 'obj', '_', source);
    } catch (e) {
      e.source = source;
      throw e;
    }

    var template = function(data) {
      return render.call(this, data, _);
    };

    // Provide the compiled source as a convenience for precompilation.
    var argument = settings.variable || 'obj';
    template.source = 'function(' + argument + '){\n' + source + '}';

    return template;
  };

  // Add a "chain" function. Start chaining a wrapped Underscore object.
  _.chain = function(obj) {
    var instance = _(obj);
    instance._chain = true;
    return instance;
  };

  // OOP
  // ---------------
  // If Underscore is called as a function, it returns a wrapped object that
  // can be used OO-style. This wrapper holds altered versions of all the
  // underscore functions. Wrapped objects may be chained.

  // Helper function to continue chaining intermediate results.
  var result = function(instance, obj) {
    return instance._chain ? _(obj).chain() : obj;
  };

  // Add your own custom functions to the Underscore object.
  _.mixin = function(obj) {
    _.each(_.functions(obj), function(name) {
      var func = _[name] = obj[name];
      _.prototype[name] = function() {
        var args = [this._wrapped];
        push.apply(args, arguments);
        return result(this, func.apply(_, args));
      };
    });
  };

  // Add all of the Underscore functions to the wrapper object.
  _.mixin(_);

  // Add all mutator Array functions to the wrapper.
  _.each(['pop', 'push', 'reverse', 'shift', 'sort', 'splice', 'unshift'], function(name) {
    var method = ArrayProto[name];
    _.prototype[name] = function() {
      var obj = this._wrapped;
      method.apply(obj, arguments);
      if ((name === 'shift' || name === 'splice') && obj.length === 0) delete obj[0];
      return result(this, obj);
    };
  });

  // Add all accessor Array functions to the wrapper.
  _.each(['concat', 'join', 'slice'], function(name) {
    var method = ArrayProto[name];
    _.prototype[name] = function() {
      return result(this, method.apply(this._wrapped, arguments));
    };
  });

  // Extracts the result from a wrapped and chained object.
  _.prototype.value = function() {
    return this._wrapped;
  };

  // Provide unwrapping proxy for some methods used in engine operations
  // such as arithmetic and JSON stringification.
  _.prototype.valueOf = _.prototype.toJSON = _.prototype.value;

  _.prototype.toString = function() {
    return '' + this._wrapped;
  };

  // AMD registration happens at the end for compatibility with AMD loaders
  // that may not enforce next-turn semantics on modules. Even though general
  // practice for AMD registration is to be anonymous, underscore registers
  // as a named module because, like jQuery, it is a base library that is
  // popular enough to be bundled in a third party lib, but not be part of
  // an AMD load request. Those cases could generate an error when an
  // anonymous define() is called outside of a loader request.
  if (typeof define === 'function' && define.amd) {
    define('underscore', [], function() {
      return _;
    });
  }
}.call(this));

},{}]},{},[2]);
