##Requirements
- [Node](https://nodejs.org/)
- [Grunt](http://gruntjs.com/)
- [Git](https://git-scm.com/downloads)

##Install
Checkout the repo and open a command prompt or terminal in the same directory.
Use npm to install dependencies and set up the project.

    $> npm install

##Build

    $> grunt



